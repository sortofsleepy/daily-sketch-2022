# Daily Sketch 2022

Random ideas and musings, attempting to do something each day. Likely to not be very exciting and or fail spectacularly. 

Getting started
====
I have a lot of interest in perhaps way too many areas, so projects here will be in a variety of formats which might either be 

* C++ 
* Unreal Engine
* Houdini 
* Rust 
* Javascript


Houdini
===
Things were made with the Indie version; that said you should still be able to open in the apprentice version. 

Rust
===
I will most likely be using [my own framework](https://gitlab.com/sortofsleepy/yoi) which does make things difficult for someone else to replicate, but from time to time may switch over to using something like [Nannou](https://nannou.cc/) or [wgpu](https://github.com/gfx-rs/wgpu)
