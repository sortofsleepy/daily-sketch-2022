use std::mem::size_of;
use rand::seq::SliceRandom;
use winit::dpi::PhysicalSize;
use yoi::prelude::*;
use yoi::vk::descriptorset::TextureSamplerDescriptor;
use yoi::vk::framework::apptime::AppTime;
use yoi::vk::framework::offscreen::OffScreen;
use yoi::vk::utils::{as_u8_slice, generate_buffer_for_dynamic_ubo};

const NUM_PANELS:i32 = 3;

/// small bits of data we can just quickly get to the shader.
#[derive(Clone, Copy)]
struct PushParams {
    params:Vec4
}

/// a more general params structure since push constants only hold so much
#[derive(Copy, Clone)]
struct Params {
    lights:[Vec4;4]
}

/// a render-able object in the scene.
struct DisplayObject {
    mesh:Mesh,
    gp:GraphicsPipeline,
    ds:VkDescriptorSet
}

struct Model {
    app:VkApp,
    push_params:PushParams,
    cubes:DisplayObject,
    timer:AppTime,
    offscreen:OffScreen,
    panels:DisplayObject
}

fn main() {
    App::new(String::from("Tryptic"), 1024, 768)
        .setup(setup)
        .draw(draw)
        .run();
}


fn setup(_app: &Yoi,window:&winit::window::Window)->Model{
    ///////// BUILD THE BASE APP ///////////
    let window_size = window.inner_size();

    // init vulkan app instance.
    let mut app = VkApp::init(window);
    app.setup();

    // build timer
    let timer = AppTime::new();

    //// BUILD PUSHABLE PARAMS  ///
    let push_params = PushParams{
        params:Vec4::new(
            0.0,
            window_size.width as f32,
            window_size.height as f32,
            1.0
        )
    };

    // build normal params
    let pbr_params = setup_parameters(app.get_instance());

    // build off screen instance
    let offscreen = OffScreen::new(app.get_instance(),window_size.width as f32, window_size.height as f32);

    // build cube field.
    let cubes = build_cube_field(app.get_instance(),offscreen.get_renderpass(),pbr_params);

    // build panels for display
    let panels = setup_panels(app.get_instance(),window_size,app.get_renderpass(),offscreen.get_color_view(),&app.get_sampler());

    // build main background panel.

    Model{
        app,
        push_params,
        cubes,
        timer,
        offscreen,
        panels
    }
}

fn draw(_app:&Yoi, _model:&mut Model) {
    let app = &mut _model.app;
    let instance = app.get_instance();
    let push_params = _model.push_params;
    let offscreen = &mut _model.offscreen;

    let panels = &mut _model.panels;

    /*
     offscreen.begin(app.get_instance(),*cb);
            scene_gp.bind_pipeline(app.get_instance(),*cb);
            scene_mesh.draw(app.get_instance(), *cb);
            offscreen.end(app.get_instance(),*cb);

     */

    //let panels = &mut _model.panels;
    let grid = &mut _model.cubes;
    app.record_commands(app.get_instance(),|cb,fb| unsafe {
        let consts = as_u8_slice(&push_params);
        {
            offscreen.begin(instance,*cb);
            grid.gp.bind_pipeline(instance,*cb);

            instance.push_consts(*cb,grid.gp.get_pipeline_layout(),vk::ShaderStageFlags::ALL,0,consts);

            grid.ds.bind_descriptor_set(instance,*cb,vk::PipelineBindPoint::GRAPHICS,grid.gp.get_pipeline_layout(),0);

            grid.mesh.draw(app.get_instance(),*cb);
            offscreen.end(app.get_instance(),*cb);
        }

        app.start_main_renderpass(cb, fb);

        {
            panels.gp.bind_pipeline(app.get_instance(), *cb);

            instance.push_consts(*cb,panels.gp.get_pipeline_layout(),vk::ShaderStageFlags::ALL,0,consts);

            for i in 0..NUM_PANELS {
                let offset = (i * 64) as u32;
                panels.ds.bind_descriptor_set_with_offsets(
                    app.get_instance(),
                    *cb,
                    vk::PipelineBindPoint::GRAPHICS,
                    panels.gp.get_pipeline_layout(),
                    0,
                    &[offset]
                );

                panels.mesh.draw(app.get_instance(),*cb);
            }
        }

        // render grid.

        /*
         {
            grid.gp.bind_pipeline(instance,*cb);
            let consts = as_u8_slice(&push_params);
            instance.push_consts(*cb,grid.gp.get_pipeline_layout(),vk::ShaderStageFlags::ALL,0,consts);

            grid.ds.bind_descriptor_set(instance,*cb,vk::PipelineBindPoint::GRAPHICS,grid.gp.get_pipeline_layout(),0);

            grid.mesh.draw(app.get_instance(),*cb);
        }
         */


        app.end_renderpass(cb);
    });
    app.present();
    app.update();

    // update time.
    app.update_timer();
    _model.push_params.params.x = app.get_elapsed_time();
}

//// funcs ///

/// build camera for the cube field.
fn build_cube_camera(instance:&Rc<Vulkan>)->VkBuffer{
    let mut cam = Camera::new();
    cam.create_perspective(70.0,1024.0 / 768.0,0.1,10000.0);

    let mut cam_buffer = VkBuffer::new();
    cam_buffer.set_arbitrary_data(instance,cam.getMatrices());

    cam_buffer

}

/// sets up the main parameters needed for this sample.
fn setup_parameters(instance:&Rc<Vulkan>)->VkBuffer{

    let p = 15.0 as f32;
    let params = Params{
        lights:[
            Vec4::new(0.0,0.0,0.0,1.0),
            Vec4::new(0.0,0.0,0.0,1.0),
            Vec4::new(0.0,0.0,0.0,1.0),
            Vec4::new(0.0,0.0,0.0,1.0),

        ]
    };
    /*
        Vec4::new(-p, -p*0.5, -p, 1.0),
            Vec4::new(-p, -p*0.5,  p, 1.0),
            Vec4::new( p, -p*0.5,  p, 1.0),
            Vec4::new(p, -p*0.5, -p, 1.0),
     */

    let mut params_buffer = VkBuffer::new();
    params_buffer.set_arbitrary_data(instance,params);

    params_buffer

}

fn build_cube_field(instance:&Rc<Vulkan>,render_pass:&VkRenderPass,parameters:VkBuffer)->DisplayObject{
    let mut ds = VkDescriptorSet::new();

    ////// BUILD FIELD /////
    let spacing = 40;

    let mut ipositions = vec![];
    let mut icolor = vec![];
    let mut pbr_settings = vec![];

    let xgrid = 14;
    let ygrid = 14;
    let zgrid = 14;

    let color_list = [
        Vec4::new(237.0,120.0,71.0,93.0),
        Vec4::new(247.0,74.0,161.0,97.0),
        Vec4::new(77.0,198.0,247.0,97.0),
        Vec4::new(80.0,71.0,237.0,93.0),
    ];

    for i in 0..xgrid {
        for j in 0..ygrid {
            for k in 0..zgrid {
                let x = spacing * ( i - xgrid / 2 );
                let y = spacing * ( j - ygrid / 2 );
                let z = spacing * ( k - zgrid / 2 );

                let color = color_list.choose(&mut rand::thread_rng()).unwrap();

                let rot_offset = rand_float(-1.0,1.0);
                ipositions.push(Vec4::new(x as f32,y as f32,z as f32,rot_offset));
                icolor.push(color.normalize());
                pbr_settings.push(rand_vec4());

            }
        }
    }

    //////// BUILD BASE PRIMITIVE ////////////
    let prim = CubeGeo::new(10,10,10,2,2,2);

    let mut mesh = Mesh::new();
    let num_instances = ipositions.len();
    mesh.add_attribute(instance,0,prim.positions);
    mesh.add_attribute(instance,1,prim.normals);
    mesh.add_instanced_attribute(instance,2,ipositions);
    mesh.add_instanced_attribute(instance,3,icolor);
    mesh.add_instanced_attribute(instance,4,pbr_settings);

    mesh.set_instance_count(num_instances as u32);
    mesh.add_index_data(instance,prim.indices);

    //// SETUP PUSH CONSTANTS ////
    let pc_range = vk::PushConstantRange::builder()
        .stage_flags(vk::ShaderStageFlags::ALL)
        .offset(0)
        .size(size_of::<PushParams>() as u32)
        .build();

    ////// BUILD DESCRIPTOR SETS ///////
    let cam_buffer = build_cube_camera(instance);
    let mut cam_desc = UniformDescriptor::new();
    cam_desc.set_buffer(cam_buffer,0,(size_of::<CameraMatrices>()) as u64);

    // add lighting information
    let mut light_desc = UniformDescriptor::new();
    light_desc.set_buffer(parameters,0,(size_of::<Params>()) as u64);
    light_desc.set_shader_stage(vk::ShaderStageFlags::ALL);
    light_desc.set_shader_binding(1);

    ds.add_uniform_descriptor(cam_desc);
    ds.add_uniform_descriptor(light_desc);
    ds.build(instance);
    ds.update_descriptor_data(instance);
    //////// BUILD PIPELINE ////////////
    let shader = VkShader::create(
        "shaders/tryptic/cube.vert",
        "shaders/tryptic/cube.frag",instance);

    let mut gp = GraphicsPipeline::new();
    gp.link_mesh_object(&mesh);
    //gp.compile(instance,&shader,render_pass,&ds);
    gp.compile_with_push_constants(instance,&shader,render_pass, &ds,&[pc_range]);

    DisplayObject{
        mesh,
        gp,
        ds
    }
}


/// builds the panels that make up the tryptic
fn setup_panels(
    instance:&Rc<Vulkan>,
    window_size:PhysicalSize<u32>,
    render_pass:&VkRenderPass,
    render_image:&vk::ImageView,
    sampler:&vk::Sampler
)->DisplayObject {

    //// BUILD PANELS //////
    let mut panel_positions = Vec::new();

    let height = (window_size.height as f32) * 0.6;
    let width = 200;

    let p = PlaneGeo::new(width, height as i32, 2, 2);

    let mut mesh = Mesh::new();

    let mut verts = vec![];

    p.vertices.iter().for_each(|v| {
        verts.push(v.x);
        verts.push(v.y);
        verts.push(v.z);
    });

    // set the position attribute
    mesh.add_attribute_with_format(instance, verts, 0, MeshAttribFormat {
        offset: 0,
        data_format: vk::Format::R32G32B32_SFLOAT,
        stride: (sizeof::<f32>() * 3) as u32
    });

    mesh.add_attribute_with_format(instance, p.uvs, 1, MeshAttribFormat {
        offset: 0,
        data_format: vk::Format::R32G32_SFLOAT,
        stride: (sizeof::<Vec2>()) as u32
    });

    mesh.add_index_data(instance, p.indices);
    mesh.set_instance_count(2);

    for i in 0..NUM_PANELS {
        let mut mat = Matrix4::new();

        let win_half = (window_size.width / 2) as f32;

        match i {
            0 => {
                mat.translate(Vec3A::new(-win_half + (win_half / 2.0), 0.0, 0.0));
            }

            1 => {
                mat.translate(Vec3A::new(0.0, 0.0, 0.0));
            }

            2 => {
                mat.translate(Vec3A::new(win_half - (win_half / 2.0), 0.0, 0.0));
            }

            _ => {}
        }

        panel_positions.push(mat);
    }


    //// BUILD CAMERA /////
    let mut cam = Camera::new();
    cam.create_perspective(60.0, 1024.0 / 768.0, 0.1, 10000.0);
    cam.set_zoom(-55.0);

    let mut cam_buffer = VkBuffer::new();
    cam_buffer.set_arbitrary_data(instance, cam.getMatrices());

    //// BUILD PANEL MODEL MATRICES ////
    let positions = panel_positions;

    let mut data = vec![];
    for pos in positions {
        data.push(pos.get_matrix());
    }

    let buffer = generate_buffer_for_dynamic_ubo(instance, &data);

    ///////// GENERATE DESCRIPTOR SET //////////////

    let mut ds = VkDescriptorSet::new();

    // generate descriptor for camera
    let mut cam_desc = UniformDescriptor::new();
    cam_desc.set_shader_binding(0);
    cam_desc.set_buffer(cam_buffer, 0, size_of::<CameraMatrices>() as u64);

    // generate dynamic descriptor for panel positions
    let mut desc = DynamicUniformDescriptor::new();
    desc.set_shader_stage(vk::ShaderStageFlags::VERTEX);
    desc.set_shader_binding(1);
    desc.set_buffer(buffer, 0, (size_of::<Mat4>()) as u64);

    // generate descriptor for image we want to render.
    let mut img_desc = TextureSamplerDescriptor::new();
    img_desc.set_image(*render_image,*sampler);
    img_desc.set_shader_binding(2);

    //// FINISH INIT DESCRIPTOR SET ////
    ds.add_uniform_descriptor(cam_desc);
    ds.add_dynamic_uniform_descriptor(desc);
    ds.add_texture_sampler_descriptor(img_desc);

    ds.build(instance);
    ds.update_descriptor_data(instance);

    ////// BUILD PIPELINE //////
    let mut gp = GraphicsPipeline::new();

    gp.link_mesh_object(&mesh);

    // load the shader
    let mut shader = VkShader::create("shaders/tryptic/panel.vert", "shaders/tryptic/panel.frag", instance);

    let pc_range = vk::PushConstantRange::builder()
        .stage_flags(vk::ShaderStageFlags::ALL)
        .offset(0)
        .size(size_of::<PushParams>() as u32)
        .build();
    //gp.compile(instance, &shader, render_pass, &ds);
    gp.compile_with_push_constants(instance,&shader,&render_pass,&ds,&[pc_range]);
    DisplayObject {
        mesh,
        gp,
        ds
    }
}