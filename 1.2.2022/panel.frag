#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (set = 0, binding = 2) uniform sampler2D tex;

layout(push_constant) uniform PushConsts {
    vec4 params; // x = time, y + z = resolution
}pushConsts;

layout(location = 0) out vec4 glFragColor;
layout(location = 0) in vec4 vPos;

/// Luma code courtesy of @hughsk
// https://github.com/hughsk/glsl-dither
//https://github.com/hughsk/glsl-luma

float luma(vec3 color) {
    return dot(color, vec3(0.299, 0.587, 0.114));
}

float luma(vec4 color) {
    return dot(color.rgb, vec3(0.299, 0.587, 0.114));
}

float dither4x4(vec2 position, float brightness) {
    int x = int(mod(position.x, 4.0));
    int y = int(mod(position.y, 4.0));
    int index = x + y * 4;
    float limit = 0.0;

    if (x < 8) {
        if (index == 0) limit = 0.0625;
        if (index == 1) limit = 0.5625;
        if (index == 2) limit = 0.1875;
        if (index == 3) limit = 0.6875;
        if (index == 4) limit = 0.8125;
        if (index == 5) limit = 0.3125;
        if (index == 6) limit = 0.9375;
        if (index == 7) limit = 0.4375;
        if (index == 8) limit = 0.25;
        if (index == 9) limit = 0.75;
        if (index == 10) limit = 0.125;
        if (index == 11) limit = 0.625;
        if (index == 12) limit = 1.0;
        if (index == 13) limit = 0.5;
        if (index == 14) limit = 0.875;
        if (index == 15) limit = 0.375;
    }

    return brightness < limit ? 0.0 : 1.0;
}

vec3 dither4x4(vec2 position, vec3 color) {
    return color * dither4x4(position, luma(color));
}

vec4 dither4x4(vec2 position, vec4 color) {
    return vec4(color.rgb * dither4x4(position, luma(color)), 1.0);
}
void main() {
    //glFragColor = vec4(1.0, vColor.x, 0.0, 1.0);

    vec2 uv = gl_FragCoord.xy / pushConsts.params.yz;
    vec4 img = texture(tex,uv);

    vec4 dith = dither4x4(gl_FragCoord.xy / vPos.xy,img);
    glFragColor = dith;
}
