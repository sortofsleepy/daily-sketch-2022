#version 450
#extension GL_ARB_separate_shader_objects : enable


layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(push_constant) uniform PushConsts {
    vec4 params; // x = time, y + z = resolution
}pushConsts;

layout(binding = 1) uniform UBOParams {
    vec4 lights[4];
} params;

vec3 rotateX(vec3 p, float theta){
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}

vec3 rotateY(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec4 iPosition;
layout(location = 3) in vec4 iColor;
layout(location = 4) in vec4 pbr_settings;

layout(location = 0) out vec4 oColor;
layout(location = 1) out vec4 oPBR;
layout(location = 2) out vec4 oNormal;
layout(location = 3) out vec3 oPos;


out gl_PerVertex
{
    vec4 gl_Position;
    float gl_PointSize;
};

void main() {
    vec3 pos = position.xyz;

    pos.xyz = rotateX(pos.xyz,sin(pushConsts.params.x * iPosition.a));
    pos.xyz = rotateY(pos.xyz,cos(pushConsts.params.x * iPosition.a));
    pos.xyz = rotateZ(pos.xyz,sin(pushConsts.params.x * iPosition.a));

    pos += iPosition.xyz;

    oColor = iColor;
    oPBR = pbr_settings;
    oNormal = normal;
    oPos = pos.xyz;


    gl_Position = ubo.proj * ubo.view * vec4(pos,1.);

}