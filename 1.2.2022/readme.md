1.2.2022
===

Pseudo dithering / Pseudo PBR + Tryptic format

Running 
===
At the moment, the core components that enable this to run are not on Cargo so getting this to run will be a bit of a pain. 

* [Install the Vulkan SDK](https://vulkan.lunarg.com/)
* [Install Rust](https://www.rust-lang.org/)
* Clone my [framework](https://gitlab.com/sortofsleepy/yoi/-/tree/rust)
* The files are technically already there under the `sketches` folder, but if they're not, drop `tryptic.rs` into `src/bin/` and the shaders
into `shaders/tryptic`
* `cd` into the sketches folder and run `cargo --bin tryptic`
