#version 450
#extension GL_ARB_separate_shader_objects : enable


layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(binding = 1) uniform UniformBufferObject2 {
    mat4 model;
} panel;


layout(location = 0) in vec3 position;

layout(location = 0) out vec4 vPos;



out gl_PerVertex
{
    vec4 gl_Position;
    float gl_PointSize;
};

void main() {
    vPos = vec4(position,1.);
    gl_Position = ubo.proj * ubo.view * panel.model * vec4(position,1.);

}