1.1.2022
===

あけましておめでとう！
[Relevant Tweet](https://twitter.com/sortofsleepy/status/1477423815000944643)

__Notes__

* The asset of the Tokyo cityscape is not included due to size and how it bumps up the size of the Houdini file. You can extract your own map tile and import in it's place via @eliemichel's [MapsModelsImporter](https://github.com/eliemichel/MapsModelsImporter) plugin.

* background is also not included due to size
