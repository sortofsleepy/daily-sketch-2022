1.8.2022 - うんこ
====


An exploration into how dynamic uniform buffers work in WebGPU. 

The characters for the word うんこ will cycle through. Each drawn pixel is an individual element that gets rendered and each pixel will receive it's own 
set of uniform values which will tell it where to position itself as well as whether it should turn itself "on".

In WebGPU and other lower level apis like Vulkan, there are no convenient functions like `uniform4fv` and similar that will let you send values
to your shader automatically, everything essentially has to be bundled into a buffer and sent to the shader in that format. This can get tricky in some situations. Hence
Dynamic uniform buffers to the rescue which essentially let you bundle all the uniform values you need into just one buffer and you can access each set of data with an offset value.

Running
===
This can currently only be run on development versions of the major browsers. Tested on Chrome Canary. Make sure to enable WebGPU as needed.

Code 
===
While this sample does contain a lot of bare metal code, it also uses a mini framework I've been building on and off so there are some abstractions to look through in order to totally understand everything.
Hopefully it's clear and please excuse the mess :p

Why not instanced attributes or Textures?
===
Instanced attributes are certainly another alternate method that could be used to re-create this particular sample. Use of Textures is another way too. 
This is more for demonstration purposes and to increase my understanding of things.

Other reading
===
* [See the proposal for the dynamic uniform buffer spec here](https://github.com/gpuweb/gpuweb/issues/116)
* [A Vulkan sample illustrating the same concept](https://github.com/SaschaWillems/Vulkan/blob/master/examples/dynamicuniformbuffer/README.md)
* [WebGPU Samples](https://austin-eng.com/webgpu-samples/)
