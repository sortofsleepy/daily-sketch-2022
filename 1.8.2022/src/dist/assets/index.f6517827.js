import{S as $,g as Q}from"./vendor.9c7e81bb.js";const K=function(){const t=document.createElement("link").relList;if(t&&t.supports&&t.supports("modulepreload"))return;for(const l of document.querySelectorAll('link[rel="modulepreload"]'))a(l);new MutationObserver(l=>{for(const n of l)if(n.type==="childList")for(const i of n.addedNodes)i.tagName==="LINK"&&i.rel==="modulepreload"&&a(i)}).observe(document,{childList:!0,subtree:!0});function r(l){const n={};return l.integrity&&(n.integrity=l.integrity),l.referrerpolicy&&(n.referrerPolicy=l.referrerpolicy),l.crossorigin==="use-credentials"?n.credentials="include":l.crossorigin==="anonymous"?n.credentials="omit":n.credentials="same-origin",n}function a(l){if(l.ep)return;l.ep=!0;const n=r(l);fetch(l.href,n)}};K();const J="float32x4",ee=GPUShaderStage.VERTEX,te=GPUShaderStage.FRAGMENT;GPUShaderStage.COMPUTE;const re="triangle-list",B="depth24plus-stencil8",ae="bgra8unorm",Y=GPUTextureUsage.RENDER_ATTACHMENT;GPUTextureUsage.RENDER_ATTACHMENT;const X=GPUBufferUsage.UNIFORM,V="uniform",q=GPUBufferUsage.COPY_DST,C="store";function le(e,t={swapchainFormat:"bgra8unorm",canvasWidth:window.innerWidth,canvasHeight:window.innerHeight,canvasAttachTo:document.body}){if(!navigator.gpu){alert("Your browser does not support WebGPU");return}navigator.gpu.requestAdapter().then(r=>{r.requestDevice().then(a=>{e(new ne(a,r,t))})})}class ne{constructor(t,r,a){this.resizing=!1,this.encoders=[],this.resizeCbs=[],this.presentationSize=[0,0],this.device=t,this.settings=a,this.canvas=a.canvas!==void 0?a.canvas:document.createElement("canvas"),this.canvas.width=a.canvasWidth,this.canvas.height=a.canvasHeight,this.appendTo(a.canvasAttachTo),this.context=this.canvas.getContext("webgpu"),this.presentationFormat=this.context.getPreferredFormat(r),this.presentationSize=[this.canvas.width,this.canvas.height],this.swapchain=this.context.configure({device:t,format:this.presentationFormat}),this.defaultDepthTexture=this.device.createTexture({size:{width:this.getCanvasWidth(),height:this.getCanvasHeight()},format:B,usage:Y}),window.addEventListener("resize",this._onResize.bind(this))}viewport(t,{x:r=0,y:a=0,width:l=this.canvas.width,height:n=this.canvas.height,minDepth:i=0,maxDepth:f=1}={}){t.setViewport(r,a,l,n,i,f)}rebuildSwapchain(){let t=this.device;this.settings,this.swapchain=this.context.configure({device:t,format:this.presentationFormat}),this.defaultDepthTexture=this.device.createTexture({size:{width:this.getCanvasWidth(),height:this.getCanvasHeight()},format:B,usage:Y})}setCanvasSize(t,r){return this.canvas.width=t,this.canvas.height=r,this}appendTo(t){return t.appendChild(this.canvas),this}createCommandEncoder(){return this.device.createCommandEncoder()}getCurrentTexture(){return this.context.getCurrentTexture().createView()}getDefaultDepthView(){return this.defaultDepthTexture.createView()}createBuffer(t,r,a){return this.device.createBuffer({size:t,usage:r,mappedAtCreation:a})}addResizeCallback(t){this.resizeCbs.push(t)}getCanvasWidth(){return this.canvas.width}getCanvasHeight(){return this.canvas.height}addCommandEncoder(t){return this.encoders.push(t),this}dispatchEncoders(){}_onResize(t){if(this.device,this.resizing=!0,!(this.canvas.parentElement!==document.body?this.canvas.parentElement:!1))this.canvas.width=window.innerWidth,this.canvas.height=window.innerHeight;else{let a=getComputedStyle(this.canvas.parentElement);this.canvas.width=parseInt(a.width),this.canvas.height=parseInt(a.height)}this.defaultDepthTexture=this.device.createTexture({size:{width:this.getCanvasWidth(),height:this.getCanvasHeight()},format:B,usage:Y}),this.resizeCbs.forEach(a=>{a()}),setTimeout(()=>{this.rebuildSwapchain(),this.resizing=!1},400)}}function ie(){let e=["./assets/u.jpg","./assets/n.jpg","./assets/ko.jpg"];return e=e.map(t=>new Promise((r,a)=>{let l=new Image;l.src=t,l.onload=()=>{r(l)},l.onerror=n=>{a(n)}})),Promise.all(e)}class se{constructor(){this.canvas=document.createElement("canvas"),this.ctx=this.canvas.getContext("2d")}read_image(t,r=t.width/2,a=t.height/2){return this.canvas.width=r,this.canvas.height=a,this.ctx.drawImage(t,0,0,r,a),this.ctx.getImageData(0,0,r,a)}debug_image_data(t){this.ctx.putImageData(t,0,0),document.body.appendChild(this.canvas)}}class N{constructor(t,{arrayType:r="Float32Array",data:a=null,usage:l=X|q,size:n=1}={}){this.arrayType="Float32Array",this.usage=X|q,this.byteLength=0,this.instance=t,this.arrayType=r,this.usage=l,a===null?this.bufferObj=this.instance.device.createBuffer({size:n,usage:this.usage}):this.set({data:a,arrayType:r})}generateUniformBuffer(t,r=4){this.bufferObj=this.instance.device.createBuffer({size:t*r,usage:GPUBufferUsage.UNIFORM|q})}updateData(t=null){t!==null?this.instance.device.queue.writeBuffer(this.bufferObj,0,t.buffer,t.byteOffset,t.byteLength):this.instance.device.queue.writeBuffer(this.bufferObj,0,this.data.buffer,this.data.byteOffset,this.data.byteLength)}set({data:t=null,usage:r=GPUBufferUsage.VERTEX,arrayType:a="Float32Array"}={}){t!==null&&(t=new window[a](t),this.bufferObj=this.instance.device.createBuffer({size:t.byteLength,usage:r,mappedAtCreation:!0}),new window[a](this.bufferObj.getMappedRange()).set(t),this.bufferObj.unmap(),this.byteLength=t.byteLength,this.data=t)}getByteLength(){return this.byteLength}getObject(){return this.bufferObj}}class fe{constructor(t){this.attributes=[],this.drawParameters={vertexCount:0,instanceCount:1,firstVertex:0,firstInstance:0,indexCount:0,baseVertex:0,firstIndex:0},this.instance=t}setInstanceCount(t){return this.drawParameters.instanceCount=t,this}addAttribute(t,r,{id:a=this.attributes.length-1}={}){let l=new N(this.instance);return l.set({data:r}),this.attributes.push(l),this}addAttributeBuffer(t,r){return this.attributes.push(r),this}setVertexCount(t){return this.drawParameters.vertexCount=t,this}draw(t){this.attributes.forEach((r,a)=>{t.setVertexBuffer(a,r.getObject())}),this.indexBuffer!==void 0?(t.setIndexBuffer(this.indexBuffer.getObject(),"uint16"),t.drawIndexed(this.drawParameters.indexCount,this.drawParameters.instanceCount,this.drawParameters.firstInstance,this.drawParameters.baseVertex,this.drawParameters.firstInstance)):t.draw(this.drawParameters.vertexCount,this.drawParameters.instanceCount,this.drawParameters.firstVertex,this.drawParameters.firstInstance)}setIndexData(t){this.instance.device,t=new Uint16Array(t);let r=t.length,a=new N(this.instance);a.set({data:t,usage:GPUBufferUsage.INDEX,arrayType:"Uint16Array"}),this.drawParameters.indexCount=r,this.indexBuffer=a}}function ce(e,t,{vertexEntry:r="main",fragmentEntry:a="main",buffers:l=[],targets:n=[]}={}){return n.length===0&&n.push({format:ae,blend:{alpha:{srcFactor:"src-alpha",dstFactor:"one-minus-src-alpha",operation:"add"},color:{srcFactor:"src-alpha",dstFactor:"one-minus-src-alpha",operation:"add"}}}),{type:"WGSLShader",vertexStage:{module:e.createShaderModule({code:t.vertex}),entryPoint:r,buffers:l},fragmentStage:{module:e.createShaderModule({code:t.fragment}),entryPoint:a,targets:n}}}let Z=`   
   
        struct Uniforms2 {
            xpos:f32;
            ypos:f32;
            pixelStatus:f32;
            offset:f32;
        };
        [[binding(0), group(1)]] var<uniform> params : Uniforms2;
        `,k=`
        struct AppUniforms {
           params: vec4<f32>;
        };
        [[binding(1), group(0)]] var<uniform> env : AppUniforms;

`,xe=`

fn colormap_red(x: f32) -> f32 {
    var x_1: f32;

    x_1 = x;
    let _e5 = x_1;
    if ((_e5 < 0.0)) {
        {
            return (4.0 / 255.0);
        }
    } else {
        let _e11 = x_1;
        if ((_e11 < (20049.0 / 182979.0))) {
            {
                let _e17 = x_1;
                return (((829.7899780273438 * _e17) + 54.5099983215332) / 255.0);
            }
        } else {
            {
                return 1.0;
            }
        }
    }
}

fn colormap_green(x_2: f32) -> f32 {
    var x_3: f32;

    x_3 = x_2;
    let _e5 = x_3;
    if ((_e5 < (20049.0 / 82979.0))) {
        {
            return 0.0;
        }
    } else {
        let _e11 = x_3;
        if ((_e11 < (327013.0 / 810990.0))) {
            {
                let _e19 = x_3;
                return ((((8546482651136.0 / 10875673600.0) * _e19) - (2064961372160.0 / 10875673600.0)) / 255.0);
            }
        } else {
            let _e27 = x_3;
            if ((_e27 <= 1.0)) {
                {
                    let _e33 = x_3;
                    return ((((103806720.0 / 483977.0) * _e33) + (19607416.0 / 483977.0)) / 255.0);
                }
            } else {
                {
                    return 1.0;
                }
            }
        }
    }
}

fn colormap_blue(x_4: f32) -> f32 {
    var x_5: f32;

    x_5 = x_4;
    let _e5 = x_5;
    if ((_e5 < 0.0)) {
        {
            return (54.0 / 255.0);
        }
    } else {
        let _e11 = x_5;
        if ((_e11 < (7249.0 / 82979.0))) {
            {
                let _e17 = x_5;
                return (((829.7899780273438 * _e17) + 54.5099983215332) / 255.0);
            }
        } else {
            let _e23 = x_5;
            if ((_e23 < (20049.0 / 82979.0))) {
                {
                    return (127.0 / 255.0);
                }
            } else {
                let _e31 = x_5;
                if ((_e31 < (327013.0 / 810990.0))) {
                    {
                        let _e37 = x_5;
                        return (((792.0225219726563 * _e37) - 64.36479187011719) / 255.0);
                    }
                } else {
                    {
                        return 1.0;
                    }
                }
            }
        }
    }
}

fn colormap(x_6: f32) -> vec4<f32> {
    var x_7: f32;

    x_7 = x_6;
    let _e6 = x_7;
    let _e7 = colormap_red(_e6 * 20.0);
    let _e9 = x_7 * 2.0;
    let _e10 = colormap_green(_e9);
    let _e12 = x_7 / 2.0;
    let _e13 = colormap_blue(_e12);
    return vec4<f32>(_e7, _e10, _e13, 1.0);
}

fn rand(n: vec2<f32>) -> f32 {
    var n_1: vec2<f32>;

    n_1 = n;
    let _e9 = n_1;
    let _e18 = n_1;
    let _e30 = n_1;
    let _e39 = n_1;
    return fract((sin(dot(_e39, vec2<f32>(12.989800453186035, 4.14139986038208))) * 43758.546875));
}

fn noise(p: vec2<f32>) -> f32 {
    var p_1: vec2<f32>;
    var ip: vec2<f32>;
    var u: vec2<f32>;
    var res: f32;

    p_1 = p;
    let _e6 = p_1;
    ip = floor(_e6);
    let _e10 = p_1;
    u = fract(_e10);
    let _e13 = u;
    let _e14 = u;
    let _e18 = u;
    u = ((_e13 * _e14) * (vec2<f32>(3.0) - (2.0 * _e18)));
    let _e24 = ip;
    let _e25 = rand(_e24);
    let _e26 = ip;
    let _e31 = ip;
    let _e36 = rand((_e31 + vec2<f32>(1.0, 0.0)));
    let _e37 = u;
    let _e40 = ip;
    let _e41 = rand(_e40);
    let _e42 = ip;
    let _e47 = ip;
    let _e52 = rand((_e47 + vec2<f32>(1.0, 0.0)));
    let _e53 = u;
    let _e56 = ip;
    let _e61 = ip;
    let _e66 = rand((_e61 + vec2<f32>(0.0, 1.0)));
    let _e67 = ip;
    let _e72 = ip;
    let _e77 = rand((_e72 + vec2<f32>(1.0, 1.0)));
    let _e78 = u;
    let _e80 = ip;
    let _e85 = ip;
    let _e90 = rand((_e85 + vec2<f32>(0.0, 1.0)));
    let _e91 = ip;
    let _e96 = ip;
    let _e101 = rand((_e96 + vec2<f32>(1.0, 1.0)));
    let _e102 = u;
    let _e105 = u;
    let _e108 = ip;
    let _e109 = rand(_e108);
    let _e110 = ip;
    let _e115 = ip;
    let _e120 = rand((_e115 + vec2<f32>(1.0, 0.0)));
    let _e121 = u;
    let _e124 = ip;
    let _e125 = rand(_e124);
    let _e126 = ip;
    let _e131 = ip;
    let _e136 = rand((_e131 + vec2<f32>(1.0, 0.0)));
    let _e137 = u;
    let _e140 = ip;
    let _e145 = ip;
    let _e150 = rand((_e145 + vec2<f32>(0.0, 1.0)));
    let _e151 = ip;
    let _e156 = ip;
    let _e161 = rand((_e156 + vec2<f32>(1.0, 1.0)));
    let _e162 = u;
    let _e164 = ip;
    let _e169 = ip;
    let _e174 = rand((_e169 + vec2<f32>(0.0, 1.0)));
    let _e175 = ip;
    let _e180 = ip;
    let _e185 = rand((_e180 + vec2<f32>(1.0, 1.0)));
    let _e186 = u;
    let _e189 = u;
    res = mix(mix(_e125, _e136, _e137.x), mix(_e174, _e185, _e186.x), _e189.y);
    let _e193 = res;
    let _e194 = res;
    return (_e193 * _e194);
}

fn fbm(p_2: vec2<f32>) -> f32 {
    var p_3: vec2<f32>;
    var f: f32 = 0.0;

    p_3 = p_2;
    let _e8 = f;
    let _e10 = p_3;
    let _e11 = env.params.x;
    let _e14 = p_3;
    let _e15 = env.params.x;
    let _e18 = noise((_e14 + vec2<f32>(_e15)));
    f = (_e8 + (0.5 * _e18));
    let _e21 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e21) * 2.0199999809265137);
    let _e25 = f;
    let _e28 = p_3;
    let _e29 = noise(_e28);
    f = (_e25 + (0.03125 * _e29));
    let _e32 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e32) * 2.009999990463257);
    let _e36 = f;
    let _e39 = p_3;
    let _e40 = noise(_e39);
    f = (_e36 + (0.25 * _e40));
    let _e43 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e43) * 2.0299999713897705);
    let _e47 = f;
    let _e50 = p_3;
    let _e51 = noise(_e50);
    f = (_e47 + (0.125 * _e51));
    let _e54 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e54) * 2.009999990463257);
    let _e58 = f;
    let _e61 = p_3;
    let _e62 = noise(_e61);
    f = (_e58 + (0.0625 * _e62));
    let _e65 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e65) * 2.0399999618530273);
    let _e69 = f;
    let _e71 = p_3;
    let _e73 = env.params.x;
    let _e77 = p_3;
    let _e79 = env.params.x;
    let _e83 = noise((_e77 + vec2<f32>(sin(_e79))));
    f = (_e69 + (0.015625 * _e83));
    let _e86 = f;
    return (_e86 / 0.96875);
}

fn pattern(p_4: vec2<f32>) -> f32 {
    var p_5: vec2<f32>;

    p_5 = p_4;
    let _e6 = p_5;
    let _e7 = p_5;
    let _e9 = p_5;
    let _e10 = fbm(_e9);
    let _e13 = p_5;
    let _e15 = p_5;
    let _e16 = fbm(_e15);
    let _e19 = fbm((_e13 + vec2<f32>(_e16)));
    let _e22 = p_5;
    let _e23 = p_5;
    let _e25 = p_5;
    let _e26 = fbm(_e25);
    let _e29 = p_5;
    let _e31 = p_5;
    let _e32 = fbm(_e31);
    let _e35 = fbm((_e29 + vec2<f32>(_e32)));
    let _e38 = fbm((_e22 + vec2<f32>(_e35)));
    return _e38;
}
`;var he={vertex:`
        struct Uniforms {
            modelViewProjectionMatrix : mat4x4<f32>;
        };
        [[binding(0), group(0)]] var<uniform> uniforms : Uniforms;
        ${k}
        ${Z}
        
        struct VertexOutput {
          [[builtin(position)]] Position : vec4<f32>;
          [[location(0)]] Coords: vec2<f32>;
        };
        
        [[stage(vertex)]]
        fn main(
            [[location(0)]] position : vec4<f32>
        ) -> VertexOutput {
          var output : VertexOutput;
          
          var pos = position;
          pos.x = pos.x + params.xpos;
          pos.y = pos.y + params.ypos;
         
          
          output.Position = uniforms.modelViewProjectionMatrix * pos;
          output.Coords = vec2(pos.xy);
          return output;
        }
    `,fragment:`
        ${Z}
        ${k}
        ${xe}
        
        fn solveColor(uv: vec2<f32>) -> vec3<f32> {
            var uv_1: vec2<f32>;
            var col: vec3<f32>;
        
            uv_1 = uv;
            let _e9 = env.params.x;
            let _e10 = uv_1;
            let _e22 = env.params.x;
            let _e23 = uv_1;
            col = (vec3<f32>(0.5) + (0.5 * sin(((vec3<f32>(_e22) + _e23.xyx) + vec3<f32>(f32(0), f32(2), f32(4))))));
            let _e60 = col;
            col = mix(normalize(vec3<f32>(128.0, 127.0, 238.0)), _e60, vec3<f32>(1.0));
            let _e84 = col;
            col = mix(normalize(vec3<f32>(105.0, 180.0, 229.0)), _e84, vec3<f32>(0.6000000238418579));
            let _e88 = col;
            return _e88;
        }
        
        [[stage(fragment)]]
        fn main(
            [[location(0)]] Coords:vec2<f32>
        ) -> [[location(0)]] vec4<f32> {
            var resolution:vec2<f32> = env.params.yz;
            var time:f32 = env.params.x;
            var status:f32 = params.pixelStatus;
            var center = vec2(0.5,0.5);
            var speed = 2.5;

      
            var uv:vec2<f32> = Coords.xy/resolution.xy;
            
            
            /// BUILD "OFF" MODE COLOR ///// 
            var shade:f32 = pattern(uv);
            var col:vec4<f32> = vec4(colormap(shade).rgb, shade);
            
            
            /// BUILD "ON" mode color ///// 
            var texcol = vec3(0.);
            var invAr:f32 = resolution.y / resolution.x;
            var x:f32 = (center.x-uv.x);
            var y:f32 = (center.y-uv.y) * invAr;

             var r:f32 = -sqrt(x*x + y*y); //uncoment this line to symmetric ripples
 
            var z:f32 = 1.0 + 0.5*sin((r + time * speed) / 0.1);
            var distanceFromCenter:f32 = sqrt(x * x + y * y);
            var sinArg:f32 = distanceFromCenter * 10.0 - time;
            var slope:f32 = cos(sinArg) ;
    
            texcol.x = texcol.x + sin(z) * 2.0 * params.offset;
      
            /// finalize colors //// 
            var off:vec3<f32> = vec3(col.xyz);
            var on:vec3<f32> = normalize(col).xyz + vec4(solveColor(uv + normalize(vec2(x, y)) * slope + texcol.xy),1.).xyz;
            
            
            
            return vec4(mix(off,on,status),1.0);
       }
    `};class de{constructor(t=1,r=1,a=1,l=1,n){this.vertices=[],this.normals=[],this.indices=[],this.uvs=[],this.customSize=3,this.numComponents=0;let i=n&&n.quads?n.quads:!1;n&&n.resolution&&n.resolution;for(let c=0;c<=l;c++)for(let x=0;x<=a;x++){let h=x/a,d=c/l,o=-t/2+h*t,_=r/2-d*r;this.vertices.push(o,_,0,1),this.uvs.push(h,1-d),this.normals.push(0,0,1,1),c<l&&x<a&&(i?this.indices.push([c*(a+1)+x,(c+1)*(a+1)+x,(c+1)*(a+1)+x+1,c*(a+1)+x+1]):(this.indices.push([c*(a+1)+x,(c+1)*(a+1)+x+1,c*(a+1)+x+1]),this.indices.push([(c+1)*(a+1)+x+1,c*(a+1)+x,(c+1)*(a+1)+x])))}let f=[];this.indices.forEach(c=>{c.forEach(x=>{f.push(x)})}),this.indices=f,this.numComponents=this.indices.length}}var y={};y.EPSILON=1e-6;y.ARRAY_TYPE=typeof Float32Array!="undefined"?Float32Array:Array;y.RANDOM=Math.random;y.ENABLE_SIMD=!1;y.SIMD_AVAILABLE=y.ARRAY_TYPE===Float32Array&&"SIMD"in window;y.USE_SIMD=y.ENABLE_SIMD&&y.SIMD_AVAILABLE;y.setMatrixArrayType=function(e){y.ARRAY_TYPE=e};var oe=Math.PI/180;y.toRadian=function(e){return e*oe};y.equals=function(e,t){return Math.abs(e-t)<=y.EPSILON*Math.max(1,Math.abs(e),Math.abs(t))};var s=window.SIMD!==void 0?window.SIMD:{},p={scalar:{},SIMD:{}};p.create=function(){var e=new y.ARRAY_TYPE(16);return e[0]=1,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=1,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=1,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e};p.clone=function(e){var t=new y.ARRAY_TYPE(16);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t[3]=e[3],t[4]=e[4],t[5]=e[5],t[6]=e[6],t[7]=e[7],t[8]=e[8],t[9]=e[9],t[10]=e[10],t[11]=e[11],t[12]=e[12],t[13]=e[13],t[14]=e[14],t[15]=e[15],t};p.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[4]=t[4],e[5]=t[5],e[6]=t[6],e[7]=t[7],e[8]=t[8],e[9]=t[9],e[10]=t[10],e[11]=t[11],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15],e};p.fromValues=function(e,t,r,a,l,n,i,f,c,x,h,d,o,_,m,v){var F=new y.ARRAY_TYPE(16);return F[0]=e,F[1]=t,F[2]=r,F[3]=a,F[4]=l,F[5]=n,F[6]=i,F[7]=f,F[8]=c,F[9]=x,F[10]=h,F[11]=d,F[12]=o,F[13]=_,F[14]=m,F[15]=v,F};p.set=function(e,t,r,a,l,n,i,f,c,x,h,d,o,_,m,v,F){return e[0]=t,e[1]=r,e[2]=a,e[3]=l,e[4]=n,e[5]=i,e[6]=f,e[7]=c,e[8]=x,e[9]=h,e[10]=d,e[11]=o,e[12]=_,e[13]=m,e[14]=v,e[15]=F,e};p.identity=function(e){return e[0]=1,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=1,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=1,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e};p.scalar.transpose=function(e,t){if(e===t){var r=t[1],a=t[2],l=t[3],n=t[6],i=t[7],f=t[11];e[1]=t[4],e[2]=t[8],e[3]=t[12],e[4]=r,e[6]=t[9],e[7]=t[13],e[8]=a,e[9]=n,e[11]=t[14],e[12]=l,e[13]=i,e[14]=f}else e[0]=t[0],e[1]=t[4],e[2]=t[8],e[3]=t[12],e[4]=t[1],e[5]=t[5],e[6]=t[9],e[7]=t[13],e[8]=t[2],e[9]=t[6],e[10]=t[10],e[11]=t[14],e[12]=t[3],e[13]=t[7],e[14]=t[11],e[15]=t[15];return e};p.SIMD.transpose=function(e,t){var r=window.SIMD,a,l,n,i,f,c,x,h,d,o;return a=r.Float32x4.load(t,0),l=r.Float32x4.load(t,4),n=r.Float32x4.load(t,8),i=r.Float32x4.load(t,12),f=r.Float32x4.shuffle(a,l,0,1,4,5),c=r.Float32x4.shuffle(n,i,0,1,4,5),x=r.Float32x4.shuffle(f,c,0,2,4,6),h=r.Float32x4.shuffle(f,c,1,3,5,7),r.Float32x4.store(e,0,x),r.Float32x4.store(e,4,h),f=r.Float32x4.shuffle(a,l,2,3,6,7),c=r.Float32x4.shuffle(n,i,2,3,6,7),d=r.Float32x4.shuffle(f,c,0,2,4,6),o=r.Float32x4.shuffle(f,c,1,3,5,7),r.Float32x4.store(e,8,d),r.Float32x4.store(e,12,o),e};p.transpose=y.USE_SIMD?p.SIMD.transpose:p.scalar.transpose;p.scalar.invert=function(e,t){var r=t[0],a=t[1],l=t[2],n=t[3],i=t[4],f=t[5],c=t[6],x=t[7],h=t[8],d=t[9],o=t[10],_=t[11],m=t[12],v=t[13],F=t[14],M=t[15],E=r*f-a*i,S=r*c-l*i,b=r*x-n*i,g=a*c-l*f,w=a*x-n*f,I=l*x-n*c,u=h*v-d*m,D=h*F-o*m,A=h*M-_*m,T=d*F-o*v,O=d*M-_*v,L=o*M-_*F,P=E*L-S*O+b*T+g*A-w*D+I*u;return P?(P=1/P,e[0]=(f*L-c*O+x*T)*P,e[1]=(l*O-a*L-n*T)*P,e[2]=(v*I-F*w+M*g)*P,e[3]=(o*w-d*I-_*g)*P,e[4]=(c*A-i*L-x*D)*P,e[5]=(r*L-l*A+n*D)*P,e[6]=(F*b-m*I-M*S)*P,e[7]=(h*I-o*b+_*S)*P,e[8]=(i*O-f*A+x*u)*P,e[9]=(a*A-r*O-n*u)*P,e[10]=(m*w-v*b+M*E)*P,e[11]=(d*b-h*w-_*E)*P,e[12]=(f*D-i*T-c*u)*P,e[13]=(r*T-a*D+l*u)*P,e[14]=(v*S-m*g-F*E)*P,e[15]=(h*g-d*S+o*E)*P,e):null};p.SIMD.invert=function(e,t){var r,a,l,n,i,f,c,x,h,d,o=s.Float32x4.load(t,0),_=s.Float32x4.load(t,4),m=s.Float32x4.load(t,8),v=s.Float32x4.load(t,12);return i=s.Float32x4.shuffle(o,_,0,1,4,5),a=s.Float32x4.shuffle(m,v,0,1,4,5),r=s.Float32x4.shuffle(i,a,0,2,4,6),a=s.Float32x4.shuffle(a,i,1,3,5,7),i=s.Float32x4.shuffle(o,_,2,3,6,7),n=s.Float32x4.shuffle(m,v,2,3,6,7),l=s.Float32x4.shuffle(i,n,0,2,4,6),n=s.Float32x4.shuffle(n,i,1,3,5,7),i=s.Float32x4.mul(l,n),i=s.Float32x4.swizzle(i,1,0,3,2),f=s.Float32x4.mul(a,i),c=s.Float32x4.mul(r,i),i=s.Float32x4.swizzle(i,2,3,0,1),f=s.Float32x4.sub(s.Float32x4.mul(a,i),f),c=s.Float32x4.sub(s.Float32x4.mul(r,i),c),c=s.Float32x4.swizzle(c,2,3,0,1),i=s.Float32x4.mul(a,l),i=s.Float32x4.swizzle(i,1,0,3,2),f=s.Float32x4.add(s.Float32x4.mul(n,i),f),h=s.Float32x4.mul(r,i),i=s.Float32x4.swizzle(i,2,3,0,1),f=s.Float32x4.sub(f,s.Float32x4.mul(n,i)),h=s.Float32x4.sub(s.Float32x4.mul(r,i),h),h=s.Float32x4.swizzle(h,2,3,0,1),i=s.Float32x4.mul(s.Float32x4.swizzle(a,2,3,0,1),n),i=s.Float32x4.swizzle(i,1,0,3,2),l=s.Float32x4.swizzle(l,2,3,0,1),f=s.Float32x4.add(s.Float32x4.mul(l,i),f),x=s.Float32x4.mul(r,i),i=s.Float32x4.swizzle(i,2,3,0,1),f=s.Float32x4.sub(f,s.Float32x4.mul(l,i)),x=s.Float32x4.sub(s.Float32x4.mul(r,i),x),x=s.Float32x4.swizzle(x,2,3,0,1),i=s.Float32x4.mul(r,a),i=s.Float32x4.swizzle(i,1,0,3,2),x=s.Float32x4.add(s.Float32x4.mul(n,i),x),h=s.Float32x4.sub(s.Float32x4.mul(l,i),h),i=s.Float32x4.swizzle(i,2,3,0,1),x=s.Float32x4.sub(s.Float32x4.mul(n,i),x),h=s.Float32x4.sub(h,s.Float32x4.mul(l,i)),i=s.Float32x4.mul(r,n),i=s.Float32x4.swizzle(i,1,0,3,2),c=s.Float32x4.sub(c,s.Float32x4.mul(l,i)),x=s.Float32x4.add(s.Float32x4.mul(a,i),x),i=s.Float32x4.swizzle(i,2,3,0,1),c=s.Float32x4.add(s.Float32x4.mul(l,i),c),x=s.Float32x4.sub(x,s.Float32x4.mul(a,i)),i=s.Float32x4.mul(r,l),i=s.Float32x4.swizzle(i,1,0,3,2),c=s.Float32x4.add(s.Float32x4.mul(n,i),c),h=s.Float32x4.sub(h,s.Float32x4.mul(a,i)),i=s.Float32x4.swizzle(i,2,3,0,1),c=s.Float32x4.sub(c,s.Float32x4.mul(n,i)),h=s.Float32x4.add(s.Float32x4.mul(a,i),h),d=s.Float32x4.mul(r,f),d=s.Float32x4.add(s.Float32x4.swizzle(d,2,3,0,1),d),d=s.Float32x4.add(s.Float32x4.swizzle(d,1,0,3,2),d),i=s.Float32x4.reciprocalApproximation(d),d=s.Float32x4.sub(s.Float32x4.add(i,i),s.Float32x4.mul(d,s.Float32x4.mul(i,i))),d=s.Float32x4.swizzle(d,0,0,0,0),d?(s.Float32x4.store(e,0,s.Float32x4.mul(d,f)),s.Float32x4.store(e,4,s.Float32x4.mul(d,c)),s.Float32x4.store(e,8,s.Float32x4.mul(d,x)),s.Float32x4.store(e,12,s.Float32x4.mul(d,h)),e):null};p.invert=y.USE_SIMD?p.SIMD.invert:p.scalar.invert;p.scalar.adjoint=function(e,t){var r=t[0],a=t[1],l=t[2],n=t[3],i=t[4],f=t[5],c=t[6],x=t[7],h=t[8],d=t[9],o=t[10],_=t[11],m=t[12],v=t[13],F=t[14],M=t[15];return e[0]=f*(o*M-_*F)-d*(c*M-x*F)+v*(c*_-x*o),e[1]=-(a*(o*M-_*F)-d*(l*M-n*F)+v*(l*_-n*o)),e[2]=a*(c*M-x*F)-f*(l*M-n*F)+v*(l*x-n*c),e[3]=-(a*(c*_-x*o)-f*(l*_-n*o)+d*(l*x-n*c)),e[4]=-(i*(o*M-_*F)-h*(c*M-x*F)+m*(c*_-x*o)),e[5]=r*(o*M-_*F)-h*(l*M-n*F)+m*(l*_-n*o),e[6]=-(r*(c*M-x*F)-i*(l*M-n*F)+m*(l*x-n*c)),e[7]=r*(c*_-x*o)-i*(l*_-n*o)+h*(l*x-n*c),e[8]=i*(d*M-_*v)-h*(f*M-x*v)+m*(f*_-x*d),e[9]=-(r*(d*M-_*v)-h*(a*M-n*v)+m*(a*_-n*d)),e[10]=r*(f*M-x*v)-i*(a*M-n*v)+m*(a*x-n*f),e[11]=-(r*(f*_-x*d)-i*(a*_-n*d)+h*(a*x-n*f)),e[12]=-(i*(d*F-o*v)-h*(f*F-c*v)+m*(f*o-c*d)),e[13]=r*(d*F-o*v)-h*(a*F-l*v)+m*(a*o-l*d),e[14]=-(r*(f*F-c*v)-i*(a*F-l*v)+m*(a*c-l*f)),e[15]=r*(f*o-c*d)-i*(a*o-l*d)+h*(a*c-l*f),e};p.SIMD.adjoint=function(e,t){var r,a,l,n,i,f,c,x,h,d,o,_,m;return r=s.Float32x4.load(t,0),a=s.Float32x4.load(t,4),l=s.Float32x4.load(t,8),n=s.Float32x4.load(t,12),h=s.Float32x4.shuffle(r,a,0,1,4,5),f=s.Float32x4.shuffle(l,n,0,1,4,5),i=s.Float32x4.shuffle(h,f,0,2,4,6),f=s.Float32x4.shuffle(f,h,1,3,5,7),h=s.Float32x4.shuffle(r,a,2,3,6,7),x=s.Float32x4.shuffle(l,n,2,3,6,7),c=s.Float32x4.shuffle(h,x,0,2,4,6),x=s.Float32x4.shuffle(x,h,1,3,5,7),h=s.Float32x4.mul(c,x),h=s.Float32x4.swizzle(h,1,0,3,2),d=s.Float32x4.mul(f,h),o=s.Float32x4.mul(i,h),h=s.Float32x4.swizzle(h,2,3,0,1),d=s.Float32x4.sub(s.Float32x4.mul(f,h),d),o=s.Float32x4.sub(s.Float32x4.mul(i,h),o),o=s.Float32x4.swizzle(o,2,3,0,1),h=s.Float32x4.mul(f,c),h=s.Float32x4.swizzle(h,1,0,3,2),d=s.Float32x4.add(s.Float32x4.mul(x,h),d),m=s.Float32x4.mul(i,h),h=s.Float32x4.swizzle(h,2,3,0,1),d=s.Float32x4.sub(d,s.Float32x4.mul(x,h)),m=s.Float32x4.sub(s.Float32x4.mul(i,h),m),m=s.Float32x4.swizzle(m,2,3,0,1),h=s.Float32x4.mul(s.Float32x4.swizzle(f,2,3,0,1),x),h=s.Float32x4.swizzle(h,1,0,3,2),c=s.Float32x4.swizzle(c,2,3,0,1),d=s.Float32x4.add(s.Float32x4.mul(c,h),d),_=s.Float32x4.mul(i,h),h=s.Float32x4.swizzle(h,2,3,0,1),d=s.Float32x4.sub(d,s.Float32x4.mul(c,h)),_=s.Float32x4.sub(s.Float32x4.mul(i,h),_),_=s.Float32x4.swizzle(_,2,3,0,1),h=s.Float32x4.mul(i,f),h=s.Float32x4.swizzle(h,1,0,3,2),_=s.Float32x4.add(s.Float32x4.mul(x,h),_),m=s.Float32x4.sub(s.Float32x4.mul(c,h),m),h=s.Float32x4.swizzle(h,2,3,0,1),_=s.Float32x4.sub(s.Float32x4.mul(x,h),_),m=s.Float32x4.sub(m,s.Float32x4.mul(c,h)),h=s.Float32x4.mul(i,x),h=s.Float32x4.swizzle(h,1,0,3,2),o=s.Float32x4.sub(o,s.Float32x4.mul(c,h)),_=s.Float32x4.add(s.Float32x4.mul(f,h),_),h=s.Float32x4.swizzle(h,2,3,0,1),o=s.Float32x4.add(s.Float32x4.mul(c,h),o),_=s.Float32x4.sub(_,s.Float32x4.mul(f,h)),h=s.Float32x4.mul(i,c),h=s.Float32x4.swizzle(h,1,0,3,2),o=s.Float32x4.add(s.Float32x4.mul(x,h),o),m=s.Float32x4.sub(m,s.Float32x4.mul(f,h)),h=s.Float32x4.swizzle(h,2,3,0,1),o=s.Float32x4.sub(o,s.Float32x4.mul(x,h)),m=s.Float32x4.add(s.Float32x4.mul(f,h),m),s.Float32x4.store(e,0,d),s.Float32x4.store(e,4,o),s.Float32x4.store(e,8,_),s.Float32x4.store(e,12,m),e};p.adjoint=y.USE_SIMD?p.SIMD.adjoint:p.scalar.adjoint;p.determinant=function(e){var t=e[0],r=e[1],a=e[2],l=e[3],n=e[4],i=e[5],f=e[6],c=e[7],x=e[8],h=e[9],d=e[10],o=e[11],_=e[12],m=e[13],v=e[14],F=e[15],M=t*i-r*n,E=t*f-a*n,S=t*c-l*n,b=r*f-a*i,g=r*c-l*i,w=a*c-l*f,I=x*m-h*_,u=x*v-d*_,D=x*F-o*_,A=h*v-d*m,T=h*F-o*m,O=d*F-o*v;return M*O-E*T+S*A+b*D-g*u+w*I};p.SIMD.multiply=function(e,t,r){var a=s.Float32x4.load(t,0),l=s.Float32x4.load(t,4),n=s.Float32x4.load(t,8),i=s.Float32x4.load(t,12),f=s.Float32x4.load(r,0),c=s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(f,0,0,0,0),a),s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(f,1,1,1,1),l),s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(f,2,2,2,2),n),s.Float32x4.mul(s.Float32x4.swizzle(f,3,3,3,3),i))));s.Float32x4.store(e,0,c);var x=s.Float32x4.load(r,4),h=s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(x,0,0,0,0),a),s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(x,1,1,1,1),l),s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(x,2,2,2,2),n),s.Float32x4.mul(s.Float32x4.swizzle(x,3,3,3,3),i))));s.Float32x4.store(e,4,h);var d=s.Float32x4.load(r,8),o=s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(d,0,0,0,0),a),s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(d,1,1,1,1),l),s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(d,2,2,2,2),n),s.Float32x4.mul(s.Float32x4.swizzle(d,3,3,3,3),i))));s.Float32x4.store(e,8,o);var _=s.Float32x4.load(r,12),m=s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(_,0,0,0,0),a),s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(_,1,1,1,1),l),s.Float32x4.add(s.Float32x4.mul(s.Float32x4.swizzle(_,2,2,2,2),n),s.Float32x4.mul(s.Float32x4.swizzle(_,3,3,3,3),i))));return s.Float32x4.store(e,12,m),e};p.scalar.multiply=function(e,t,r){var a=t[0],l=t[1],n=t[2],i=t[3],f=t[4],c=t[5],x=t[6],h=t[7],d=t[8],o=t[9],_=t[10],m=t[11],v=t[12],F=t[13],M=t[14],E=t[15],S=r[0],b=r[1],g=r[2],w=r[3];return e[0]=S*a+b*f+g*d+w*v,e[1]=S*l+b*c+g*o+w*F,e[2]=S*n+b*x+g*_+w*M,e[3]=S*i+b*h+g*m+w*E,S=r[4],b=r[5],g=r[6],w=r[7],e[4]=S*a+b*f+g*d+w*v,e[5]=S*l+b*c+g*o+w*F,e[6]=S*n+b*x+g*_+w*M,e[7]=S*i+b*h+g*m+w*E,S=r[8],b=r[9],g=r[10],w=r[11],e[8]=S*a+b*f+g*d+w*v,e[9]=S*l+b*c+g*o+w*F,e[10]=S*n+b*x+g*_+w*M,e[11]=S*i+b*h+g*m+w*E,S=r[12],b=r[13],g=r[14],w=r[15],e[12]=S*a+b*f+g*d+w*v,e[13]=S*l+b*c+g*o+w*F,e[14]=S*n+b*x+g*_+w*M,e[15]=S*i+b*h+g*m+w*E,e};p.multiply=y.USE_SIMD?p.SIMD.multiply:p.scalar.multiply;p.mul=p.multiply;p.scalar.translate=function(e,t,r){var a=r[0],l=r[1],n=r[2],i,f,c,x,h,d,o,_,m,v,F,M;return t===e?(e[12]=t[0]*a+t[4]*l+t[8]*n+t[12],e[13]=t[1]*a+t[5]*l+t[9]*n+t[13],e[14]=t[2]*a+t[6]*l+t[10]*n+t[14],e[15]=t[3]*a+t[7]*l+t[11]*n+t[15]):(i=t[0],f=t[1],c=t[2],x=t[3],h=t[4],d=t[5],o=t[6],_=t[7],m=t[8],v=t[9],F=t[10],M=t[11],e[0]=i,e[1]=f,e[2]=c,e[3]=x,e[4]=h,e[5]=d,e[6]=o,e[7]=_,e[8]=m,e[9]=v,e[10]=F,e[11]=M,e[12]=i*a+h*l+m*n+t[12],e[13]=f*a+d*l+v*n+t[13],e[14]=c*a+o*l+F*n+t[14],e[15]=x*a+_*l+M*n+t[15]),e};p.SIMD.translate=function(e,t,r){var a=s.Float32x4.load(t,0),l=s.Float32x4.load(t,4),n=s.Float32x4.load(t,8),i=s.Float32x4.load(t,12),f=s.Float32x4(r[0],r[1],r[2],0);t!==e&&(e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[4]=t[4],e[5]=t[5],e[6]=t[6],e[7]=t[7],e[8]=t[8],e[9]=t[9],e[10]=t[10],e[11]=t[11]),a=s.Float32x4.mul(a,s.Float32x4.swizzle(f,0,0,0,0)),l=s.Float32x4.mul(l,s.Float32x4.swizzle(f,1,1,1,1)),n=s.Float32x4.mul(n,s.Float32x4.swizzle(f,2,2,2,2));var c=s.Float32x4.add(a,s.Float32x4.add(l,s.Float32x4.add(n,i)));return s.Float32x4.store(e,12,c),e};p.translate=y.USE_SIMD?p.SIMD.translate:p.scalar.translate;p.scalar.scale=function(e,t,r){var a=r[0],l=r[1],n=r[2];return e[0]=t[0]*a,e[1]=t[1]*a,e[2]=t[2]*a,e[3]=t[3]*a,e[4]=t[4]*l,e[5]=t[5]*l,e[6]=t[6]*l,e[7]=t[7]*l,e[8]=t[8]*n,e[9]=t[9]*n,e[10]=t[10]*n,e[11]=t[11]*n,e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15],e};p.SIMD.scale=function(e,t,r){var a=window.SIMD,l,n,i,f=a.Float32x4(r[0],r[1],r[2],0);return l=a.Float32x4.load(t,0),a.Float32x4.store(e,0,a.Float32x4.mul(l,a.Float32x4.swizzle(f,0,0,0,0))),n=a.Float32x4.load(t,4),a.Float32x4.store(e,4,a.Float32x4.mul(n,a.Float32x4.swizzle(f,1,1,1,1))),i=a.Float32x4.load(t,8),a.Float32x4.store(e,8,a.Float32x4.mul(i,a.Float32x4.swizzle(f,2,2,2,2))),e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15],e};p.scale=y.USE_SIMD?p.SIMD.scale:p.scalar.scale;p.rotate=function(e,t,r,a){var l=a[0],n=a[1],i=a[2],f=Math.sqrt(l*l+n*n+i*i),c,x,h,d,o,_,m,v,F,M,E,S,b,g,w,I,u,D,A,T,O,L,P,R;return Math.abs(f)<y.EPSILON?null:(f=1/f,l*=f,n*=f,i*=f,c=Math.sin(r),x=Math.cos(r),h=1-x,d=t[0],o=t[1],_=t[2],m=t[3],v=t[4],F=t[5],M=t[6],E=t[7],S=t[8],b=t[9],g=t[10],w=t[11],I=l*l*h+x,u=n*l*h+i*c,D=i*l*h-n*c,A=l*n*h-i*c,T=n*n*h+x,O=i*n*h+l*c,L=l*i*h+n*c,P=n*i*h-l*c,R=i*i*h+x,e[0]=d*I+v*u+S*D,e[1]=o*I+F*u+b*D,e[2]=_*I+M*u+g*D,e[3]=m*I+E*u+w*D,e[4]=d*A+v*T+S*O,e[5]=o*A+F*T+b*O,e[6]=_*A+M*T+g*O,e[7]=m*A+E*T+w*O,e[8]=d*L+v*P+S*R,e[9]=o*L+F*P+b*R,e[10]=_*L+M*P+g*R,e[11]=m*L+E*P+w*R,t!==e&&(e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]),e)};p.scalar.rotateX=function(e,t,r){var a=Math.sin(r),l=Math.cos(r),n=t[4],i=t[5],f=t[6],c=t[7],x=t[8],h=t[9],d=t[10],o=t[11];return t!==e&&(e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]),e[4]=n*l+x*a,e[5]=i*l+h*a,e[6]=f*l+d*a,e[7]=c*l+o*a,e[8]=x*l-n*a,e[9]=h*l-i*a,e[10]=d*l-f*a,e[11]=o*l-c*a,e};p.SIMD.rotateX=function(e,t,r){var a=window.SIMD,l=a.Float32x4.splat(Math.sin(r)),n=a.Float32x4.splat(Math.cos(r));t!==e&&(e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]);var i=a.Float32x4.load(t,4),f=a.Float32x4.load(t,8);return a.Float32x4.store(e,4,a.Float32x4.add(a.Float32x4.mul(i,n),a.Float32x4.mul(f,l))),a.Float32x4.store(e,8,a.Float32x4.sub(a.Float32x4.mul(f,n),a.Float32x4.mul(i,l))),e};p.rotateX=y.USE_SIMD?p.SIMD.rotateX:p.scalar.rotateX;p.scalar.rotateY=function(e,t,r){var a=Math.sin(r),l=Math.cos(r),n=t[0],i=t[1],f=t[2],c=t[3],x=t[8],h=t[9],d=t[10],o=t[11];return t!==e&&(e[4]=t[4],e[5]=t[5],e[6]=t[6],e[7]=t[7],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]),e[0]=n*l-x*a,e[1]=i*l-h*a,e[2]=f*l-d*a,e[3]=c*l-o*a,e[8]=n*a+x*l,e[9]=i*a+h*l,e[10]=f*a+d*l,e[11]=c*a+o*l,e};p.SIMD.rotateY=function(e,t,r){var a=window.SIMD,l=a.Float32x4.splat(Math.sin(r)),n=a.Float32x4.splat(Math.cos(r));t!==e&&(e[4]=t[4],e[5]=t[5],e[6]=t[6],e[7]=t[7],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]);var i=a.Float32x4.load(t,0),f=a.Float32x4.load(t,8);return a.Float32x4.store(e,0,a.Float32x4.sub(a.Float32x4.mul(i,n),a.Float32x4.mul(f,l))),a.Float32x4.store(e,8,a.Float32x4.add(a.Float32x4.mul(i,l),a.Float32x4.mul(f,n))),e};p.rotateY=y.USE_SIMD?p.SIMD.rotateY:p.scalar.rotateY;p.scalar.rotateZ=function(e,t,r){var a=Math.sin(r),l=Math.cos(r),n=t[0],i=t[1],f=t[2],c=t[3],x=t[4],h=t[5],d=t[6],o=t[7];return t!==e&&(e[8]=t[8],e[9]=t[9],e[10]=t[10],e[11]=t[11],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]),e[0]=n*l+x*a,e[1]=i*l+h*a,e[2]=f*l+d*a,e[3]=c*l+o*a,e[4]=x*l-n*a,e[5]=h*l-i*a,e[6]=d*l-f*a,e[7]=o*l-c*a,e};p.SIMD.rotateZ=function(e,t,r){var a=window.SIMD,l=a.Float32x4.splat(Math.sin(r)),n=a.Float32x4.splat(Math.cos(r));t!==e&&(e[8]=t[8],e[9]=t[9],e[10]=t[10],e[11]=t[11],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]);var i=a.Float32x4.load(t,0),f=a.Float32x4.load(t,4);return a.Float32x4.store(e,0,a.Float32x4.add(a.Float32x4.mul(i,n),a.Float32x4.mul(f,l))),a.Float32x4.store(e,4,a.Float32x4.sub(a.Float32x4.mul(f,n),a.Float32x4.mul(i,l))),e};p.rotateZ=y.USE_SIMD?p.SIMD.rotateZ:p.scalar.rotateZ;p.fromTranslation=function(e,t){return e[0]=1,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=1,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=1,e[11]=0,e[12]=t[0],e[13]=t[1],e[14]=t[2],e[15]=1,e};p.fromScaling=function(e,t){return e[0]=t[0],e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=t[1],e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=t[2],e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e};p.fromRotation=function(e,t,r){var a=r[0],l=r[1],n=r[2],i=Math.sqrt(a*a+l*l+n*n),f,c,x;return Math.abs(i)<y.EPSILON?null:(i=1/i,a*=i,l*=i,n*=i,f=Math.sin(t),c=Math.cos(t),x=1-c,e[0]=a*a*x+c,e[1]=l*a*x+n*f,e[2]=n*a*x-l*f,e[3]=0,e[4]=a*l*x-n*f,e[5]=l*l*x+c,e[6]=n*l*x+a*f,e[7]=0,e[8]=a*n*x+l*f,e[9]=l*n*x-a*f,e[10]=n*n*x+c,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e)};p.fromXRotation=function(e,t){var r=Math.sin(t),a=Math.cos(t);return e[0]=1,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=a,e[6]=r,e[7]=0,e[8]=0,e[9]=-r,e[10]=a,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e};p.fromYRotation=function(e,t){var r=Math.sin(t),a=Math.cos(t);return e[0]=a,e[1]=0,e[2]=-r,e[3]=0,e[4]=0,e[5]=1,e[6]=0,e[7]=0,e[8]=r,e[9]=0,e[10]=a,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e};p.fromZRotation=function(e,t){var r=Math.sin(t),a=Math.cos(t);return e[0]=a,e[1]=r,e[2]=0,e[3]=0,e[4]=-r,e[5]=a,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=1,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e};p.fromRotationTranslation=function(e,t,r){var a=t[0],l=t[1],n=t[2],i=t[3],f=a+a,c=l+l,x=n+n,h=a*f,d=a*c,o=a*x,_=l*c,m=l*x,v=n*x,F=i*f,M=i*c,E=i*x;return e[0]=1-(_+v),e[1]=d+E,e[2]=o-M,e[3]=0,e[4]=d-E,e[5]=1-(h+v),e[6]=m+F,e[7]=0,e[8]=o+M,e[9]=m-F,e[10]=1-(h+_),e[11]=0,e[12]=r[0],e[13]=r[1],e[14]=r[2],e[15]=1,e};p.getTranslation=function(e,t){return e[0]=t[12],e[1]=t[13],e[2]=t[14],e};p.getRotation=function(e,t){var r=t[0]+t[5]+t[10],a=0;return r>0?(a=Math.sqrt(r+1)*2,e[3]=.25*a,e[0]=(t[6]-t[9])/a,e[1]=(t[8]-t[2])/a,e[2]=(t[1]-t[4])/a):t[0]>t[5]&&t[0]>t[10]?(a=Math.sqrt(1+t[0]-t[5]-t[10])*2,e[3]=(t[6]-t[9])/a,e[0]=.25*a,e[1]=(t[1]+t[4])/a,e[2]=(t[8]+t[2])/a):t[5]>t[10]?(a=Math.sqrt(1+t[5]-t[0]-t[10])*2,e[3]=(t[8]-t[2])/a,e[0]=(t[1]+t[4])/a,e[1]=.25*a,e[2]=(t[6]+t[9])/a):(a=Math.sqrt(1+t[10]-t[0]-t[5])*2,e[3]=(t[1]-t[4])/a,e[0]=(t[8]+t[2])/a,e[1]=(t[6]+t[9])/a,e[2]=.25*a),e};p.fromRotationTranslationScale=function(e,t,r,a){var l=t[0],n=t[1],i=t[2],f=t[3],c=l+l,x=n+n,h=i+i,d=l*c,o=l*x,_=l*h,m=n*x,v=n*h,F=i*h,M=f*c,E=f*x,S=f*h,b=a[0],g=a[1],w=a[2];return e[0]=(1-(m+F))*b,e[1]=(o+S)*b,e[2]=(_-E)*b,e[3]=0,e[4]=(o-S)*g,e[5]=(1-(d+F))*g,e[6]=(v+M)*g,e[7]=0,e[8]=(_+E)*w,e[9]=(v-M)*w,e[10]=(1-(d+m))*w,e[11]=0,e[12]=r[0],e[13]=r[1],e[14]=r[2],e[15]=1,e};p.fromRotationTranslationScaleOrigin=function(e,t,r,a,l){var n=t[0],i=t[1],f=t[2],c=t[3],x=n+n,h=i+i,d=f+f,o=n*x,_=n*h,m=n*d,v=i*h,F=i*d,M=f*d,E=c*x,S=c*h,b=c*d,g=a[0],w=a[1],I=a[2],u=l[0],D=l[1],A=l[2];return e[0]=(1-(v+M))*g,e[1]=(_+b)*g,e[2]=(m-S)*g,e[3]=0,e[4]=(_-b)*w,e[5]=(1-(o+M))*w,e[6]=(F+E)*w,e[7]=0,e[8]=(m+S)*I,e[9]=(F-E)*I,e[10]=(1-(o+v))*I,e[11]=0,e[12]=r[0]+u-(e[0]*u+e[4]*D+e[8]*A),e[13]=r[1]+D-(e[1]*u+e[5]*D+e[9]*A),e[14]=r[2]+A-(e[2]*u+e[6]*D+e[10]*A),e[15]=1,e};p.fromQuat=function(e,t){var r=t[0],a=t[1],l=t[2],n=t[3],i=r+r,f=a+a,c=l+l,x=r*i,h=a*i,d=a*f,o=l*i,_=l*f,m=l*c,v=n*i,F=n*f,M=n*c;return e[0]=1-d-m,e[1]=h+M,e[2]=o-F,e[3]=0,e[4]=h-M,e[5]=1-x-m,e[6]=_+v,e[7]=0,e[8]=o+F,e[9]=_-v,e[10]=1-x-d,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e};p.frustum=function(e,t,r,a,l,n,i){var f=1/(r-t),c=1/(l-a),x=1/(n-i);return e[0]=n*2*f,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=n*2*c,e[6]=0,e[7]=0,e[8]=(r+t)*f,e[9]=(l+a)*c,e[10]=(i+n)*x,e[11]=-1,e[12]=0,e[13]=0,e[14]=i*n*2*x,e[15]=0,e};p.perspective=function(e,t,r,a,l){var n=1/Math.tan(t/2),i=1/(a-l);return e[0]=n/r,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=n,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=(l+a)*i,e[11]=-1,e[12]=0,e[13]=0,e[14]=2*l*a*i,e[15]=0,e};p.perspectiveFromFieldOfView=function(e,t,r,a){var l=Math.tan(t.upDegrees*Math.PI/180),n=Math.tan(t.downDegrees*Math.PI/180),i=Math.tan(t.leftDegrees*Math.PI/180),f=Math.tan(t.rightDegrees*Math.PI/180),c=2/(i+f),x=2/(l+n);return e[0]=c,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=x,e[6]=0,e[7]=0,e[8]=-((i-f)*c*.5),e[9]=(l-n)*x*.5,e[10]=a/(r-a),e[11]=-1,e[12]=0,e[13]=0,e[14]=a*r/(r-a),e[15]=0,e};p.ortho=function(e,t,r,a,l,n,i){var f=1/(t-r),c=1/(a-l),x=1/(n-i);return e[0]=-2*f,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=-2*c,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=2*x,e[11]=0,e[12]=(t+r)*f,e[13]=(l+a)*c,e[14]=(i+n)*x,e[15]=1,e};p.lookAt=function(e,t,r,a){var l,n,i,f,c,x,h,d,o,_,m=t[0],v=t[1],F=t[2],M=a[0],E=a[1],S=a[2],b=r[0],g=r[1],w=r[2];return Math.abs(m-b)<y.EPSILON&&Math.abs(v-g)<y.EPSILON&&Math.abs(F-w)<y.EPSILON?p.identity(e):(h=m-b,d=v-g,o=F-w,_=1/Math.sqrt(h*h+d*d+o*o),h*=_,d*=_,o*=_,l=E*o-S*d,n=S*h-M*o,i=M*d-E*h,_=Math.sqrt(l*l+n*n+i*i),_?(_=1/_,l*=_,n*=_,i*=_):(l=0,n=0,i=0),f=d*i-o*n,c=o*l-h*i,x=h*n-d*l,_=Math.sqrt(f*f+c*c+x*x),_?(_=1/_,f*=_,c*=_,x*=_):(f=0,c=0,x=0),e[0]=l,e[1]=f,e[2]=h,e[3]=0,e[4]=n,e[5]=c,e[6]=d,e[7]=0,e[8]=i,e[9]=x,e[10]=o,e[11]=0,e[12]=-(l*m+n*v+i*F),e[13]=-(f*m+c*v+x*F),e[14]=-(h*m+d*v+o*F),e[15]=1,e)};p.str=function(e){return"mat4("+e[0]+", "+e[1]+", "+e[2]+", "+e[3]+", "+e[4]+", "+e[5]+", "+e[6]+", "+e[7]+", "+e[8]+", "+e[9]+", "+e[10]+", "+e[11]+", "+e[12]+", "+e[13]+", "+e[14]+", "+e[15]+")"};p.frob=function(e){return Math.sqrt(Math.pow(e[0],2)+Math.pow(e[1],2)+Math.pow(e[2],2)+Math.pow(e[3],2)+Math.pow(e[4],2)+Math.pow(e[5],2)+Math.pow(e[6],2)+Math.pow(e[7],2)+Math.pow(e[8],2)+Math.pow(e[9],2)+Math.pow(e[10],2)+Math.pow(e[11],2)+Math.pow(e[12],2)+Math.pow(e[13],2)+Math.pow(e[14],2)+Math.pow(e[15],2))};p.add=function(e,t,r){return e[0]=t[0]+r[0],e[1]=t[1]+r[1],e[2]=t[2]+r[2],e[3]=t[3]+r[3],e[4]=t[4]+r[4],e[5]=t[5]+r[5],e[6]=t[6]+r[6],e[7]=t[7]+r[7],e[8]=t[8]+r[8],e[9]=t[9]+r[9],e[10]=t[10]+r[10],e[11]=t[11]+r[11],e[12]=t[12]+r[12],e[13]=t[13]+r[13],e[14]=t[14]+r[14],e[15]=t[15]+r[15],e};p.subtract=function(e,t,r){return e[0]=t[0]-r[0],e[1]=t[1]-r[1],e[2]=t[2]-r[2],e[3]=t[3]-r[3],e[4]=t[4]-r[4],e[5]=t[5]-r[5],e[6]=t[6]-r[6],e[7]=t[7]-r[7],e[8]=t[8]-r[8],e[9]=t[9]-r[9],e[10]=t[10]-r[10],e[11]=t[11]-r[11],e[12]=t[12]-r[12],e[13]=t[13]-r[13],e[14]=t[14]-r[14],e[15]=t[15]-r[15],e};p.sub=p.subtract;p.multiplyScalar=function(e,t,r){return e[0]=t[0]*r,e[1]=t[1]*r,e[2]=t[2]*r,e[3]=t[3]*r,e[4]=t[4]*r,e[5]=t[5]*r,e[6]=t[6]*r,e[7]=t[7]*r,e[8]=t[8]*r,e[9]=t[9]*r,e[10]=t[10]*r,e[11]=t[11]*r,e[12]=t[12]*r,e[13]=t[13]*r,e[14]=t[14]*r,e[15]=t[15]*r,e};p.multiplyScalarAndAdd=function(e,t,r,a){return e[0]=t[0]+r[0]*a,e[1]=t[1]+r[1]*a,e[2]=t[2]+r[2]*a,e[3]=t[3]+r[3]*a,e[4]=t[4]+r[4]*a,e[5]=t[5]+r[5]*a,e[6]=t[6]+r[6]*a,e[7]=t[7]+r[7]*a,e[8]=t[8]+r[8]*a,e[9]=t[9]+r[9]*a,e[10]=t[10]+r[10]*a,e[11]=t[11]+r[11]*a,e[12]=t[12]+r[12]*a,e[13]=t[13]+r[13]*a,e[14]=t[14]+r[14]*a,e[15]=t[15]+r[15]*a,e};p.exactEquals=function(e,t){return e[0]===t[0]&&e[1]===t[1]&&e[2]===t[2]&&e[3]===t[3]&&e[4]===t[4]&&e[5]===t[5]&&e[6]===t[6]&&e[7]===t[7]&&e[8]===t[8]&&e[9]===t[9]&&e[10]===t[10]&&e[11]===t[11]&&e[12]===t[12]&&e[13]===t[13]&&e[14]===t[14]&&e[15]===t[15]};p.equals=function(e,t){var r=e[0],a=e[1],l=e[2],n=e[3],i=e[4],f=e[5],c=e[6],x=e[7],h=e[8],d=e[9],o=e[10],_=e[11],m=e[12],v=e[13],F=e[14],M=e[15],E=t[0],S=t[1],b=t[2],g=t[3],w=t[4],I=t[5],u=t[6],D=t[7],A=t[8],T=t[9],O=t[10],L=t[11],P=t[12],R=t[13],W=t[14],H=t[15];return Math.abs(r-E)<=y.EPSILON*Math.max(1,Math.abs(r),Math.abs(E))&&Math.abs(a-S)<=y.EPSILON*Math.max(1,Math.abs(a),Math.abs(S))&&Math.abs(l-b)<=y.EPSILON*Math.max(1,Math.abs(l),Math.abs(b))&&Math.abs(n-g)<=y.EPSILON*Math.max(1,Math.abs(n),Math.abs(g))&&Math.abs(i-w)<=y.EPSILON*Math.max(1,Math.abs(i),Math.abs(w))&&Math.abs(f-I)<=y.EPSILON*Math.max(1,Math.abs(f),Math.abs(I))&&Math.abs(c-u)<=y.EPSILON*Math.max(1,Math.abs(c),Math.abs(u))&&Math.abs(x-D)<=y.EPSILON*Math.max(1,Math.abs(x),Math.abs(D))&&Math.abs(h-A)<=y.EPSILON*Math.max(1,Math.abs(h),Math.abs(A))&&Math.abs(d-T)<=y.EPSILON*Math.max(1,Math.abs(d),Math.abs(T))&&Math.abs(o-O)<=y.EPSILON*Math.max(1,Math.abs(o),Math.abs(O))&&Math.abs(_-L)<=y.EPSILON*Math.max(1,Math.abs(_),Math.abs(L))&&Math.abs(m-P)<=y.EPSILON*Math.max(1,Math.abs(m),Math.abs(P))&&Math.abs(v-R)<=y.EPSILON*Math.max(1,Math.abs(v),Math.abs(R))&&Math.abs(F-W)<=y.EPSILON*Math.max(1,Math.abs(F),Math.abs(W))&&Math.abs(M-H)<=y.EPSILON*Math.max(1,Math.abs(M),Math.abs(H))};var z={};z.create=function(){var e=new y.ARRAY_TYPE(3);return e[0]=0,e[1]=0,e[2]=0,e};z.clone=function(e){var t=new y.ARRAY_TYPE(3);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t};z.fromValues=function(e,t,r){var a=new y.ARRAY_TYPE(3);return a[0]=e,a[1]=t,a[2]=r,a};z.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e};z.set=function(e,t,r,a){return e[0]=t,e[1]=r,e[2]=a,e};z.add=function(e,t,r){return e[0]=t[0]+r[0],e[1]=t[1]+r[1],e[2]=t[2]+r[2],e};z.subtract=function(e,t,r){return e[0]=t[0]-r[0],e[1]=t[1]-r[1],e[2]=t[2]-r[2],e};z.sub=z.subtract;z.multiply=function(e,t,r){return e[0]=t[0]*r[0],e[1]=t[1]*r[1],e[2]=t[2]*r[2],e};z.mul=z.multiply;z.divide=function(e,t,r){return e[0]=t[0]/r[0],e[1]=t[1]/r[1],e[2]=t[2]/r[2],e};z.div=z.divide;z.ceil=function(e,t){return e[0]=Math.ceil(t[0]),e[1]=Math.ceil(t[1]),e[2]=Math.ceil(t[2]),e};z.floor=function(e,t){return e[0]=Math.floor(t[0]),e[1]=Math.floor(t[1]),e[2]=Math.floor(t[2]),e};z.min=function(e,t,r){return e[0]=Math.min(t[0],r[0]),e[1]=Math.min(t[1],r[1]),e[2]=Math.min(t[2],r[2]),e};z.max=function(e,t,r){return e[0]=Math.max(t[0],r[0]),e[1]=Math.max(t[1],r[1]),e[2]=Math.max(t[2],r[2]),e};z.round=function(e,t){return e[0]=Math.round(t[0]),e[1]=Math.round(t[1]),e[2]=Math.round(t[2]),e};z.scale=function(e,t,r){return e[0]=t[0]*r,e[1]=t[1]*r,e[2]=t[2]*r,e};z.scaleAndAdd=function(e,t,r,a){return e[0]=t[0]+r[0]*a,e[1]=t[1]+r[1]*a,e[2]=t[2]+r[2]*a,e};z.distance=function(e,t){var r=t[0]-e[0],a=t[1]-e[1],l=t[2]-e[2];return Math.sqrt(r*r+a*a+l*l)};z.dist=z.distance;z.squaredDistance=function(e,t){var r=t[0]-e[0],a=t[1]-e[1],l=t[2]-e[2];return r*r+a*a+l*l};z.sqrDist=z.squaredDistance;z.length=function(e){var t=e[0],r=e[1],a=e[2];return Math.sqrt(t*t+r*r+a*a)};z.len=z.length;z.squaredLength=function(e){var t=e[0],r=e[1],a=e[2];return t*t+r*r+a*a};z.sqrLen=z.squaredLength;z.negate=function(e,t){return e[0]=-t[0],e[1]=-t[1],e[2]=-t[2],e};z.inverse=function(e,t){return e[0]=1/t[0],e[1]=1/t[1],e[2]=1/t[2],e};z.normalize=function(e,t){var r=t[0],a=t[1],l=t[2],n=r*r+a*a+l*l;return n>0&&(n=1/Math.sqrt(n),e[0]=t[0]*n,e[1]=t[1]*n,e[2]=t[2]*n),e};z.dot=function(e,t){return e[0]*t[0]+e[1]*t[1]+e[2]*t[2]};z.cross=function(e,t,r){var a=t[0],l=t[1],n=t[2],i=r[0],f=r[1],c=r[2];return e[0]=l*c-n*f,e[1]=n*i-a*c,e[2]=a*f-l*i,e};z.lerp=function(e,t,r,a){var l=t[0],n=t[1],i=t[2];return e[0]=l+a*(r[0]-l),e[1]=n+a*(r[1]-n),e[2]=i+a*(r[2]-i),e};z.hermite=function(e,t,r,a,l,n){var i=n*n,f=i*(2*n-3)+1,c=i*(n-2)+n,x=i*(n-1),h=i*(3-2*n);return e[0]=t[0]*f+r[0]*c+a[0]*x+l[0]*h,e[1]=t[1]*f+r[1]*c+a[1]*x+l[1]*h,e[2]=t[2]*f+r[2]*c+a[2]*x+l[2]*h,e};z.bezier=function(e,t,r,a,l,n){var i=1-n,f=i*i,c=n*n,x=f*i,h=3*n*f,d=3*c*i,o=c*n;return e[0]=t[0]*x+r[0]*h+a[0]*d+l[0]*o,e[1]=t[1]*x+r[1]*h+a[1]*d+l[1]*o,e[2]=t[2]*x+r[2]*h+a[2]*d+l[2]*o,e};z.random=function(e,t){t=t||1;var r=y.RANDOM()*2*Math.PI,a=y.RANDOM()*2-1,l=Math.sqrt(1-a*a)*t;return e[0]=Math.cos(r)*l,e[1]=Math.sin(r)*l,e[2]=a*t,e};z.transformMat4=function(e,t,r){var a=t[0],l=t[1],n=t[2],i=r[3]*a+r[7]*l+r[11]*n+r[15];return i=i||1,e[0]=(r[0]*a+r[4]*l+r[8]*n+r[12])/i,e[1]=(r[1]*a+r[5]*l+r[9]*n+r[13])/i,e[2]=(r[2]*a+r[6]*l+r[10]*n+r[14])/i,e};z.transformMat3=function(e,t,r){var a=t[0],l=t[1],n=t[2];return e[0]=a*r[0]+l*r[3]+n*r[6],e[1]=a*r[1]+l*r[4]+n*r[7],e[2]=a*r[2]+l*r[5]+n*r[8],e};z.transformQuat=function(e,t,r){var a=t[0],l=t[1],n=t[2],i=r[0],f=r[1],c=r[2],x=r[3],h=x*a+f*n-c*l,d=x*l+c*a-i*n,o=x*n+i*l-f*a,_=-i*a-f*l-c*n;return e[0]=h*x+_*-i+d*-c-o*-f,e[1]=d*x+_*-f+o*-i-h*-c,e[2]=o*x+_*-c+h*-f-d*-i,e};z.rotateX=function(e,t,r,a){var l=[],n=[];return l[0]=t[0]-r[0],l[1]=t[1]-r[1],l[2]=t[2]-r[2],n[0]=l[0],n[1]=l[1]*Math.cos(a)-l[2]*Math.sin(a),n[2]=l[1]*Math.sin(a)+l[2]*Math.cos(a),e[0]=n[0]+r[0],e[1]=n[1]+r[1],e[2]=n[2]+r[2],e};z.rotateY=function(e,t,r,a){var l=[],n=[];return l[0]=t[0]-r[0],l[1]=t[1]-r[1],l[2]=t[2]-r[2],n[0]=l[2]*Math.sin(a)+l[0]*Math.cos(a),n[1]=l[1],n[2]=l[2]*Math.cos(a)-l[0]*Math.sin(a),e[0]=n[0]+r[0],e[1]=n[1]+r[1],e[2]=n[2]+r[2],e};z.rotateZ=function(e,t,r,a){var l=[],n=[];return l[0]=t[0]-r[0],l[1]=t[1]-r[1],l[2]=t[2]-r[2],n[0]=l[0]*Math.cos(a)-l[1]*Math.sin(a),n[1]=l[0]*Math.sin(a)+l[1]*Math.cos(a),n[2]=l[2],e[0]=n[0]+r[0],e[1]=n[1]+r[1],e[2]=n[2]+r[2],e};z.forEach=function(){var e=z.create();return function(t,r,a,l,n,i){var f,c;for(r||(r=3),a||(a=0),l?c=Math.min(l*r+a,t.length):c=t.length,f=a;f<c;f+=r)e[0]=t[f],e[1]=t[f+1],e[2]=t[f+2],n(e,e,i),t[f]=e[0],t[f+1]=e[1],t[f+2]=e[2];return t}}();z.angle=function(e,t){var r=z.fromValues(e[0],e[1],e[2]),a=z.fromValues(t[0],t[1],t[2]);z.normalize(r,r),z.normalize(a,a);var l=z.dot(r,a);return l>1?0:Math.acos(l)};z.str=function(e){return"vec3("+e[0]+", "+e[1]+", "+e[2]+")"};z.exactEquals=function(e,t){return e[0]===t[0]&&e[1]===t[1]&&e[2]===t[2]};z.equals=function(e,t){var r=e[0],a=e[1],l=e[2],n=t[0],i=t[1],f=t[2];return Math.abs(r-n)<=y.EPSILON*Math.max(1,Math.abs(r),Math.abs(n))&&Math.abs(a-i)<=y.EPSILON*Math.max(1,Math.abs(a),Math.abs(i))&&Math.abs(l-f)<=y.EPSILON*Math.max(1,Math.abs(l),Math.abs(f))};const _e=new se,U={0:{img_data:[],on_pixels:[]},1:{img_data:[],on_pixels:[]},2:{img_data:[],on_pixels:[]}};function pe(){const e=window.location.href.split("?");return!!new URLSearchParams(e[1]).has("debug")}let G=null;const j=pe();j&&(G=$(),document.body.appendChild(G.domElement));le(e=>{window.instance=e,ie().then(t=>{Fe(t)})});function Fe(e){const t=window.instance,r=t.device;e.forEach((g,w)=>{U[w].img_data=_e.read_image(g)});let a=ce(r,he,{buffers:[{arrayStride:4*4,attributes:[{shaderLocation:0,offset:0,format:J}]}]}),l=1,n=new de(l,l,1,1),i=new fe(t),f=ve(t,l);i.addAttribute(0,new Float32Array(n.vertices)),i.setIndexData(n.indices);let c=we(t,f),x=Me(t),h=r.createPipelineLayout({bindGroupLayouts:[x.layout,c.dynamicBindGroupLayout]});const d=r.createRenderPipeline({layout:h,vertex:a.vertexStage,fragment:a.fragmentStage,primitive:{topology:re,cullMode:"back"},depthStencil:{depthWriteEnabled:!0,depthCompare:"less",format:B}});let o={colorAttachments:[{view:void 0,loadValue:{r:0,g:.5,b:.5,a:1},storeOp:"store"}],depthStencilAttachment:{view:t.getDefaultDepthView(),depthLoadValue:1,depthStoreOp:C,stencilLoadValue:0,stencilStoreOp:C}};const _=p.create(),m=Math.abs(window.innerWidth/window.innerHeight);p.perspective(_,60,m,1,1e4);const v=p.create();p.translate(v,v,z.fromValues(0,0,-50));const F=p.create();p.multiply(F,_,v);let M=F;r.queue.writeBuffer(x.buffer.getObject(),0,M.buffer,M.byteOffset,M.byteLength);let E=new Float32Array(128*128);for(let g in U){let w=U[g],I=w.img_data.data,u=I.length;for(let D=0;D<u;D+=4)I[D]<10?w.on_pixels.push(2):w.on_pixels.push(0)}let S=0;setInterval(()=>{U[S].on_pixels.onUpdate=()=>{c.togglePixels(E),c.updateBuffer()},Q.to(E,2.4,U[S].on_pixels),S===2?S=0:S++},1500),t.addResizeCallback(()=>{const g=Math.abs(window.innerWidth/window.innerHeight);p.perspective(_,60,g,1,1e4);const w=p.create();p.translate(w,w,z.fromValues(0,0,-50));const I=p.create();p.multiply(I,_,w);let u=I;r.queue.writeBuffer(x.buffer.getObject(),0,u.buffer,u.byteOffset,u.byteLength),o.depthStencilAttachment={view:t.getDefaultDepthView(),depthLoadValue:1,depthStoreOp:C,stencilLoadValue:0,stencilStoreOp:C}});let b=()=>{if(requestAnimationFrame(b),j&&G.begin(),!t.resizing){me(x.app_buffer),o.colorAttachments[0].view=t.getCurrentTexture();const g=r.createCommandEncoder(),w=g.beginRenderPass(o);w.setPipeline(d),w.setBindGroup(0,x.bind_group);const I=[0];for(let u=0;u<f.num_instances;++u)I[0]=u*c.alignedUniformBytes,w.setBindGroup(1,c.dynamicBindGroup,I),i.draw(w);w.endPass(),r.queue.submit([g.finish()])}j&&G.end()};b()}function me(e){e.data[0]=performance.now()*5e-4,e.updateData()}function ve(e,t=1){let r=256/2,a=r,l=r,n=[];for(let i=0;i<a;++i)for(let f=0;f<l;++f){let c=t*(i-r/2),x=t*(f-r/2);n.push([-x,c,0,1])}return{num_instances:n.length,cols:a,rows:l,rolledPositions:n}}function Me(e){let t=new N(e,{size:4*16}),r=new N(e,{size:4*4});r.set({data:[0,window.innerWidth,window.innerHeight,0],usage:GPUBufferUsage.UNIFORM|GPUBufferUsage.COPY_DST});let a=e.device.createBindGroupLayout({entries:[{binding:0,visibility:GPUShaderStage.VERTEX,buffer:{type:V,minBindingSize:4*16}},{binding:1,visibility:GPUShaderStage.VERTEX|GPUShaderStage.FRAGMENT,buffer:{type:V,minBindingSize:4*4}}]});a.label="camera-layout";let l=e.device.createBindGroup({layout:a,entries:[{binding:0,resource:{buffer:t.getObject()}},{binding:1,resource:{buffer:r.getObject()}}]});return{buffer:t,app_buffer:r,bind_group:l,layout:a}}function we(e,t){const r=t.num_instances,a=3*Float32Array.BYTES_PER_ELEMENT,l=Math.ceil(a/256)*256,n=l/Float32Array.BYTES_PER_ELEMENT,i=e.device.createBuffer({size:r*l+Float32Array.BYTES_PER_ELEMENT,usage:GPUBufferUsage.COPY_DST|GPUBufferUsage.UNIFORM}),f=new Float32Array(r*n);for(let d=0;d<r;++d){let o=t.rolledPositions[d];f[n*d+0]=o[0],f[n*d+1]=o[1],f[n*d+2]=0,f[n*d+3]=Math.random()+.5}const c=r;for(let d=0;d<f.length;d+=c)e.device.queue.writeBuffer(i,d*Float32Array.BYTES_PER_ELEMENT,f.buffer,f.byteOffset+d*Float32Array.BYTES_PER_ELEMENT);const x=e.device.createBindGroupLayout({entries:[{binding:0,visibility:ee|te,buffer:{type:V,hasDynamicOffset:!0,minBindingSize:4*4}}]}),h=e.device.createBindGroup({layout:x,entries:[{binding:0,resource:{buffer:i,offset:0,size:4*Float32Array.BYTES_PER_ELEMENT}}]});return{dynamicBindGroupLayout:x,dynamicBindGroup:h,alignedUniformBytes:l,togglePixels(d){for(let o=0;o<r;++o){let _=d[o];f[n*o+2]=_}},updateBuffer(){const d=r;for(let o=0;o<f.length;o+=d)e.device.queue.writeBuffer(i,o*Float32Array.BYTES_PER_ELEMENT,f.buffer,f.byteOffset+o*Float32Array.BYTES_PER_ELEMENT)}}}
