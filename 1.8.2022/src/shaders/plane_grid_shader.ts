// some effects based off of the following
// https://www.shadertoy.com/view/tdG3Rd

let params = `   
   
        struct Uniforms2 {
            xpos:f32;
            ypos:f32;
            pixelStatus:f32;
            offset:f32;
        };
        [[binding(0), group(1)]] var<uniform> params : Uniforms2;
        `
let app_params = `
        struct AppUniforms {
           params: vec4<f32>;
        };
        [[binding(1), group(0)]] var<uniform> env : AppUniforms;

`

let color_map = `

fn colormap_red(x: f32) -> f32 {
    var x_1: f32;

    x_1 = x;
    let _e5 = x_1;
    if ((_e5 < 0.0)) {
        {
            return (4.0 / 255.0);
        }
    } else {
        let _e11 = x_1;
        if ((_e11 < (20049.0 / 182979.0))) {
            {
                let _e17 = x_1;
                return (((829.7899780273438 * _e17) + 54.5099983215332) / 255.0);
            }
        } else {
            {
                return 1.0;
            }
        }
    }
}

fn colormap_green(x_2: f32) -> f32 {
    var x_3: f32;

    x_3 = x_2;
    let _e5 = x_3;
    if ((_e5 < (20049.0 / 82979.0))) {
        {
            return 0.0;
        }
    } else {
        let _e11 = x_3;
        if ((_e11 < (327013.0 / 810990.0))) {
            {
                let _e19 = x_3;
                return ((((8546482651136.0 / 10875673600.0) * _e19) - (2064961372160.0 / 10875673600.0)) / 255.0);
            }
        } else {
            let _e27 = x_3;
            if ((_e27 <= 1.0)) {
                {
                    let _e33 = x_3;
                    return ((((103806720.0 / 483977.0) * _e33) + (19607416.0 / 483977.0)) / 255.0);
                }
            } else {
                {
                    return 1.0;
                }
            }
        }
    }
}

fn colormap_blue(x_4: f32) -> f32 {
    var x_5: f32;

    x_5 = x_4;
    let _e5 = x_5;
    if ((_e5 < 0.0)) {
        {
            return (54.0 / 255.0);
        }
    } else {
        let _e11 = x_5;
        if ((_e11 < (7249.0 / 82979.0))) {
            {
                let _e17 = x_5;
                return (((829.7899780273438 * _e17) + 54.5099983215332) / 255.0);
            }
        } else {
            let _e23 = x_5;
            if ((_e23 < (20049.0 / 82979.0))) {
                {
                    return (127.0 / 255.0);
                }
            } else {
                let _e31 = x_5;
                if ((_e31 < (327013.0 / 810990.0))) {
                    {
                        let _e37 = x_5;
                        return (((792.0225219726563 * _e37) - 64.36479187011719) / 255.0);
                    }
                } else {
                    {
                        return 1.0;
                    }
                }
            }
        }
    }
}

fn colormap(x_6: f32) -> vec4<f32> {
    var x_7: f32;

    x_7 = x_6;
    let _e6 = x_7;
    let _e7 = colormap_red(_e6 * 20.0);
    let _e9 = x_7 * 2.0;
    let _e10 = colormap_green(_e9);
    let _e12 = x_7 / 2.0;
    let _e13 = colormap_blue(_e12);
    return vec4<f32>(_e7, _e10, _e13, 1.0);
}

fn rand(n: vec2<f32>) -> f32 {
    var n_1: vec2<f32>;

    n_1 = n;
    let _e9 = n_1;
    let _e18 = n_1;
    let _e30 = n_1;
    let _e39 = n_1;
    return fract((sin(dot(_e39, vec2<f32>(12.989800453186035, 4.14139986038208))) * 43758.546875));
}

fn noise(p: vec2<f32>) -> f32 {
    var p_1: vec2<f32>;
    var ip: vec2<f32>;
    var u: vec2<f32>;
    var res: f32;

    p_1 = p;
    let _e6 = p_1;
    ip = floor(_e6);
    let _e10 = p_1;
    u = fract(_e10);
    let _e13 = u;
    let _e14 = u;
    let _e18 = u;
    u = ((_e13 * _e14) * (vec2<f32>(3.0) - (2.0 * _e18)));
    let _e24 = ip;
    let _e25 = rand(_e24);
    let _e26 = ip;
    let _e31 = ip;
    let _e36 = rand((_e31 + vec2<f32>(1.0, 0.0)));
    let _e37 = u;
    let _e40 = ip;
    let _e41 = rand(_e40);
    let _e42 = ip;
    let _e47 = ip;
    let _e52 = rand((_e47 + vec2<f32>(1.0, 0.0)));
    let _e53 = u;
    let _e56 = ip;
    let _e61 = ip;
    let _e66 = rand((_e61 + vec2<f32>(0.0, 1.0)));
    let _e67 = ip;
    let _e72 = ip;
    let _e77 = rand((_e72 + vec2<f32>(1.0, 1.0)));
    let _e78 = u;
    let _e80 = ip;
    let _e85 = ip;
    let _e90 = rand((_e85 + vec2<f32>(0.0, 1.0)));
    let _e91 = ip;
    let _e96 = ip;
    let _e101 = rand((_e96 + vec2<f32>(1.0, 1.0)));
    let _e102 = u;
    let _e105 = u;
    let _e108 = ip;
    let _e109 = rand(_e108);
    let _e110 = ip;
    let _e115 = ip;
    let _e120 = rand((_e115 + vec2<f32>(1.0, 0.0)));
    let _e121 = u;
    let _e124 = ip;
    let _e125 = rand(_e124);
    let _e126 = ip;
    let _e131 = ip;
    let _e136 = rand((_e131 + vec2<f32>(1.0, 0.0)));
    let _e137 = u;
    let _e140 = ip;
    let _e145 = ip;
    let _e150 = rand((_e145 + vec2<f32>(0.0, 1.0)));
    let _e151 = ip;
    let _e156 = ip;
    let _e161 = rand((_e156 + vec2<f32>(1.0, 1.0)));
    let _e162 = u;
    let _e164 = ip;
    let _e169 = ip;
    let _e174 = rand((_e169 + vec2<f32>(0.0, 1.0)));
    let _e175 = ip;
    let _e180 = ip;
    let _e185 = rand((_e180 + vec2<f32>(1.0, 1.0)));
    let _e186 = u;
    let _e189 = u;
    res = mix(mix(_e125, _e136, _e137.x), mix(_e174, _e185, _e186.x), _e189.y);
    let _e193 = res;
    let _e194 = res;
    return (_e193 * _e194);
}

fn fbm(p_2: vec2<f32>) -> f32 {
    var p_3: vec2<f32>;
    var f: f32 = 0.0;

    p_3 = p_2;
    let _e8 = f;
    let _e10 = p_3;
    let _e11 = env.params.x;
    let _e14 = p_3;
    let _e15 = env.params.x;
    let _e18 = noise((_e14 + vec2<f32>(_e15)));
    f = (_e8 + (0.5 * _e18));
    let _e21 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e21) * 2.0199999809265137);
    let _e25 = f;
    let _e28 = p_3;
    let _e29 = noise(_e28);
    f = (_e25 + (0.03125 * _e29));
    let _e32 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e32) * 2.009999990463257);
    let _e36 = f;
    let _e39 = p_3;
    let _e40 = noise(_e39);
    f = (_e36 + (0.25 * _e40));
    let _e43 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e43) * 2.0299999713897705);
    let _e47 = f;
    let _e50 = p_3;
    let _e51 = noise(_e50);
    f = (_e47 + (0.125 * _e51));
    let _e54 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e54) * 2.009999990463257);
    let _e58 = f;
    let _e61 = p_3;
    let _e62 = noise(_e61);
    f = (_e58 + (0.0625 * _e62));
    let _e65 = p_3;
    p_3 = ((mat2x2<f32>(vec2<f32>(0.800000011920929, 0.6000000238418579), vec2<f32>(-0.6000000238418579, 0.800000011920929)) * _e65) * 2.0399999618530273);
    let _e69 = f;
    let _e71 = p_3;
    let _e73 = env.params.x;
    let _e77 = p_3;
    let _e79 = env.params.x;
    let _e83 = noise((_e77 + vec2<f32>(sin(_e79))));
    f = (_e69 + (0.015625 * _e83));
    let _e86 = f;
    return (_e86 / 0.96875);
}

fn pattern(p_4: vec2<f32>) -> f32 {
    var p_5: vec2<f32>;

    p_5 = p_4;
    let _e6 = p_5;
    let _e7 = p_5;
    let _e9 = p_5;
    let _e10 = fbm(_e9);
    let _e13 = p_5;
    let _e15 = p_5;
    let _e16 = fbm(_e15);
    let _e19 = fbm((_e13 + vec2<f32>(_e16)));
    let _e22 = p_5;
    let _e23 = p_5;
    let _e25 = p_5;
    let _e26 = fbm(_e25);
    let _e29 = p_5;
    let _e31 = p_5;
    let _e32 = fbm(_e31);
    let _e35 = fbm((_e29 + vec2<f32>(_e32)));
    let _e38 = fbm((_e22 + vec2<f32>(_e35)));
    return _e38;
}
`

export default {

    vertex:`
        struct Uniforms {
            modelViewProjectionMatrix : mat4x4<f32>;
        };
        [[binding(0), group(0)]] var<uniform> uniforms : Uniforms;
        ${app_params}
        ${params}
        
        struct VertexOutput {
          [[builtin(position)]] Position : vec4<f32>;
          [[location(0)]] Coords: vec2<f32>;
        };
        
        [[stage(vertex)]]
        fn main(
            [[location(0)]] position : vec4<f32>
        ) -> VertexOutput {
          var output : VertexOutput;
          
          var pos = position;
          pos.x = pos.x + params.xpos;
          pos.y = pos.y + params.ypos;
         
          
          output.Position = uniforms.modelViewProjectionMatrix * pos;
          output.Coords = vec2(pos.xy);
          return output;
        }
    `,

    fragment:`
        ${params}
        ${app_params}
        ${color_map}
        
        fn solveColor(uv: vec2<f32>) -> vec3<f32> {
            var uv_1: vec2<f32>;
            var col: vec3<f32>;
        
            uv_1 = uv;
            let _e9 = env.params.x;
            let _e10 = uv_1;
            let _e22 = env.params.x;
            let _e23 = uv_1;
            col = (vec3<f32>(0.5) + (0.5 * sin(((vec3<f32>(_e22) + _e23.xyx) + vec3<f32>(f32(0), f32(2), f32(4))))));
            let _e60 = col;
            col = mix(normalize(vec3<f32>(128.0, 127.0, 238.0)), _e60, vec3<f32>(1.0));
            let _e84 = col;
            col = mix(normalize(vec3<f32>(105.0, 180.0, 229.0)), _e84, vec3<f32>(0.6000000238418579));
            let _e88 = col;
            return _e88;
        }
        
        [[stage(fragment)]]
        fn main(
            [[location(0)]] Coords:vec2<f32>
        ) -> [[location(0)]] vec4<f32> {
            var resolution:vec2<f32> = env.params.yz;
            var time:f32 = env.params.x;
            var status:f32 = params.pixelStatus;
            var center = vec2(0.5,0.5);
            var speed = 2.5;

      
            var uv:vec2<f32> = Coords.xy/resolution.xy;
            
            
            /// BUILD "OFF" MODE COLOR ///// 
            var shade:f32 = pattern(uv);
            var col:vec4<f32> = vec4(colormap(shade).rgb, shade);
            
            
            /// BUILD "ON" mode color ///// 
            var texcol = vec3(0.);
            var invAr:f32 = resolution.y / resolution.x;
            var x:f32 = (center.x-uv.x);
            var y:f32 = (center.y-uv.y) * invAr;

             var r:f32 = -sqrt(x*x + y*y); //uncoment this line to symmetric ripples
 
            var z:f32 = 1.0 + 0.5*sin((r + time * speed) / 0.1);
            var distanceFromCenter:f32 = sqrt(x * x + y * y);
            var sinArg:f32 = distanceFromCenter * 10.0 - time;
            var slope:f32 = cos(sinArg) ;
    
            texcol.x = texcol.x + sin(z) * 2.0 * params.offset;
      
            /// finalize colors //// 
            var off:vec3<f32> = vec3(col.xyz);
            var on:vec3<f32> = normalize(col).xyz + vec4(solveColor(uv + normalize(vec2(x, y)) * slope + texcol.xy),1.).xyz;
            
            
            
            return vec4(mix(off,on,status),1.0);
       }
    `
}
/*
   col = 0.5 + 0.5*cos(time+uv.xyx+vec3(0.0,2.0,4.0));
 */