export function build_assets(){

    // build asset list
    let assets = [
        "./assets/u.jpg",
        "./assets/n.jpg",
        "./assets/ko.jpg",
    ]

    assets = assets.map(itm => {
        //@ts-ignore
        return new Promise((res,rej)=>{
            let img = new Image();
            img.src = itm;
            img.onload = ()=>{res(img);}
            img.onerror = (e)=>{rej(e)}
        })
    })

    //@ts-ignore
    return Promise.all(assets);
}