
export default class Grid {
    gridWidth:number = 100;
    gridHeight:number = 100;
    instance:any;
    numInstances:number;
    grid_positions:Array<number> = [];

    constructor(instance,gridWidth:number=100,gridHeight:number=100) {


        this.instance = instance;
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;

        this.numInstances = gridWidth * gridHeight;

    }

    /**
     * Builds instanced positions for the grid.
     */
    _build(){
        let res = 1;
        let cols = this.gridWidth / res;
        let rows = this.gridHeight / res;

        let positions = [];

        // build instanced positions
        for(let y = 0; y < rows; ++ y) {
            for (let x = 0; x < cols; ++x) {
                let _x = x * res;
                let _y = y * res;

                // center grid
                _y -= (rows / 2) * res;
                _x -= (cols / 2) * res;


                positions.push(
                    _x,
                    _y,
                    0,
                    1);
            }
        }
        this.grid_positions = positions;
    }

}