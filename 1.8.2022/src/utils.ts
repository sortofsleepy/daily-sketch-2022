import mat4 from "./library/math/mat4";
import vec3 from "./library/math/vec3";

export function makeProjection(){
    const projection = mat4.create();
    const aspect = Math.abs(window.innerWidth / window.innerHeight)
    mat4.perspective(projection,(2 * Math.PI) / 5, aspect, 1, 1000);
    return projection;
}


function getTransformMatrix(projection){
    const viewMatrix = mat4.create();
    mat4.translate(viewMatrix,viewMatrix,vec3.fromValues(0,0,-20))
    const now = Date.now() / 1000
    mat4.rotate(
        viewMatrix,
        viewMatrix,
        1,
        vec3.fromValues(Math.sin(now),Math.cos(now),0)
    )

    const mvp = mat4.create();
    mat4.multiply(mvp,projection,viewMatrix);

    return mvp as Float32Array;

}