

export default class CharReader {
    canvas:HTMLCanvasElement;
    ctx:CanvasRenderingContext2D;
    constructor() {
        this.canvas = document.createElement("canvas");
        this.ctx = this.canvas.getContext("2d");
    }

    /**
     * Reads a character image. Returns the image data for the particular char.
     * @param img
     */
    read_image(img:HTMLImageElement,width:number = img.width / 2, height:number = img.height / 2){

        this.canvas.width = width;
        this.canvas.height = height;


        this.ctx.drawImage(img,0,0,width,height);

        let img_data = this.ctx.getImageData(0,0,width,height);


        return img_data;
    }

    debug_image_data(data:ImageData){
        this.ctx.putImageData(data,0,0);
        document.body.appendChild(this.canvas)

    }
}