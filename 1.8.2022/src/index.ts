import createInstance from "./library/core/instance";
import {build_assets} from "./assets";
import CharReader from "./charreader";
import Mesh from "./library/framework/mesh";
import {createWGSLShader} from "./library/core/shader";
import grid_shader from "./shaders/plane_grid_shader"
import {
    DEPTH24_STENCIL8,
    FLOAT32_4, SHADER_FRAGMENT, SHADER_VERTEX,
    STORE_OP,
    TRIANGLE_LIST,
    UNIFORM_TYPE
} from "./library/constants";
import Buffer from "./library/core/buffer";
import Plane from "./library/geometry/plane";
import mat4 from "./library/math/mat4";
import vec3 from "./library/math/vec3";
import Stats from "stats.js"
import gsap from "gsap"

// build reader - this will read asset images into pixels.
const reader = new CharReader();
const char_data = {
    0:{
        img_data:[], // general image data
        on_pixels:[] // pixel status values. 1 = "on" , 0 = "off"
    },
    1:{
        img_data:[],
        on_pixels:[]
    },
    2:{
        img_data:[],
        on_pixels:[]
    }
}

// check if debug flag is set
function checkDebug() {
    const urlSplit = window.location.href.split('?')
    const query = new URLSearchParams(urlSplit[1])
    if (query.has('debug')) {
        return true
    }
    return false
}

let stats =null;
const IS_DEBUG = checkDebug();

// if debug, show fps
if(IS_DEBUG){
    stats = Stats();
    document.body.appendChild(stats.domElement);
}

// build webgpu instance
createInstance(instance => {
    window["instance"] = instance;
    build_assets().then(assets => {
        //@ts-ignore
        start(assets);
    })
});

/**
 * Kicks things off after assets are loaded and instance is created
 * @param assets
 */
function start(assets:Array<HTMLImageElement>) {
    const instance = window["instance"];
    const device = instance.device;

    // build out image data
    assets.forEach((itm, idx) => {
        char_data[idx].img_data = reader.read_image(itm);
    })

    let shader = createWGSLShader(device, grid_shader, {
        buffers: [
            {
                arrayStride: 4 * 4,
                attributes: [
                    {
                        shaderLocation: 0,
                        offset: 0,
                        format: FLOAT32_4
                    }
                ]
            }
        ]
    })

    let resolution = 1;
    let plane = new Plane(resolution, resolution, 1, 1);
    let mesh = new Mesh(instance);

    let grid = build_grid(instance, resolution);
    mesh.addAttribute(0, new Float32Array(plane.vertices))
    mesh.setIndexData(plane.indices)

    /// build params
    let params = build_grid_params(instance, grid);
    let ubo = build_uniforms(instance);

    // build pipeline layout
    let pipeline_layout = device.createPipelineLayout({
        bindGroupLayouts: [
            ubo.layout,
            params.dynamicBindGroupLayout
        ]
    })

    // build render pipeline
    const pipeline = device.createRenderPipeline({
        layout: pipeline_layout,
        vertex: shader.vertexStage,
        fragment: shader.fragmentStage,
        primitive: {
            topology: TRIANGLE_LIST,
            cullMode: "back"
        },
        depthStencil: {
            depthWriteEnabled: true,
            depthCompare: 'less',
            format: DEPTH24_STENCIL8
        }
    })


    // build a render pass.
    let render_pass = {
        colorAttachments: [
            {
                view: undefined, // gets set later
                loadValue: {
                    r: 0.0,
                    g: 0.5,
                    b: 0.5,
                    a: 1.0
                },
                storeOp: 'store'
            }
        ],
        depthStencilAttachment: {
            view: instance.getDefaultDepthView(),
            depthLoadValue: 1.0,
            depthStoreOp: STORE_OP,
            stencilLoadValue: 0,
            stencilStoreOp: STORE_OP
        }
    }




    ////// SETUP CAMERA ///////////
    const projection = mat4.create();
    const aspect = Math.abs(window.innerWidth / window.innerHeight)
    mat4.perspective(projection, 60.0, aspect, 1, 10000);

    const viewMatrix = mat4.create();
    mat4.translate(viewMatrix, viewMatrix, vec3.fromValues(0, 0, -50))

    const mvp = mat4.create();
    mat4.multiply(mvp, projection, viewMatrix);

    let transformedMatrix = mvp as Float32Array

    device.queue.writeBuffer(ubo.buffer.getObject(), 0, transformedMatrix.buffer, transformedMatrix.byteOffset, transformedMatrix.byteLength);

    // manages the state of each pixel on the screen
    let pixels = new Float32Array(128 * 128)

    // figure out which pixels for each character are "on"
    for(let char in char_data){
        let element = char_data[char];
        let u = element["img_data"]["data"];
        let u_len = u.length;

        for(let i = 0; i < u_len; i+= 4){
            if(u[i] < 10){
                element.on_pixels.push(2.0);
            }else{
                element.on_pixels.push(0.0);
            }
        }
    }

    let count = 0;
    setInterval(()=>{

        char_data[count].on_pixels.onUpdate = ()=>{
            params.togglePixels(pixels);
            params.updateBuffer();
        }

        // there is apparently a highly jank way of tweening array->array
        // gsap.to(<array to change>, <duration>, <array data to apply>)
        // https://greensock.com/forums/topic/8325-tween-an-array-value/#comment-32321_wrap
        // Note that duration is not in milliseconds for some reason but I believe in seconds.
        //@ts-ignore
        gsap.to(pixels,2.4,char_data[count].on_pixels);
        if(count === 2){
            count = 0;
        }else{
            count ++;
        }
    },1500)


    /// ON RESIZE /////////
    // resize things when window resizes.
    instance.addResizeCallback(() => {
        const aspect = Math.abs(window.innerWidth / window.innerHeight)
        mat4.perspective(projection, 60.0, aspect, 1, 10000);

        const viewMatrix = mat4.create();
        mat4.translate(viewMatrix, viewMatrix, vec3.fromValues(0, 0, -50))

        const mvp = mat4.create();
        mat4.multiply(mvp, projection, viewMatrix);

        let transformedMatrix = mvp as Float32Array

        device.queue.writeBuffer(ubo.buffer.getObject(), 0, transformedMatrix.buffer, transformedMatrix.byteOffset, transformedMatrix.byteLength);


        render_pass.depthStencilAttachment = {
            //view:depthTexture.createView(),
            view: instance.getDefaultDepthView(),
            depthLoadValue: 1.0,
            depthStoreOp: STORE_OP,
            stencilLoadValue: 0,
            stencilStoreOp: STORE_OP
        }
    })

    let animate = () => {
        requestAnimationFrame(animate);

        if(IS_DEBUG){
            stats.begin();
        }

        // ensure we only operate when we aren't rebuilding the swapchain, etc
        if(!instance.resizing){


            // update app time.
            update_time(ubo.app_buffer);


            // get current frame rendered
            render_pass.colorAttachments[0].view = instance.getCurrentTexture();

            // build command encoder
            const commandEncoder = device.createCommandEncoder();

            // start render pass
            const passEncoder = commandEncoder.beginRenderPass(render_pass);

            // set the pipeline to use
            passEncoder.setPipeline(pipeline);

            // set the bind group we want to use.
            passEncoder.setBindGroup(0,ubo.bind_group);

            // Dynamic uniforms work kind of like instanced attributes.
            // here we set the individual uniform data for each pixel and finally draw each pixel.
            const dynamicOffsets = [0];
            for (let i = 0; i < grid.num_instances; ++i) {
                dynamicOffsets[0] = i * params.alignedUniformBytes;
                passEncoder.setBindGroup(1, params.dynamicBindGroup, dynamicOffsets);
                mesh.draw(passEncoder)
            }

            // finish renderpass
            passEncoder.endPass();

            // submit for processing
            device.queue.submit([commandEncoder.finish()]);
        }


        if(IS_DEBUG){
            stats.end();
        }
    }


    animate();


}



/**
 * Updates the current runtime for the app.
 * @param params
 */
function update_time(params:Buffer){
    params.data[0] = performance.now() * 0.0005;
    params.updateData();
}

/**
 * Builds grid positions
 * @param instance {Object} Instance object.
 * @param res {number} resolution for the grid.
 */
function build_grid(instance,res = 1){
    let size = 256 / 2; // each image is 256x256, divide by 2 for perf, then scale up

    let cols = size;
    let rows = size;

    let rolledPositions = [];

    for(let x = 0; x < cols ; ++x){
        for(let y = 0; y < rows; ++y){
            let _x = res * ( x - size / 2 );
            let _y = res * ( y - size / 2 );
            rolledPositions.push([-_y,_x,0,1]);
        }
    }

    return {
        num_instances:rolledPositions.length ,
        cols:cols,
        rows:rows,
        rolledPositions
    }
}

/**
 * Build general uniforms
 * @param instance {WebGPUInstance}
 */
function build_uniforms(instance){

    let camera_ubo = new Buffer(instance,{
        size:4 * 16
    });
    let app_ubo = new Buffer(instance,{
        size:4 * 4
    });

    app_ubo.set({
        data:[
            0,
            window.innerWidth,
            window.innerHeight,
            0
        ],
        usage:GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST
    })

    let layout = instance.device.createBindGroupLayout({
        entries: [
            {
                binding: 0,
                visibility: GPUShaderStage.VERTEX,
                buffer: {
                    type: UNIFORM_TYPE,
                    minBindingSize: 4 * 16,
                },
            },

            {
                binding: 1,
                visibility: GPUShaderStage.VERTEX | GPUShaderStage.FRAGMENT,
                buffer: {
                    type: UNIFORM_TYPE,
                    minBindingSize: 4 * 4,
                },
            },
        ],
    })

    layout.label = "camera-layout"

    let ubo_desc = instance.device.createBindGroup({
        layout:layout,
        entries:[
            {
                binding:0,
                resource:{
                    buffer:camera_ubo.getObject()
                }
            },
            {
                binding:1,
                resource:{
                    buffer:app_ubo.getObject()
                }
            }
        ]
    })

    return {
        buffer:camera_ubo,
        app_buffer:app_ubo,

        bind_group:ubo_desc,
        layout:layout
    }


}




/**
 * Builds parameters to feed to the grid. Uses the idea of dynamic uniforms which can be individually set with different values
 * for each piece of geometry you want to draw. A very similar idea to instanced attributes, but more useful for smaller bits of data.
 * 
 * @param instance {WebGPUInstance} a WebGPUInstance object, or any object that has a GPUDevice object attached to it.
 * @param grid {Object} object created after calling build grid
 */
function build_grid_params(instance,grid){
    const numTriangles = grid.num_instances;
    
    // number of items * byte size per item. 
    const uniformBytes = 3 * Float32Array.BYTES_PER_ELEMENT;
    
    // To be honest, I'm not sure how this value was derived, originally found in WebGPU samples site from @austinEng
    // In Vulkan you need to follow minimum memory alignment specifications for your particular GPU but Vulkan can help derive that for you; in
    // an error message I came across, it seemed to suggest 256 was the minimum alignment value you need to align by.
    const alignedUniformBytes = Math.ceil(uniformBytes / 256) * 256;
    
    // align byte size to float byte size
    const alignedUniformFloats = alignedUniformBytes / Float32Array.BYTES_PER_ELEMENT;
    
    // build a buffer for all the uniforms
    const uniformBuffer = instance.device.createBuffer({
        size: numTriangles * alignedUniformBytes + Float32Array.BYTES_PER_ELEMENT,
        usage: GPUBufferUsage.COPY_DST | GPUBufferUsage.UNIFORM,
    });

    // buffer data
    const uniformBufferData= new Float32Array(
        numTriangles * alignedUniformFloats
    );

    for(let i = 0; i < numTriangles; ++i){
        let position = grid.rolledPositions[i];
        uniformBufferData[alignedUniformFloats * i + 0] = position[0]; // x position for pixel
        uniformBufferData[alignedUniformFloats * i + 1] = position[1]; // y position for pixel 
        uniformBufferData[alignedUniformFloats * i + 2] = 0.0; // pixel status, whether it should be "on"(1.0) or "off"(0.0)
        uniformBufferData[alignedUniformFloats * i + 3] = Math.random() + 0.5 // offset to help with any animation.

    }

    // TODO not totally sure how the max length is calculated, probably just needs to be large enough to fit all elements.
    //const maxMappingLength = (1024 * 1024) / Float32Array.BYTES_PER_ELEMENT;
    const maxMappingLength = numTriangles;

    for (
        let offset = 0;
        offset < uniformBufferData.length;
        offset += maxMappingLength
    ) {

        instance.device.queue.writeBuffer(
            uniformBuffer,
            offset * Float32Array.BYTES_PER_ELEMENT,
            uniformBufferData.buffer,
            uniformBufferData.byteOffset + offset * Float32Array.BYTES_PER_ELEMENT
        );
    }

    const dynamicBindGroupLayout = instance.device.createBindGroupLayout({
        entries: [
            {
                binding: 0,
                visibility: SHADER_VERTEX | SHADER_FRAGMENT,
                buffer: {
                    type: UNIFORM_TYPE,
                    hasDynamicOffset: true,
                    minBindingSize: 4 * 4,
                },
            },
        ],
    });

    const dynamicBindGroup = instance.device.createBindGroup({
        layout: dynamicBindGroupLayout,
        entries: [
            {
                binding: 0,
                resource: {
                    buffer: uniformBuffer,
                    offset: 0,
                    size: 4 * Float32Array.BYTES_PER_ELEMENT,
                },
            },
        ],
    });



    return {

        dynamicBindGroupLayout:dynamicBindGroupLayout,
        dynamicBindGroup:dynamicBindGroup,
        alignedUniformBytes:alignedUniformBytes,

        /**
         * tells the indices specified inside the UBO to flip their status to "on"
         * @param indices {Array} an array of pixels to turn on
         */
        togglePixels(indices){

            for(let i = 0; i < numTriangles; ++i){
                let index = indices[i];
                uniformBufferData[alignedUniformFloats * i + 2] = index;
            }
        },

        updateBuffer(){
            const maxMappingLength = numTriangles;

            for (
                let offset = 0;
                offset < uniformBufferData.length;
                offset += maxMappingLength
            ) {

                instance.device.queue.writeBuffer(
                    uniformBuffer,
                    offset * Float32Array.BYTES_PER_ELEMENT,
                    uniformBufferData.buffer,
                    uniformBufferData.byteOffset + offset * Float32Array.BYTES_PER_ELEMENT
                );
            }

        }
    }

}