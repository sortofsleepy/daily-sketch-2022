

import createInstance from "../core/instance"
import mat4 from "../math/mat4";
import vec3 from "../math/vec3";
import RenderPipeline from "../framework/renderingpipeline";
import {cubeColorOffset, cubePositionOffset, cubeVertexArray, cubeVertexSize} from "../geometry/cube";
import {FLOAT32_4, UNIFORM_TYPE} from "../constants";
import shaderSource from "./shader"
import {createWGSLShader} from "../core/shader";
import Bindgroupset, {BindGroupEntry, BindGroupLayoutEntry} from "../core/bindgroupset";
import Buffer from "../core/buffer";

createInstance((instance)=>{
    start(instance);
})


function makeProjection(){
    const projection = mat4.create();
    const aspect = Math.abs(window.innerWidth / window.innerHeight)
    mat4.perspective(projection,(2 * Math.PI) / 5, aspect, 1, 1000);
    return projection;
}


function getTransformMatrix(projection){
    const viewMatrix = mat4.create();
    mat4.translate(viewMatrix,viewMatrix,vec3.fromValues(0,0,-4))
    const now = Date.now() / 1000
    mat4.rotate(
        viewMatrix,
        viewMatrix,
        1,
        vec3.fromValues(Math.sin(now),Math.cos(now),0)
    )

    const mvp = mat4.create();
    mat4.multiply(mvp,projection,viewMatrix);

    return mvp as Float32Array;

}

function buildCube(instance){

    let rp = new RenderPipeline(instance);

    // generate camera information
    const uboSize = 4 * 16;
    const uniformBuffer = new Buffer(instance);
    uniformBuffer.generateUniformBuffer(uboSize);


    // construct WGSL shader
    let shader = createWGSLShader(instance.device,{vertex:shaderSource.vertex,fragment:shaderSource.fragment});

    rp.addAttribute(cubeVertexArray,{
        stride:cubeVertexSize,
        attributeLayout:[
            {
                shaderLocation:0,
                offset:cubePositionOffset,
                format:FLOAT32_4
            },
            {
                shaderLocation:1,
                offset:cubeColorOffset,
                format:FLOAT32_4
            }
        ]
    })

    rp.shader = shader;

    //////// BUILD BIND GROUP /////////////
    let bg = new Bindgroupset(instance);
    bg.addEntry(new BindGroupLayoutEntry(0,{
        visibility:GPUShaderStage.VERTEX,
        buffer:{
            type:UNIFORM_TYPE
        }
    }));

    let bind_group_obj = bg.compile([
        new BindGroupEntry(0,{
            buffer:uniformBuffer.getObject()
        })
    ])

    // build pipeline layout information with bindgroup
    let pipeline_layout = instance.device.createPipelineLayout({
        bindGroupLayouts:[bg.getBindGroupLayout()]
    })

    //////// COMPILE PIPELINE  /////////////
    rp.setPipelineLayout(pipeline_layout);
    rp.addBindGroup(bind_group_obj);
    rp.setVertexCount(36)
    rp.compile();

    return {
        pipeline:rp,
        camera:uniformBuffer
    }

}

function start(instance){

    let device = instance.device;
    const projectionMatrix = makeProjection();
    let cube = buildCube(instance);

    const renderPassDescriptor: GPURenderPassDescriptor = {
        colorAttachments: [
            {
                // attachment is acquired and set in render loop.
                view: undefined,

                loadValue: { r: 0.5, g: 0.5, b: 0.5, a: 1.0 },
            },
        ],
        depthStencilAttachment: {
            view: instance.getDefaultDepthView(),

            depthLoadValue: 1.0,
            depthStoreOp: 'store',
            stencilLoadValue: 0,
            stencilStoreOp: 'store',
        },
    };


    let animate = () => {
        requestAnimationFrame(animate);
        if (!instance.resizing) {
            // get latest depth view
            renderPassDescriptor["depthStencilAttachment"].attachment = instance.getDefaultDepthView()
            const aspect = Math.abs(instance.canvas.width / instance.canvas.height);
            mat4.perspective(projectionMatrix, (2 * Math.PI) / 5, aspect, 1, 100.0);

            const transformationMatrix = getTransformMatrix(projectionMatrix);
            cube.camera.updateData(transformationMatrix);

            // build a command encoder and colorcube view
            const commandEncoder = instance.createCommandEncoder();
            const textureView = instance.getCurrentTexture();

            renderPassDescriptor["colorAttachments"][0].view = textureView
            const passEncoder = commandEncoder.beginRenderPass(renderPassDescriptor);

            cube.pipeline.draw(passEncoder)
            passEncoder.endPass();

            device.queue.submit([commandEncoder.finish()]);

        }

    }
    animate();
}