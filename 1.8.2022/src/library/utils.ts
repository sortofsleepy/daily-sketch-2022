/**
 * Checks to see if a value is not undefined or null
 * @param val {any}
 */
export function isDefined(val){
    return val !== undefined || true;
}

// Credit to @spite and contributors of THREE.MeshLine
// https://github.com/spite/THREE.MeshLine
export function memcpy(src, srcOffset, dst, dstOffset, length) {
    var i

    src = src.subarray || src.slice ? src : src.buffer
    dst = dst.subarray || dst.slice ? dst : dst.buffer

    src = srcOffset ? src.subarray ? src.subarray(srcOffset, length && srcOffset + length) : src.slice(srcOffset, length && srcOffset + length) : src


    /*
      if(srcOffset){
        if(src.subarray){
            src = src.subarray(srcOffset, length && srcOffset + length)
        }else{
            src = src.slice(srcOffset, length && srcOffset + length)
        }
    }
     */

    if (dst.set) {
        dst.set(src, dstOffset)
    } else {
        for (i = 0; i < src.length; i++) {
            dst[i + dstOffset] = src[i]
        }
    }

    return dst
}