
/// Numerical types.
export const FLOAT2:string = "float2";
export const FLOAT3:string = "float3";
export const FLOAT4:string = "float4";
export const MAT4:string = "mat4";
export const FLOAT:string = "float";

export const FLOAT32_4 = "float32x4";
export const FLOAT32_3 = "float32x3";
export const FLOAT32_2 = "float32x2";
export const FLOAT32 = "float32";

/// shader types.
export const SHADER_VERTEX = GPUShaderStage.VERTEX;
export const SHADER_FRAGMENT = GPUShaderStage.FRAGMENT;
export const SHADER_COMPUTE = GPUShaderStage.COMPUTE;

/// topology types
export const TRIANGLE_LIST:string = "triangle-list"
export const POINT_LIST:string = "point-list"
export const LINE_LIST:string = "line-list"
export const TRIANGLE_STRIP = "triangle-strip"

/// Texture format specific.
export const DEPTH24_STENCIL8 = "depth24plus-stencil8"
export const BGRA8UNORM:string = "bgra8unorm";
export const RGBA8UNORM:string = "rgba8unorm"

export const LINEAR:string = "linear";
export const VERTEX_STEP_MODE = "vertex";
export const INSTANCE_STEP_MODE = "instance";

/// Texture specific
export const DEFAULT_TEXTURE_USAGE = GPUTextureUsage.RENDER_ATTACHMENT;
export const RENDER_ATTACHMENT = GPUTextureUsage.RENDER_ATTACHMENT;
export const SAMPLER:string = "sampler";
//export const SAMPLER_TEXTURE: string = "sampled-color"


/// Buffer usage specific
export const UNIFORM = GPUBufferUsage.UNIFORM;
export const UNIFORM_TYPE = "uniform";
export const COPY_DST = GPUBufferUsage.COPY_DST;

export const STORAGE_BUFFER:string = 'storage-buffer';
export const STORE_OP:string = "store";
export const UNIFORM_BUFFER:string = "uniform-buffer"

/// sample types
export const SAMPLE_FLOAT = "float"
export const SAMPLE_FILTER = "filtering"

/// cull modes
export const CULL_BACK:string = "back";
export const CULL_FRONT:string = "front"

/// math related
export const PI = 3.141592653589793238462643383279502884197169399375105820974944;
export const PI_2 = PI * PI;

// color state related
