import Mesh from "../deprecate/mesh";
import {
    CULL_BACK,
    FLOAT32_2,
    LINEAR,
    SAMPLE_FILTER, SAMPLE_FLOAT,
    SHADER_FRAGMENT
} from "../constants";
import {createWGSLShader} from "../core/shader";
import DescriptorLayout from "../deprecate/bindgroup";
import Texture from "../core/texture";
import default_shaders from "../shaders/quad"

export default class Quad extends Mesh{
    instance:any;
    bindgroup:any;
    sampler:any;
    layerTexture:Texture;
    descriptorLayout:DescriptorLayout;
    customUniformValues:any;
    textureDirty:boolean = false;

    testBuffer:any;
    constructor(instance,{
        uniformValues=[],
        shader = null,
        testTexture=null,
        targets=[],
        colorStates = [],
        descriptors = []
    }={}) {
        super(instance);

        this.instance = instance;
        this.descriptorLayout = new DescriptorLayout(instance);

        this.descriptorLayout
            .addDescriptor({
            binding:0,
            visibility:SHADER_FRAGMENT,
            sampler:{
                type:SAMPLE_FILTER
            }
            })
            .addDescriptor({
                binding:1,
                visibility:SHADER_FRAGMENT,
                texture:{
                    samplerType:SAMPLE_FLOAT
                }
            })

        if(descriptors.length > 0){
            descriptors.forEach(descriptor => {
                this.descriptorLayout.addDescriptor(descriptor)
            })
        }

        // TODO make min/mag user set-able.
        this.sampler = this.instance.device.createSampler({
            magFilter:LINEAR,
            minFilter:LINEAR
        });

        this.layerTexture = testTexture !== null ? testTexture : new Texture(instance,{
            usage:GPUTextureUsage.TEXTURE_BINDING |
                GPUTextureUsage.COPY_DST |
                GPUTextureUsage.RENDER_ATTACHMENT,
        });

        this.customUniformValues = uniformValues.length > 0 ? uniformValues : [];

        this._setupMesh({
            uniformValues:uniformValues,
            shader:shader,
            targets:targets,
            colorStates:colorStates
        });

    }

    setTexture(tex:Texture){
        this.layerTexture = tex;
        this.textureDirty = true;
    }


    renderQuad(passEncoder){

        // if render texture has changed, re-build bind group
        // (unfortunately dynamic references don't seem to work)
        if(this.textureDirty){
            this.bindgroup = this.instance.device.createBindGroup({
                layout:this.getBindGroupLayout(),
                entries:[
                    {
                        binding:0,
                        resource:this.sampler
                    },
                    {
                        binding: 1,
                        resource: this.layerTexture.getView()
                    },
                    ...this.customUniformValues
                ]
            })

            this.textureDirty = false;

        }

        if(this.bindgroup){
            passEncoder.setBindGroup(0,this.bindgroup);
        }

        if(this.layerTexture){
            this.draw(passEncoder)
        }

    }

    _setupMesh({
        uniformValues = [],
        shader = null,
        targets=[],
        colorStates=[]
    }={}){
        // setup vertices
        let vertices = [
            -1.0, 1,
            -1.0, -4.0,
            4.0, 1.0
        ]



        this.setNumVertices(3);
        this.addAttribute(new Float32Array(vertices));


        if(targets.length < 1){
            this.setShader(createWGSLShader(this.instance.device,{
                vertex:shader !== null ? shader.vertex : default_shaders.vertex,
                fragment:shader !== null ? shader.fragment : default_shaders.fragment
            }))
        }else{
            this.setShader(createWGSLShader(this.instance.device,{
                vertex:shader !== null ? shader.vertex : default_shaders.vertex,
                fragment:shader !== null ? shader.fragment : default_shaders.fragment
            },{
                targets:targets
            }))
        }


        this.descriptorLayout.compileDescriptorLayout();
        let pipelineLayout = this.descriptorLayout.generatePipelineLayout();

        if(colorStates.length > 0){
            colorStates.forEach(state => {
                this.addColorState(state);
            })
        }

        this.compile({
            layout:pipelineLayout,
            buffers:[
                {
                    arrayStride:2 * 4,
                    attributes:[
                        {
                            shaderLocation:0,
                            offset:0,
                            format:FLOAT32_2
                        }
                    ]
                }
            ],
            cullMode:CULL_BACK
        })


        this.bindgroup = this.instance.device.createBindGroup({
            layout:this.getBindGroupLayout(),
            entries:[
                {
                    binding:0,
                    resource:this.sampler
                },
                {
                    binding: 1,
                    resource: this.layerTexture.getView()
                },
                ...uniformValues
            ]
        })





    }


}