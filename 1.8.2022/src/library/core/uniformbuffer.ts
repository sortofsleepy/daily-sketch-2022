/**
 * An attempt at making something like a Uniform Buffer Object
 */
import Buffer from "./buffer";
import {COPY_DST} from "../constants";

interface UniformValue {
    name:string;
    value:any;
    type:string;
}

export default class UniformBuffer {

    // reference to a WebGPU instance
    instance:any;

    // the size of the buffer in bytes
    bufferSize:number = 0;

    // stores the raw information about your uniform buffer
    rawBufferData:Array<UniformValue> = [];

    // stores the actual uniform information we send to the shader.
    bufferData:Float32Array;

    // contains the declaration for your uniform buffer that you can insert into your shader.
    uniformStruct:Array<string> = [];

    // type name for the struct in your shader
    blockName:string;

    // the actual buffer
    buffer:Buffer;

    // indicates that we need to recompile all arrays in the event we
    // add a new uniform value.
    bufferDirty:boolean = false;

    constructor(instance,{
        blockName = "Uniform"
    }={}) {


        this.blockName = blockName;
        this.uniformStruct.push(
            `
              [[block]] struct ${blockName} {
              
            `
        );

        this.buffer = new Buffer(instance);
    }

    /**
     * Adds a floating point value to the UBO
     * @param name {string} the name to refer to within the shader and the UBO itself.
     * @param val {number} the value for this uniform value.
     */
    setFloat(name:string,val:number){
        this.bufferSize += 1;
        this.bufferDirty = true;
        this.uniformStruct.push(
            `${name}:f32;\n`
        );

        this.rawBufferData.push({
            name:name,
            value:val,
            type:"float"
        });

    }

    setVec3(name:string,val:[number,number,number]){
        this.bufferSize += 3;
        this.bufferDirty = true;
        this.rawBufferData.push({
            name:name,
            value:val,
            type:"vec3"
        })

        this.uniformStruct.push(
            `${name}:vec3<f32>;\n`
        );

    }


    setVec4(name:string,val:[number,number,number,number]){
        this.bufferSize += 4;
        this.bufferDirty = true;
        this.rawBufferData.push({
            name:name,
            value:val,
            type:"vec4"
        })

        this.uniformStruct.push(
            `${name}:vec4<f32>;\n`
        );
    }

    setMat4(name:string,val:Float32Array){

        this.bufferSize += 16;
        this.bufferDirty = true;
        // ensure that we have enough components for a mat4
        if(val.length !== 16){
            return;
        }

        this.rawBufferData.push({
            name:name,
            value:val,
            type:"mat4x4"
        })

        this.uniformStruct.push(
            `${name}:mat4x4<f32>;\n`
        );
    }

    /**
     * Updates the specified uniform value
     * @param name {string} uniform name
     * @param newVal {any} the new uniform value.
     */
    updateValue(name:string,newVal:any){
        this.rawBufferData.forEach(itm => {
            if(itm.name === name){
                itm.value = newVal;
            }
        });

        this._updateDataBuffer();
    }

    /**
     * Prepares and compiles the buffer for use
     * @param binding {number} the binding number to refer to this UBO within the shader
     * @param group {number} the group number to that this UBO is a part of
     * @param variableName {number} the name to refer to this UBO
     * @param byteSize {number} bytesize for each component of the UBO.
     */
    compile({
        binding=0,
        group=0,
        variableName="uniforms",
        byteSize=4
            }={}){

        // make the closing brace for the struct and add the binding declaration
        this.uniformStruct.push(
            "\n};\n",
            `[[binding(${binding}), group(${group})]] var<uniform> ${variableName} : ${this.blockName};`,
        );

        // instantiate a new underlying WebGPU buffer for Uniform purposes
        // by default we assume all values are floats, so we keep the default bytesize of 4
        this.buffer.generateUniformBuffer(this.bufferSize,byteSize);

        // initialize Typed Array
        this.bufferData = new Float32Array(this.bufferSize);

        this._updateDataBuffer();
    }

    /**
     * Returns the raw WebGPU buffer
     */
    getRawBuffer(){
        return this.buffer.getObject();
    }

    /**
     * Returns the underlying buffer object.
     */
    getBuffer(){
        return this.buffer;
    }

    /**
     * Returns the shader code that can be used to refer to this UBO.
     */
    getShaderCode(){
        return this.uniformStruct.join("");
    }

    /**
     * Updates the data buffer with any updated values
     */
    _updateDataBuffer(){

        this.rawBufferData.forEach((u,i) => {

            switch (u.type){
                case "float":
                    this.bufferData.set([u.value],i);
                    break;

                default:

                    this.bufferData.set(u.value,i);
                    break;
            }

        })

        // update the buffer that gets sent.
        this.buffer.updateData(this.bufferData);

    }

}