import {BGRA8UNORM, DEPTH24_STENCIL8, TRIANGLE_LIST} from "../constants";
import {
    CoreObject,
    DepthStencilSettings,
    PipelineVertexState
} from "./interfaces";


export default class RenderPipeline implements CoreObject{


    // type of primitive to use for the pipeline
    topology:string;

    // reference to WebGPU instance
    instance:any;

    // color states for the pipeline
    colorStates:Array<any>;

    // reference to actual pipeline object
    pipeline:any;

    name:string;

    // the format to use for the swapchain image
    swapchainFormat:string = BGRA8UNORM;

    // settings for the depth state
    depthStencilState:DepthStencilSettings = {
        depthWriteEnabled:true,
        depthCompare:"less",
        format:DEPTH24_STENCIL8
    }

    // settings for the raster state
    rasterizationState:any = {
        cullMode:"back"
    }

    // settings for all the vertex buffers that will be associated with the pipeline.
    vertexState:any = {
        vertexBuffers:[]
    }

    // any blend states for the pipeline
    blendStates:Array<any> = []

    // pipeline layout
    pipelineLayout:any;

    constructor(wgpuCore,{
        topology=TRIANGLE_LIST,
        colorStates=[],
        blendStates = [],
        vertexState = [],
        pipelineLayout = null
    }={}) {

        this.topology = topology;
        this.instance = wgpuCore;
        this.colorStates = colorStates.length !== 0 ? colorStates : [];
        this.pipelineLayout = pipelineLayout;
        this.blendStates = blendStates;
        this.vertexState.vertexBuffers = vertexState.length !== 0 ? vertexState : [];

    }

    setVertexState(arrayStride,attributes:Array<PipelineVertexState>){
        this.vertexState.vertexBuffers = attributes;
    }

    /**
     * Returns the bind group layout at the specified index.
     * @param index {number} index of the bind group layout you want.
     */
    getBindGroupLayout(index=0){
        return this.pipeline.getBindGroupLayout(index);
    }

    setDepthWriteEnabled(enabled){
        this.depthStencilState.depthWriteEnabled = enabled;
        return this;
    }

    setDepthCompare(compare){
        this.depthStencilState.depthCompare = compare;
        return this;
    }

    setDepthStencilSettings(settings:DepthStencilSettings){
        this.depthStencilState = settings;
    }

    setTopology(topology:string){
        this.topology = topology;
    }

    buildPipeline(shader,{
        layout=null,
        cullMode="back",
        buffers=[]
    }={}){


        let colorStateBuild = [];
        this.colorStates.forEach(state => {
            let cstate:GPUColorStateDescriptor = {
                format:state.format,
                alphaBlend:state.alphaBlend,
                colorBlend:state.colorBlend,
                writeMask:state.writeMask
            }
            colorStateBuild.push(cstate);
        })

        let settings = {
            vertex: shader.vertexStage,
            fragment: shader.fragmentStage,
            colorStates:colorStateBuild,
            primitive: {
                topology: this.topology,
                cullMode:cullMode
            },
            blendStates:this.blendStates,
            depthStencil:this.depthStencilState
        }


        // check for the step mode in each buffer object, default to "vertex" if not already there
        buffers.forEach(buffer => {
            if(!buffer.hasOwnProperty("stepMode")){
                buffer.stepMode = "vertex";
            }
        })

        // buffers are appended separately in case vertices are declared in-shader
        if(buffers.length > 0){
            settings["vertex"]["buffers"] = buffers;
        }

        // if pipeline layout is attached,
        if(layout !== null){
            settings["layout"] = layout;
        }

        this.pipeline = this.instance.device.createRenderPipeline(settings);


    }

    getObject(){
        return this.pipeline
    }

}