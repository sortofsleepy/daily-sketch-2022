import {BGRA8UNORM} from "../constants";

export interface ShaderSource {
    vertex?:string
    fragment?:string;
    compute?:string;
}

export interface ShaderSettings {
    vertexEntry:string;
    fragmentEntry:string;
}

export interface TargetSetting {
    format:string,
    blend:{
        alpha:{
            srcFactor:string,
            dstFactor:string,
            operation:string
        }
    }
}

/**
 * Generates an object containing stages for a shader. Assumes shader source will be or WGSL
 * @param device {WebGPUDevice} a WebGPU device object
 * @param sources {ShaderSource} a Shader source object containing the sources for the vertex and fragment stages.
 * @param shaderSettings {ShaderSettings} shader settings (currently only need to set entry point function)
 */
export function createWGSLShader(device,sources:ShaderSource, {
    vertexEntry="main",
    fragmentEntry="main",
    buffers = [],
    targets=[
    ]
}={}) {

    if(targets.length === 0){
        targets.push(   {
            format: BGRA8UNORM,
            blend:{
                alpha:{
                    srcFactor:"src-alpha",
                    dstFactor:"one-minus-src-alpha",
                    operation:"add"
                },
                color:{
                    srcFactor:"src-alpha",
                    dstFactor:"one-minus-src-alpha",
                    operation:"add"
                }
            }
        })
    }

        return {
            type:"WGSLShader",
            vertexStage:{
                module:device.createShaderModule({
                    code:sources.vertex
                }),
                entryPoint:vertexEntry,
                buffers:buffers
            },
            fragmentStage:{
                module: device.createShaderModule({
                    code:sources.fragment
                }),
                entryPoint: fragmentEntry,
                targets:targets
            }
        }

}

export function createWGSLComputeShader(device,sources:ShaderSource, {
    computeEntry="main"
}={}) {
    return {
        type:"WGSLShader",
        computeStage:{
            module:device.createShaderModule({
                code:sources.compute
            }),
            entryPoint:computeEntry
        },
    }

}
export function createGLSLShader(
    device,
    compiler,
    sources:ShaderSource,
    shaderSettings={
    vertexEntry:"main",
    fragmentEntry:"main",
    targets:[
        {
            format: BGRA8UNORM
        }
    ]
}){
    const glsl_ = compiler;
    return {
        type:"GLSLShader",
        vertexStage:{
            module:device.createShaderModule({
                code:glsl_.compileGLSL(sources.vertex,"vertex")
            }),
            entryPoint:"main"
        },
        fragmentStage:{
            module: device.createShaderModule({
                code:glsl_.compileGLSL(sources.fragment,"fragment")
            }),
            entryPoint: "main",
            targets:shaderSettings.targets
        }
    }
}

