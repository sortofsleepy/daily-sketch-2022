import {BGRA8UNORM, RGBA8UNORM} from "../constants";
import {CoreObject} from "./interfaces";


export default class Texture implements CoreObject{

    instance:any;

    // whether or not the colorcube is ready to use.
    ready:boolean = false;

    textureObject:any;

    // Settings for how the texture gets used.
    usage:number;

    // format for the texture
    format:string;

    constructor(instance,{
        width = 256,
        height = 256,
        usage = null,
        format=RGBA8UNORM
    }={}){
        this.instance = instance;
        this.usage = usage;
        this.format = format;

        // build a base texture object.
        this.textureObject = this.instance.device.createTexture({
            size:[width,height,1],
            format:format,
            usage:usage !== null ? usage : GPUTextureUsage.SAMPLED | GPUTextureUsage.STORAGE
        })
    }

    getObject(){ return this.textureObject; }

    getView(){
        return this.textureObject.createView();
    }
    async loadTexture(path){

        const img = new Image();
        img.src = path;

        await img.decode();

        const imageBitmap = await createImageBitmap(img);

        this.textureObject = this.instance.device.createTexture({
            size:[imageBitmap.width,imageBitmap.height,1],
            format:this.format,
            usage: this.usage
        })



        /*
        this.instance.device.queue.copyImageBitmapToTexture(
            { imageBitmap },
            { texture: this.textureObject },
            [imageBitmap.width, imageBitmap.height, 1]
        );
         */

        this.instance.device.queue.copyExternalImageToTexture(
            {source:imageBitmap},
            { texture: this.textureObject },
            {
                width:imageBitmap.width,
                height:imageBitmap.height
            }
        )
    }


}