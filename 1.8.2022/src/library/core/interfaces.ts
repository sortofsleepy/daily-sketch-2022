export interface DepthStencilSettings {
    depthWriteEnabled:boolean;
    depthCompare:string;
    format:string;
}

// used to help define a base WebGPU object wrapper.
export interface CoreObject {
    getObject:Function;
    name:string;
}

// a description of an attribute when constructing a pipeline.
export interface VertexAttributeState {
    shaderLocation:number;
    offset:number;
    format:string;
}

// a description of one vertex buffer when creating a pipeline.
export interface PipelineVertexState{
    arrayStride:number;
    attributes:Array<VertexAttributeState>;
}