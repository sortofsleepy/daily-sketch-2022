import {DEFAULT_TEXTURE_USAGE, DEPTH24_STENCIL8, RGBA8UNORM,BGRA8UNORM} from "../constants";
import RenderPassDescriptor from "./renderpassdescriptor";
import Texture from "./texture";

/**
 * This abstracts the concept of a Framebuffer object from WebGL
 */
export default class Fbo {
    instance:any;

    // format for fbo texture.
    fboFormat:string = BGRA8UNORM;

    attachments:Array<any> = [];

    width:number = 0;
    height:number = 0;

    textureUsage:any;

    // current pass encoder being used
    passEncoder:any = null;

    renderDescriptor:RenderPassDescriptor;

    depthTexture:any;
    depthFormat:any;
    depthUsage:any;
    
    constructor(instance,{
        fboFormat=BGRA8UNORM,
        width=window.innerWidth,
        height=window.innerHeight,
        usage = GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT,
        depthFormat = DEPTH24_STENCIL8,
        depthUsage = DEFAULT_TEXTURE_USAGE
    }={}) {
        this.instance = instance;
        this.fboFormat = fboFormat;
        this.width = width;
        this.height = height;
        this.depthFormat = depthFormat;
        this.depthUsage = depthUsage;


        this.textureUsage = usage;
        // create depth texture for this fbo
        this.depthTexture = instance.device.createTexture({
            size:{
                width:width,
                height:height
            },
            format:depthFormat,
            usage:depthUsage
        })

        // set depth texture
        this.renderDescriptor = new RenderPassDescriptor();
        this.renderDescriptor.setDepthStencilView(this.depthTexture.createView());

        // build default attachment
        // TODO figure out multiple attachments

        this.attachments.push(

            new Texture(instance,{
                width:width,
                height:height,
                format:fboFormat,
                usage:usage
            })
        )
    }

    /**
     * Sets the clear color for the fbo. Assumes FBO only has one attachment.
     * @param r {number} the red value to use
     * @param g {number} the green value to use
     * @param b {number} the blue value to use.
     * @param a {number} the alpha value to use.
     */
    setClearColor(r:number,g:number,b:number,a:number = 1){
        this.renderDescriptor.colorAttachments[0].loadValue = {
            r:r,
            g:g,
            b:b,
            a:a
        }
    }

    /**
     * Returns the GPUTexture object at the specified attachment point
     * @param index {number} the index of the attachment - defaults to 0
     */
    getAttachment(index=0){
        return this.attachments[index];
    }

    /**
     * Returns the GPUTextureView object at the specified attachment point.
     * @param index {number} the index of the attachment - defaults to 0
     */
    getAttachmentView(index=0){
        return this.attachments[index].getView()
    }

    updateColorAttachment(tex:Texture){
        this.renderDescriptor.setColorAttachmentView(tex.getView());
    }

    resize(width:number,height:number){
        let instance = this.instance;
        // create depth texture for this fbo
        this.depthTexture = instance.device.createTexture({
            size:{
                width:width,
                height:height
            },
            format:this.depthFormat,
            usage:this.depthUsage
        })

        // set depth texture
        this.renderDescriptor = new RenderPassDescriptor();
        this.renderDescriptor.setDepthStencilView(this.depthTexture.createView());

        this.attachments = [];
        this.attachments.push(
            new Texture(instance,{
                width:width,
                height:height,
                format:this.fboFormat,
                usage:this.textureUsage
            })
        )
    }

    /**
     * "Binds" the FBO for rendering.
     * @param commandEncoder {GPUCommandEncoder} the command encoder to use
     * @param renderFb {function} the function that will render your scene onto the Framebuffer attachment.
     */
    bind(commandEncoder,renderFb:Function){
        //this.renderDescriptor.updateColorAttachment(this.attachments[0].createView());

        this.renderDescriptor.setColorAttachmentView(this.attachments[0].getView());

        const passEncoder = commandEncoder.beginRenderPass(this.renderDescriptor);
        this.passEncoder = passEncoder;
        renderFb(passEncoder);
    }

    /**
     * Returns the render pass descriptor for the fbo.
     */
    getRenderDescriptor(){
        return this.renderDescriptor;
    }

    /**
     * Ends writing onto the framebuffer.
     */
    unbind(){
        this.passEncoder.endPass();
        this.passEncoder = null;
    }

    /**
     * Returns the width of the frame buffer
     */
    getWidth(){
        return this.width;
    }

    /**
     * Returns the height of the frame buffer.
     */
    getHeight(){
        return this.height;
    }
}