import {DEFAULT_TEXTURE_USAGE, DEPTH24_STENCIL8} from "../constants";

/**
 * The function for intializing a WebGPU instance
 * @param cb {function} A function to call once the device is acquired.
 * @param settings {object} any settings that might be needed ie. swapchain
 */
export default function(cb,settings={
    swapchainFormat:"bgra8unorm",
    canvasWidth:window.innerWidth,
    canvasHeight:window.innerHeight,
    canvasAttachTo:document.body
}){
    if(!navigator["gpu"]){
        alert("Your browser does not support WebGPU");
        return;
    }

    navigator["gpu"]["requestAdapter"]().then( res => {

        res["requestDevice"]().then(device => {
            cb(new WebGPUInstance(device,res,settings));
        });
    });


}

/**
 * A class that defines the basic components necessary for any WebGPU scene including context,
 * canvas and resize handling.
 */
class WebGPUInstance{

    // instance settings
    settings:any;

    // GPU device to use
    device:any;

    // context for the canvas
    context:any;

    // canvas to use.
    canvas:HTMLCanvasElement;

    // default depth colorcube
    defaultDepthTexture:any;

    // default swapchain
    swapchain:any;

    // flag to indicate whether or not we've called resize
    resizing:boolean = false;

    // a list of command encoders that are being used.
    encoders:Array<CommandEncoder> = [];

    // a list of callbacks to run when re-sizing of canvas space occurs
    resizeCbs:Array<Function> = [];

    adapter:any;

    presentationFormat:any;

    presentationSize:[number,number] = [0,0];

    constructor(device,adapter,settings){
        this.device = device;

        this.settings = settings;

        this.canvas = settings.canvas !== undefined ? settings.canvas : document.createElement("canvas");
        this.canvas.width = settings.canvasWidth;
        this.canvas.height = settings.canvasHeight;

        this.appendTo(settings.canvasAttachTo);

        // TODO currently Safari and Chromium based browsers return a different context type. Be sure to
        // double check down the line.
        this.context = this.canvas.getContext("webgpu");

        this.presentationFormat = this.context.getPreferredFormat(adapter);
        this.presentationSize = [
            this.canvas.width,
            this.canvas.height
        ]

        // build default swapchain
        this.swapchain = this.context["configure"]({
            device,
            format:this.presentationFormat
        })

        // build default depth texture
        this.defaultDepthTexture = this.device.createTexture({
            size:{
                width:this.getCanvasWidth(),
                height:this.getCanvasHeight()
            },
            format:DEPTH24_STENCIL8,
            usage:DEFAULT_TEXTURE_USAGE
        })

        window.addEventListener("resize",this._onResize.bind(this));
    }


    /**
     * Sets the viewport for the current render pass
     * @param passEncoder {GPURenderPassEncoder} the renderpass encoder to use
     * @param x {number} x position of origin
     * @param y {number} y position of origin
     * @param width {number} width of viewport
     * @param height {number} height of viewport
     * @param minDepth {number} minimum depth for the viewport. Can not be < 0
     * @param maxDepth {number} maximum depth for the viewport. Must be at least 1.
     */
    viewport(passEncoder,{
        x=0,
        y=0,
        width=this.canvas.width,
        height=this.canvas.height,
        minDepth=0,
        maxDepth=1
    }={}){
        passEncoder.setViewport(x,y,width,height,minDepth,maxDepth);
    }
    /**
     * Rebuilds the swapchain
     */
    rebuildSwapchain(){

        let device = this.device;
        let settings = this.settings;

        // build default swapchain
        this.swapchain = this.context["configure"]({
            device,
            format:this.presentationFormat
        })

        // build default depth colorcube
        this.defaultDepthTexture = this.device.createTexture({
            size:{
                width:this.getCanvasWidth(),
                height:this.getCanvasHeight()
            },
            format:DEPTH24_STENCIL8,
            usage:DEFAULT_TEXTURE_USAGE
        })

    }

    /**
     * Sets the canvas size
     * @param width {number} the width for the canvas
     * @param height {number} the height for the canvas
     */
    setCanvasSize(width:number,height:number){
        this.canvas.width = width;
        this.canvas.height = height;
        return this;
    }

    /**
     * Appends the canvas element for this WebGPU instance to the DOM
     * @param el {HTMLElement} the element to append the WebGPU instance's canvas to .
     */
    appendTo(el:HTMLElement){
        el.appendChild(this.canvas);
        return this;
    }

    /**
     * Shorthand for creating a command encoder off of the device for the ins
     */
    createCommandEncoder(){
        return this.device.createCommandEncoder();
    }

    /**
     * Returns the current swapchain texture.
     */
    getCurrentTexture(){
        return this.context.getCurrentTexture().createView();
    }

    /**
     * Returns the colorcube view for the default depth colorcube
     */
    getDefaultDepthView(){
        return this.defaultDepthTexture.createView();
    }

    /**
     * Wrapper function for createBuffer
     * @param size
     * @param usage
     * @param mapped
     */
    createBuffer(size,usage,mapped){
        return this.device.createBuffer({
            size:size,
            usage:usage,
            mappedAtCreation:mapped
        })
    }

    addResizeCallback(cb:Function){
        this.resizeCbs.push(cb);
    }

    /**
     * Returns the canvas width
     */
    getCanvasWidth(){
        return this.canvas.width;

    }

    /**
     * Returns the canvas height.
     */
    getCanvasHeight(){
        return this.canvas.height;
    }

    /**
     * Adds a command encoder to the default list of encoders.
     * @param encoder {CommandEncoder} a command encoder instance.
     */
    addCommandEncoder(encoder:CommandEncoder){
        this.encoders.push(encoder);
        return this;
    }

    /**
     * Dispatches all stored encoders on the default
     * @deprecated
     */
    dispatchEncoders(){

      /*
        // extract the encoder object from the list.
        let encoders = this.encoders.map(encoder => {
            return encoder.encoder;
        })

        this.device.queue.submit(encoders);
       */
    }


    /**
     * Resize handler for when browser window is resized.
     * @param e
     */
    _onResize(e){

        let device = this.device;
        this.resizing = true;

        // ================ HANDLE CANVAS RESIZE ======================== //

        let parent = this.canvas.parentElement !== document.body ? this.canvas.parentElement : false;
        if(!parent){
            this.canvas.width = window.innerWidth;
            this.canvas.height = window.innerHeight;
        }else{
            let parent = getComputedStyle(this.canvas.parentElement);
            this.canvas.width = parseInt(parent.width);
            this.canvas.height = parseInt(parent.height);
        }

        this.defaultDepthTexture = this.device.createTexture({
            size:{
                width:this.getCanvasWidth(),
                height:this.getCanvasHeight()
            },
            format:DEPTH24_STENCIL8,
            usage:DEFAULT_TEXTURE_USAGE
        })


        // run through re-size callbacks
        this.resizeCbs.forEach(cb => {
            cb();
        })

        setTimeout(()=> {
            this.rebuildSwapchain();
            this.resizing = false;

        },400)

    }
}

