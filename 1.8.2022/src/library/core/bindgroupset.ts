/**
 * A general type to use when specifying actual bind group resources.
 * Resource property should match the type of resource you're specifying,
 * ie for buffers it should look something like
 *
 * {
 *     buffer:<buffer object>,
 *     //... any other properties for the buffer
 * }
 */
export class BindGroupEntry {

    binding:number;
    resource:any;

    constructor(binding=0,resource=null) {

        this.binding = binding;
        this.resource = resource;
    }

    /**
     * Returns a buffer entry suitable for use with a buffer.
     * TODO maybe do a typeof check since samplers and textures pretty much follow the same form
     */
    getBufferLayoutEntry(){
        return {
            binding:this.binding,
            resource:{
                buffer:this.resource,
            }
        }
    }

}

export class BindGroupLayoutEntry {
    constructor(binding=0,{
        visibility=GPUShaderStage.VERTEX_SHADER,
        buffer={
            type:"uniform"
        }
    }={}) {

        return {
            binding:binding,
            visibility:visibility,
            buffer:buffer
        }
    }

}


/**
 * Describes a bind group to be used with a mesh.
 */
export default class Bindgroupset {

    instance:any;
    layoutEntries:Array<BindGroupLayoutEntry> = [];
    bgLayout:any;
    bindGroup:any;

    constructor(instance) {

        this.instance = instance;
    }

    /**
     * Adds a layout entry describing how the Bind group will look.
     * @param entry
     */
    addEntry(entry:BindGroupLayoutEntry){
        this.layoutEntries.push(entry);
        return this;
    }

    /**
     * Compiles the set into a BindGroup.
     * @param resources {Array} an array of BindGroupEntry objects.
     * @return bindGroup returns the bind group that is created.
     */
    compile(resources:Array<BindGroupEntry>){
        // makes sure nothing is null
        resources.forEach(itm => {
            if(!itm.resource){
                console.warn(`Unable to compile bindgroup, a resource with the binding of ${itm.binding} is null`);
            }
        })

        // compile the layout.
        this.bgLayout = this.instance.device.createBindGroupLayout({
            entries: this.layoutEntries
        });

        this.bindGroup = this.instance.device.createBindGroup({
            layout:this.bgLayout,
            entries:resources
        })
        return this.bindGroup;
    }

    getBindGroupLayout(){
        return this.bgLayout;
    }

    getBindGroup(){
        return this.bindGroup;
    }
}