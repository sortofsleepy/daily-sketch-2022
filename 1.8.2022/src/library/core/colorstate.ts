import {BGRA8UNORM} from "../constants";

/**
 * A helper class for managing color state.Should return an object in line with a GPUColorStateDescriptor
 * see https://alaingalvan.medium.com/raw-webgpu-58522e638b17#3d83 for an example of how it's used normally.
 */
export default class ColorState {

    instance:any;
    format:string = BGRA8UNORM;

    alphaBlend:any =  {
        srcFactor: 'src-alpha',
        dstFactor: 'one-minus-src-alpha',
        operation: 'add'
    }

    colorBlend:any = {
        srcFactor: 'src-alpha',
        dstFactor: 'one-minus-src-alpha',
        operation: 'add'
    }

    writeMask:any =  GPUColorWrite.ALL

    constructor(instance,{
        alphaBlend = null,
        colorBlend = null,
        writeMask = null
    }={}) {


        this.instance = instance;

        this.alphaBlend = alphaBlend !== null ? alphaBlend : this.alphaBlend;
        this.colorBlend = colorBlend !== null ? colorBlend : this.colorBlend;
        this.writeMask = writeMask !== null ? writeMask : this.writeMask;

    }

    setAlphaSrcFactor(factor:string){
        this.alphaBlend.srcFactor = factor;
    }
    setAlphaDstFactor(factor:string){
        this.alphaBlend.dstFactor = factor;
    }

    setAlphaOperation(operation:string){
        this.alphaBlend.operation = operation;
    }
    setColorSrcFactor(factor:string){
        this.colorBlend.srcFactor = factor;
    }
    setColorDstFactor(factor:string){
        this.colorBlend.dstFactor = factor;
    }

    setColorOperation(operation:string){
        this.colorBlend.operation = operation;
    }
    get(){
        return {
            format:this.format,
            alphaBlend: this.alphaBlend,
            colorBlend: this.colorBlend,
            writeMask: this.writeMask
        }
    }

}