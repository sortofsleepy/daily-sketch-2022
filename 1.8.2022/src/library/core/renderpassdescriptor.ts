/**
 * A wrapper around setting up the GPURenderPassDescriptor for rendering.
 */
export default class Renderpassdescriptor {

    //clear color for the scene
    clearColor:[number,number,number,number] = [1.0,0.5,0.5,1.]

    // describes the color attachments for the scene.
    colorAttachments:Array<ColorAttachmentDescriptor> = [];

    // describes depth stencil attachment.
    depthStencilAttachment:DepthStencilAttachmentDescriptor;

    constructor() {

        // set default load value;
        this.colorAttachments.push(
            {
                view:null,
                loadValue:{
                    r:this.clearColor[0],
                    g:this.clearColor[1],
                    b:this.clearColor[2],
                    a:this.clearColor[3],
                }
            }
        )

        this.depthStencilAttachment = {
            view: null,
            depthLoadValue: 1.0,
            depthStoreOp: 'store',
            stencilLoadValue: 0,
            stencilStoreOp: 'store',
        }
    }

    setAttachmentClearColor(r:number,g:number,b:number,a:number=1,attachmentIndex=0){
        this.clearColor = [r,g,b,a];
    }

    setRed(r:number){
        this.clearColor[0] = r;
    }

    setGreen(g:number){
        this.clearColor[1] = g;
    }

    setBlue(b:number){
        this.clearColor[2] = b;
    }

    setAlpha(a:number){
        this.clearColor[3] = a;
    }

    /**
     * Sets the texture for the depth stencil
     * @param depth {GPUTextureView} texture for the depth stencil
     */
    setDepthStencilView(depth:GPUTextureView){
        this.depthStencilAttachment.view = depth;
        return this;
    }

    /**
     * Sets the color texture for the specified attachment index
     * @param colorAttachment {GPUTextureView} a texture to output
     * @param index {number} the index of the attachment you want to set. 0 is assumed to be the primary attachment.
     */
    setColorAttachmentView(colorAttachment:GPUTextureView,index=0){
        this.colorAttachments[index].view = colorAttachment;
        return this;
    }

}

export class RenderPassDescriptor {
}