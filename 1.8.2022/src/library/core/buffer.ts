/**
 * Basic wrapper around the idea of a buffer.
 */
import {CoreObject} from "./interfaces";
import {COPY_DST, UNIFORM} from "../constants";

export default class Buffer implements CoreObject{

    name:string;
    instance:any;

    // raw data for buffer in TypedArray
    data:any;

    bufferObj:any;
    arrayType:any = "Float32Array"

    usage:number = UNIFORM | COPY_DST
    byteLength:number = 0;


    constructor(instance,{
        arrayType="Float32Array",
        data = null,
        usage = UNIFORM | COPY_DST,
        size=1
    }={}) {

        this.instance = instance;
        this.arrayType = arrayType;
        this.usage = usage;


        if(data === null){
            this.bufferObj = this.instance.device.createBuffer({
                size:size,
                usage:this.usage
            });
        }else{

            this.set({
                data:data,
                arrayType:arrayType
            })
        }
    }

    /**
     * generates a buffer suitable for using as a uniform buffer with the size and
     * correct usage flags.
     * @param size{number} size of the buffer in bytes.
     * @param byteSize{number} byteSize of each component of the buffer. It is assumed to be 4 by default.
     */
    generateUniformBuffer(size:number,byteSize:number = 4){

        // TODO update size to include byteSize parameter by default.
        this.bufferObj = this.instance.device.createBuffer({
            size:size * byteSize,
            usage:GPUBufferUsage.UNIFORM | COPY_DST
        })
    }

    updateData(data:ArrayBufferView = null){


        if(data !== null){

            this.instance.device.queue.writeBuffer(
                this.bufferObj,
                0,
                data.buffer,
                data.byteOffset,
                data.byteLength
            );
        }else{

            this.instance.device.queue.writeBuffer(
                this.bufferObj,
                0,
                this.data.buffer,
                this.data.byteOffset,
                this.data.byteLength
            );
        }
    }


    /**
     * sets the information for a buffer
     * @param size {number} size of buffer
     * @param data {Array} a TypedArray of data for the buffer
     * @param usage {number} constants for where to use the data in the buffer
     * @param arrayType {string} the type of array to expect, will be used to fill buffer
     */
    set({
        data = null,
        usage = GPUBufferUsage.VERTEX,
        arrayType="Float32Array"
    }={}){

        if(data === null){
            return;
        }

        data = new window[arrayType](data);

        this.bufferObj = this.instance.device.createBuffer({
            size:data.byteLength,
            usage:usage,
            mappedAtCreation:true
        });

        new window[arrayType](this.bufferObj.getMappedRange()).set(data);
        this.bufferObj.unmap();
        this.byteLength = data.byteLength

        this.data = data;

    }

    /**
     * Returns the byte length of the data associated with this buffer.
     */
    getByteLength(){
        return this.byteLength;
    }

    /**
     * Returns the raw GPUBuffer object.
     */
    getObject(){ return this.bufferObj;}

}