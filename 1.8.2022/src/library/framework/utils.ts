import mat4 from "../math/mat4";
import vec3 from "../math/vec3";


/**
 * Stuff here is really just for debugging
 * @param projection
 * @param zoom
 * @param rotate
 */


export function getTransformMatrix(projection,zoom=-4,rotate=true){
    const viewMatrix = mat4.create();
    mat4.translate(viewMatrix,viewMatrix,vec3.fromValues(0,0,zoom))
    const now = Date.now() / 1000

    if(rotate){
        mat4.rotate(
            viewMatrix,
            viewMatrix,
            1,
            vec3.fromValues(Math.sin(now),Math.cos(now),0)
        )
    }

    const mvp = mat4.create();
    mat4.multiply(mvp,projection,viewMatrix);

    return mvp as Float32Array;

}
export function makeProjection(width=window.innerWidth,height = window.innerHeight){
    const projection = mat4.create();
    const aspect = Math.abs(width / height)
    mat4.perspective(projection,Math.PI / 4 , aspect, 1, 100000);
    return projection;
}
