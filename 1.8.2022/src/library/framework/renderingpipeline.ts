
import Buffer from "../core/buffer";
import ColorState from "../core/colorstate";
import {DEPTH24_STENCIL8, FLOAT32_4} from "../constants";
import {DepthStencilSettings} from "../core/interfaces";
import Bindgroupset from "../core/bindgroupset";


interface AttributeDescription {
    shaderLocation:number;
    offset:number;
    format:string;
}

interface Attribute {
    arrayStride:number,
    attributes:Array<AttributeDescription>
}


export default class RenderPipeline{

    pipeline:any;
    instance:any;

    // attributes for the mesh
    attributes:Array<Attribute> = [];

    // drawing parameters
    // see https://gpuweb.github.io/gpuweb/#render-pass-encoder-drawing#dom-gpurenderencoderbase-draw for list
    drawParameters:any = {
        vertexCount:0,
        instanceCount:1,
        firstVertex:0,
        firstInstance:0,
        indexCount:0,
        baseVertex:0,
        firstIndex:0
    };

    // list of data buffers. Should correspond to how the layouts are described in the attributes array
    attributeBuffers:Array<Buffer> = [];

    // topology type to use for the mesh;
    topology:string = "triangle-list";

    cullMode:string = "none"

    name:string;

    colorStates:Array<ColorState> = [];

    shader:any = null;

    // pipeline layout for the pipeline.
    pipelineLayout:any;

    // any blend states for the pipeline
    blendStates:Array<any> = []

    // bind groups to use when rendering contents in this pipeline
    bindGroups:Array<Bindgroupset> = [];

    // settings for the depth state
    depthStencilState:DepthStencilSettings = {
        depthWriteEnabled:true,
        depthCompare:"less",
        format:DEPTH24_STENCIL8
    }

    // buffer holding index data for the pipeline to render.
    indexBuffer:Buffer;

    constructor(ctx) {
        this.instance = ctx;

    }

    setVertexCount(count:number){
        this.drawParameters.vertexCount = count;
        return this;
    }

    /**
     * Adds a BindGroup to this Render
     * @param bg {Bindgroupset} A bind group set object.
     */
    addBindGroup(bg:Bindgroupset){
        this.bindGroups.push(bg);
        return this;
    }

    addAttribute(data:any,{
        stride = 4,
        attributeLayout = [{
            shaderLocation:0,
            offset:0,
            format:FLOAT32_4
        }]
    }={}){

        let device = this.instance.device;
        let mappedAtCreation = true;
        let flags = 0

        let buffer = new Buffer(this.instance);
        buffer.set({
            data:data,
            usage:flags !== 0 ? GPUBufferUsage.VERTEX | flags : GPUBufferUsage.VERTEX
        });

        // add buffer to list of buffers.
        this.attributeBuffers.push(buffer);

        // construct the layout information
        let attrib:Attribute = {
            arrayStride:stride,
            attributes:attributeLayout
        }

        // store layout information
        this.attributes.push(attrib);

    }


    compile(){

        if(!this.shader){
            console.warn("Attempting to construct a rendering pipeline without a shader")
            return;
        }
        let shader = this.shader;
        let layouts = this.attributes;

        let colorStateBuild = [];
        this.colorStates.forEach(state => {
            let cstate:GPUColorStateDescriptor = {
                format:state.format,
                alphaBlend:state.alphaBlend,
                colorBlend:state.colorBlend,
                writeMask:state.writeMask
            }
            colorStateBuild.push(cstate);
        })


        // build out the attribute definitions.
        shader.vertexStage.buffers = layouts.map(itm => {
            return itm;
        })

        // final settings to pass into the createRenderPipeline function
        let settings = {
            layout:this.pipelineLayout,
            vertex: shader.vertexStage,
            fragment: shader.fragmentStage,
            colorStates:colorStateBuild,
            primitive: {
                topology: this.topology,
                cullMode:this.cullMode
            },
            blendStates:this.blendStates,
            depthStencil:this.depthStencilState
        }


        // construct the pipeline.
        this.pipeline = this.instance.device.createRenderPipeline(settings);

    }

    setIndexData(data:Array<number>){
        let device = this.instance.device;

        //@ts-ignore
        data = new Uint16Array(data);
        let indexCount = data.length * 3;

        let buffer = new Buffer(this.instance);
        buffer.set({
            data:data,
            usage:GPUBufferUsage.INDEX,
            arrayType:"Uint16Array"
        });

        this.drawParameters.indexCount = indexCount;
        this.indexBuffer = buffer;


    }


    /**
     * Renders the data for the pipline.
     * @param passEncoder {GPURenderPassEncoder} renderpass encoder for the pipeline.
     */
    draw(passEncoder){
        passEncoder.setPipeline(this.pipeline);

        // set bind groups
        this.bindGroups.forEach((bg,i) => {
            passEncoder.setBindGroup(i, bg);
        })

        this.attributeBuffers.forEach((attrib,i)=>{
            passEncoder.setVertexBuffer(i,attrib.getObject())
        })


        if(!this.indexBuffer){

            passEncoder.draw(
                this.drawParameters.vertexCount,
                this.drawParameters.instanceCount,
                this.drawParameters.firstVertex,
                this.drawParameters.firstInstance
            );
        }else{

            passEncoder.setIndexBuffer(this.indexBuffer.getObject(),"uint16")

            passEncoder.drawIndexed(
                this.drawParameters.indexCount,
                this.drawParameters.instanceCount,
                this.drawParameters.firstIndex,
                this.drawParameters.baseVertex,
                this.drawParameters.firstInstance
            )
        }
    }

    /**
     * Sets the pipeilne for the Mesh
     * @param pipeline {RenderPipeline} the render pipeline to use.
     */
    setPipelineLayout(pipelineLayout:RenderPipeline){
        this.pipelineLayout = pipelineLayout;
        return this;
    }

    /**
     * Sets the number of vertices to draw
     * @param numVertices
     */
    setNumVertices(numVertices:number){
        this.drawParameters.vertexCount = numVertices;
        return this;
    }

    /**
     * Sets the number of instances to render. Will only work in Vertex input for an attribute is set to "instance"
     * @param instances
     */
    setNumInstances(instances:number){
        this.drawParameters.instanceCount =  instances;
        return this;
    }

}