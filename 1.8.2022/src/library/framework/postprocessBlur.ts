import Buffer from "../core/buffer";
import Fbo from "../core/fbo";
import Quad from "../geometry/quad";
import blurShader from "../shaders/blurShader"
import {SHADER_FRAGMENT, UNIFORM_TYPE} from "../constants";
import Texture from "../core/texture";

/**
 * Sets up uniform buffer needed for blur shader processing.
 * Shader adapted from
 * https://github.com/paulhoux/Cinder-Samples/blob/master/BloomingNeon/assets/blur.frag
 * @param instance
 */
function setupBlurShaderUniforms(instance){
    const uboSize = 4 * 3;
    const uniformBuffer = new Buffer(instance);
    uniformBuffer.generateUniformBuffer(uboSize);

    let elements = new Float32Array([
        1,1, // sample offset
        1 // attenuation
    ])

    uniformBuffer.updateData(elements)

    return {
        elements:elements,
        buffer:uniformBuffer,
        uniformsdirty:false,
        setSampleOffset(x:number,y:number){
            this.elements[0] = x;
            this.elements[1] = y;


            this.uniformsdirty = true;
        },

        setAttenuation(attenuation:number){
            this.elements[2] = attenuation;
            this.buffer.updateData(this.elements)
            this.uniformsdirty = true;
        },

        update(){
            if(this.uniformsdirty){
                this.buffer.updateData(this.elements);
                this.uniformsdirty = false;
            }
        }

    }
}

export default function setupPostProcessing(instance){
    let shaderUniforms = setupBlurShaderUniforms(instance);
    let uniformBuffer = shaderUniforms.buffer;

    let fbo = new Fbo(instance);
    let fbo2 = new Fbo(instance);


    return  {

        fbos:[fbo,fbo2],

        // uniform buffer and handling
        uniforms:shaderUniforms,

        // quad object used to render things.
        quad:new Quad(instance,{
            shader:blurShader,
            descriptors:[

                {
                    binding:2,
                    visibility:SHADER_FRAGMENT,
                    buffer:{
                        type:UNIFORM_TYPE
                    }
                }
            ],
            uniformValues:[
                {
                    binding: 2,
                    resource:{
                        buffer:uniformBuffer.getObject()
                    }
                },
            ]
        }),

        // sets the image for the post processing
        setImage(image:Texture){
            this.quad.setTexture(image);
        },

        // runs the post processing setup.
        blur(commandEncoder,image:Texture){

            if(image === undefined){
                return;
            }

            this.setImage(image);


            /////// BLUR HORIZONTAL //////////
            this.uniforms.setSampleOffset(1.0 / this.fbos[0].getWidth(),0.0);
            this.uniforms.update();

            this.fbos[0].bind(commandEncoder,passEncoder =>{
                this.quad.renderQuad(passEncoder);
            });
            this.fbos[0].unbind();

            this.setImage(this.fbos[0].getAttachment());

            /////// BLUR VERTICAL ////////////
            this.uniforms.setSampleOffset(0.0,1.0 / this.fbos[0].getHeight());
            this.uniforms.update();

            this.fbos[1].bind(commandEncoder,passEncoder =>{
                this.quad.renderQuad(passEncoder);
            });
            this.fbos[1].unbind();

        },

        getOutput(){
            return this.fbos[1].getAttachment()
        }
    }

}