import Buffer from "../core/buffer";
import PerspectiveCamera from "./camera";
import OrbitCam from "./orbitcam";
import UBO from "../core/uniformbuffer";

/**
 * A Perspective Camera implementation with some orbit functionality bundled in.
 */
export default class SceneCamera {

    buffer:Buffer;
    instance:any;

    perspectiveCamera:any;
    orbitCamera:any;

    ubo:UBO;

    orbitOn:boolean = true;

    constructor(instance,{
        fov=Math.PI / 4,
        orbitCameraDistance = 5,
        binding=0
    }={}) {

        this.instance = instance;
        //this.buffer = new Buffer(instance);
        //this.buffer.generateUniformBuffer(4 * 16);

        this.perspectiveCamera = new PerspectiveCamera(Math.PI / 4);
        this.orbitCamera = new OrbitCam();
        this.orbitCamera.setDistance(orbitCameraDistance)

        this.ubo = new UBO(instance,{
            blockName:"CameraUniform"
        });

        this.ubo.setMat4("modelViewProjectionMatrix",this.perspectiveCamera.getViewProjectionMatrix());

        this.ubo.compile({
            binding:binding
        })
    }


    update(){
        // if orbit controls are turned on, update view matrix of main camera.
        if(this.orbitOn){
            this.perspectiveCamera.setViewMatrix(this.orbitCamera.getViewMatrix());
        }

        // update the camera uniform buffer
        this.ubo.updateValue("modelViewProjection",this.perspectiveCamera.getViewProjectionMatrix());

    }

    getUboShaderCode(){
        return this.ubo.getShaderCode()
    }

    // return the camera buffer
    getBuffer(){
        return this.ubo.getBuffer();
    }

}