import Buffer from "../core/buffer"

interface MeshAttribute {
    name:string;
    data:Array<any>;
    location:number;
    buffer:Buffer;
}

/**
 * Represents mesh data for rendering.
 *
 * TODO being rebuilt - so some things may not make sense just yet.
 */
export default class Mesh {
    instance:any;
    attributes:Array<Buffer> = [];
    indexBuffer:Buffer
    // drawing parameters
    // see https://gpuweb.github.io/gpuweb/#render-pass-encoder-drawing#dom-gpurenderencoderbase-draw for list
    drawParameters:any = {
        vertexCount:0,
        instanceCount:1,
        firstVertex:0,
        firstInstance:0,
        indexCount:0,
        baseVertex:0,
        firstIndex:0
    };

    constructor(instance) {
        this.instance = instance;

    }

    setInstanceCount(count:number){
        this.drawParameters.instanceCount = count;
        return this;
    }

    addAttribute(location:number,data:Float32Array,{
        id = this.attributes.length - 1 // an id for the attribute. defaults to last attrib index
    }={}){


        let buffer = new Buffer(this.instance);
        buffer.set({
            data:data,
        })

        this.attributes.push(buffer);
        return this;
    }

    addAttributeBuffer(location:number,data:Buffer){
        this.attributes.push(data);
        return this;
    }

    setVertexCount(count:number){
        this.drawParameters.vertexCount = count;
        return this;
    }

    draw(passEncoder){
        this.attributes.forEach((itm,idx)=>{
            passEncoder.setVertexBuffer(idx,itm.getObject())
        })

        if(this.indexBuffer !== undefined){


            passEncoder.setIndexBuffer(this.indexBuffer.getObject(),"uint16");
            passEncoder.drawIndexed(
                this.drawParameters.indexCount,
                this.drawParameters.instanceCount,
                this.drawParameters.firstInstance,
                this.drawParameters.baseVertex,
                this.drawParameters.firstInstance);

        }else{
            passEncoder.draw(this.drawParameters.vertexCount,this.drawParameters.instanceCount,this.drawParameters.firstVertex,this.drawParameters.firstInstance);
        }
    }

    /**
     * Adds indices
     * @param data
     */
    setIndexData(data:Array<number>){
        let device = this.instance.device;

        //@ts-ignore
        data = new Uint16Array(data);
        let indexCount = data.length;

        let buffer = new Buffer(this.instance);
        buffer.set({
            data:data,
            usage:GPUBufferUsage.INDEX,
            arrayType:"Uint16Array"
        });

        this.drawParameters.indexCount = indexCount;
        this.indexBuffer = buffer;


    }
}