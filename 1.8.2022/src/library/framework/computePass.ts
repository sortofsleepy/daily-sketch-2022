import Buffer from "../core/buffer";

/**
 * A compute pass to ping pong data in a compute shader.
 */
export class ComputePingPongPass {


    // store all bind groups
    bindGroups:Array<any> = [0,0];
    bindGroupSettings:Array<BindGroupDescriptorEntry> = [];
    instance:any;
    pipeline:any;
    buffers:Array<Buffer> = [];

    numItems:number = 1;


    // data array for uniform values that need to go into the compute shader.
    // by default, we reserve one slot for time
    uniformBufferData:Float32Array;

    // TODO this should be user-settable somehow, current issue is how best to accommodate different variable types.
    uniformBufferRawData:Array<any> = [0,0];

    // buffer to hold uniforms for the compupte shader.
    uniformBuffer:Buffer;


    constructor(instance,{
        shader = null
    }={}) {


        this.instance = instance;
        this.pipeline = this.instance.device.createComputePipeline({
            compute:shader.computeStage
        })
    }

    setNumItems(items:number){
        this.numItems = items;
    }

    /**
     * Sets the data you want to manipulate with the compute shader.
     * @param data
     */
    setData(data:Array<number>){
        let d = new Float32Array(data);


        for(let i = 0; i < 2; ++i){
            let buffer = new Buffer(this.instance);
            buffer.set({
                data:d,
                usage:GPUBufferUsage.VERTEX | GPUBufferUsage.STORAGE | GPUBufferUsage.UNIFORM ,
            });

            this.buffers.push(buffer);

            this.bindGroupSettings.push({
                binding:i,
                resource:{
                    buffer:buffer.getObject()
                }
            })
        }

        ////// ADD UNIFORM SETTINGS ///////
        this.uniformBuffer = new Buffer(this.instance);
        this.uniformBuffer.generateUniformBuffer(4 * this.uniformBufferRawData.length);
        this.uniformBufferData = new Float32Array(this.uniformBufferRawData.length);
        this.uniformBufferData.set(this.uniformBufferRawData);
        this.uniformBuffer.updateData(this.uniformBufferData)





    }

    /**
     * Adds a floating point value uniform to the list of uniform items.
     * This assumes the uniform structure in your shader contains the same number of values
     * as the uniformBufferRawData array in this object.
     * @param val {number} Value to add.
     */
    addFloatUniform(val:number){
        this.uniformBufferRawData.push(val);
        this.uniformBuffer.generateUniformBuffer(4 * this.uniformBufferRawData.length);
        this.uniformBufferData = new Float32Array(this.uniformBufferRawData.length);

        this.updateUniforms();
    }

    /**
     * Update uniforms
     */
    updateUniforms(){

        this.uniformBufferData.set(this.uniformBufferRawData);
        this.uniformBuffer.updateData(this.uniformBufferData)
    }

    /**
     * Compiles the compute pass
     */
    compile(){

        // add uniform buffer to bindgroup settings.
        this.bindGroupSettings.push({
            binding:2,
            resource:{
                buffer:this.uniformBuffer.getObject()
            }
        })

        if(this.bindGroupSettings.length > 0) {
            this.bindGroups[0] = this.instance.device.createBindGroup({
                layout: this.pipeline.getBindGroupLayout(0),
                entries: this.bindGroupSettings
            })
            this.bindGroups[1] = this.instance.device.createBindGroup({
                layout: this.pipeline.getBindGroupLayout(0),
                entries: this.bindGroupSettings
            })
        }

    }

    /**
     * Returns the buffer for the output.
     */
    getBuffer(){

        return this.buffers[1];
    }

    /**
     * Return the raw buffer of the output
     */
    getRawBuffer(){
        return this.buffers[1].getObject();
    }

    /**
     * Runs the compute pass
     * @param commandEncoder {GPUCommandEncoder} the command encoder object to use for the render.
     */
    run(commandEncoder){
        const passEncoder = commandEncoder.beginComputePass();
        passEncoder.setPipeline(this.pipeline);

        if(this.bindGroups.length > 0) {

            passEncoder.setBindGroup(0,this.bindGroups[0]);
            passEncoder.setBindGroup(1,this.bindGroups[1]);
        }


        passEncoder.dispatch(this.numItems)
        passEncoder.endPass();


        let tmp = this.bindGroups[1]
        this.bindGroups[1] = this.bindGroups[0];
        this.bindGroups[0] = tmp;
    }

}