import mat4 from "../math/mat4"
import vec3 from "../math/vec3";
import Buffer from "../core/buffer";

class Camera {
    ctx:any;
    buffer:any;
    projectionMatrix:Array<number> = mat4.create();
    viewMatrix:Array<number> = mat4.create();

    constructor(instance) {
        this.ctx = instance;
    }
}

export default class PerspectiveCamera{
    projectionMatrix:Array<number> = mat4.create();
    viewMatrix:Array<number> = mat4.create();
    position:Array<number> = vec3.create();
    up:Array<number> = vec3.create();
    target:Array<number> = vec3.create();

    cameraData:any = new Float32Array(16);

    fov:number;
    near:number;
    far:number;

    constructor(fov:number=Math.PI / 4,aspect:number=window.innerWidth/window.innerHeight,near:number=0.1,far:number=100000.0) {
        this.projectionMatrix = mat4.perspective(this.projectionMatrix,fov,aspect,near,far);

        this.fov = fov;
        this.near = near;
        this.far = far;
        // set default target position.
        this.setZoom(-10);
    }

    setViewMatrix(mat:Array<number> = []){
        if(mat.length > 0){
            mat4.copy(this.viewMatrix,mat);
        }
    }

    /**
     * Sets the zoom level of the camera
     * @param zoomLevel {number} the value of the zoom. Negative value moves camera back, positive moves forward.
     */
    setZoom(zoomLevel:number){
        this.target[2] = zoomLevel;
        mat4.translate(this.viewMatrix,this.viewMatrix,this.target)
    }

    resize(aspect:number=window.innerWidth/window.innerHeight){
        this.projectionMatrix = mat4.perspective(this.projectionMatrix,this.fov,aspect,this.near,this.far);
    }

    getViewProjectionMatrix(){
        this.cameraData.set(mat4.multiply(mat4.create(),this.projectionMatrix,this.viewMatrix));
        return this.cameraData;
    }

    /**
     * Multiples the current view and projection matrices with the model .
     * @param model
     */
    getModelViewProjectionMatrix(model:Array<number> = mat4.create()){
        let viewProjection = this.getViewProjectionMatrix();
        return mat4.multiply(mat4.create(),viewProjection,model);
    }

    getProjectionMatrix(){
        return this.projectionMatrix;
    }

    getViewMatrix(){
        return this.viewMatrix;
    }

    /**
     * Updates a WebGPU buffer with data
     * @param buffer {Buffer} a buffer object.
     */
    updateBufferWithData(buffer:Buffer){
        buffer.updateData(this.getViewProjectionMatrix());
    }
}