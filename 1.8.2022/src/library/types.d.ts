// Temp interfaces and type declarations until these are native.
declare interface GPUBuffer{}
declare interface GPUCommandEncoder{}
declare interface GPUDevice{}
declare interface GPURenderPassDescriptor{}
declare interface GPUColorStateDescriptor{}
declare const GPUColorWrite;
declare interface WebGPUInstance{}

declare const GPUBufferUsage;
declare interface GPUTextureView {}
declare const GPUShaderStage;
declare const GPUTextureUsage;
declare const GPUColorStateDescriptor;
declare interface CommandEncoder{}
/*


declare interface ObjectConstructor {
    assign(...objects:Object[]):Object;
}



// overrides on the window object so we can customize it a bit.
interface Window {
    emitter:any;
    viewWidth:number;
    viewHeight:number;
    mousePos:any;
    router:any;
    state:any;
    Typekit:any;
    TweenMax:any;
    SIMD:any;
    addResizeCallback:Function;

    //WebGPU types
    GPUBuffer:any;
    GPUDevice:any;
    GPUBufferUsage:any;
    GPUQueue:any;

}

declare module Typekit {
    export function load(opts:Object):void;
}

// Temp interfaces and type declarations until these are native.
declare interface GPUBuffer{}
declare interface GPUCommandEncoder{}
declare interface GPUDevice{}
declare interface GPURenderPassDescriptor{}
declare interface GPUColorStateDescriptor{}
declare const GPUColorWrite;
declare interface WebGPUInstance{};

declare const GPUBufferUsage;
declare interface GPUTextureView {}
declare const GPUShaderStage;
declare const GPUTextureUsage;
declare const GPUColorStateDescriptor;

/// COMMAND ENCODER
interface CommandEncoder {
    encoder:any;
    id?:string
}

/// PIPELINE SETTINGS

interface ShaderAttribute {
    shaderLocation:number;
    offset:number;
    format:string;
}

interface PipelineSettings {
    layout?:any;
    cullMode:string;
    topology?:string;
    colorStates?:Array<any>
    buffers:Array<BufferDefinition>
}

interface BufferDefinition{
    arrayStride:number;
    attributes:Array<ShaderAttribute>,
    stepMode?:string;
}


// describes the entry for a bind group that
// requires a buffer.
interface BufferDescriptorType {
    buffer: any;
    offset?:number;
    size?:number;
}

// describes the entry for a bind group that
// requires a sampler
interface SamplerDescriptorType {
    type:string;
}

// describes the entry for a bind group that
// requires a texture.
interface TextureDescriptorType {
    samplerType:string;
}


declare interface BindGroupDescriptorEntry {
    binding:number;
    visibility?:number;
    buffer?:any;
    resource?:BufferDescriptorType;
    sampler?:SamplerDescriptorType;
    texture?:TextureDescriptorType;
}

declare interface BindGroupDescriptor {

}

 */
