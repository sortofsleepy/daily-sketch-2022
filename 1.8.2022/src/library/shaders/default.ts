export default {
    vertex:`

       /////// CAMERA UNIFORMS FOR BLOCK SCENE ////////
       [[block]] struct Uniforms {
            modelViewProjectionMatrix : mat4x4<f32>;
       };
       [[binding(0), group(0)]] var<uniform> uniforms : Uniforms;
       

       
       // vertex output 
       struct vertexOutput {
            [[builtin(position)]] Position : vec4<f32>;
            [[location(0)]] vUv:vec2<f32>;
       };
       
    
       [[stage(vertex)]]
       fn main(
            [[location(0)]] position:vec4<f32>)->vertexOutput {
  
            //////// DECLARE VARIABLES ////////////
            var output:vertexOutput;
            
            // re-ref vertex position of cubes. 
            var pos:vec4<f32> = position;
            
            // set output variables
            output.vUv = vec2<f32>(1.0,1.0);
            output.Position = uniforms.modelViewProjectionMatrix * vec4<f32>(pos.xyz,1.0);
            
            return output;
       }
    `,
    fragment:`
  

       [[stage(fragment)]]
        fn main(
          [[location(0)]] vUv : vec2<f32>
        
        ) -> [[location(0)]] vec4<f32>{
            return vec4<f32>(vUv,0.0,1.0);
        }
    `,

}