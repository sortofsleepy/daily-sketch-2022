/**
 * General shader when using to render a quad full screen.
 * Assumes a texture will be used to display something.
 */
export default {
    vertex:`
   
            [[location(0)]] var<in> position : vec2<f32>;

            [[builtin(position)]] var<out> Position : vec4<f32>;
            
            [[location(0)]] var<out> vUv:vec2<f32>;            
            [[stage(vertex)]]
            fn main() -> void {
                 var scale:vec2<f32> = vec2<f32>(0.5,0.5);
                
                 vUv = position.xy * -scale + scale;
                 vUv.x = 1.0 - vUv.x;
                 Position =  vec4<f32>(position,0.0,1.);
                  return;
            }
    `,
    fragment:`
            [[binding(0), group(0)]] var mySampler: sampler;
            [[binding(1), group(0)]] var myTexture: texture_2d<f32>;
            
            [[location(0)]] var<out> glFragColor : vec4<f32>;
            [[location(0)]] var<in> vUv : vec2<f32>;
             
            [[stage(fragment)]]
            fn main()->void {
                var v:vec4<f32> = textureSample(myTexture, mySampler, vUv);
                glFragColor = v;
            }`
}