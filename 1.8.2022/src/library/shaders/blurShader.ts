export default {
    vertex:`

        struct vertexOutput {
            [[builtin(position)]] Position : vec4<f32>;
            [[location(0)]] vUv:vec2<f32>;
        };

        [[stage(vertex)]]
        fn main([[location(0)]] position:vec2<f32>) -> vertexOutput {
            
            var scale: vec2<f32> = vec2<f32>(0.5,0.5);
            
            var output:vertexOutput;
            output.vUv = position.xy * scale + scale;
            output.vUv = vec2<f32>(output.vUv.x,1.0 - output.vUv.y);
            output.Position = vec4<f32>(position,0.0,1.0);
            return output;
        }
    `,

    fragment:`
  
       [[block]] struct Uniforms {
           sample_offset:vec2<f32>;
           attenuation:f32;
        };
        [[binding(2), group(0)]] var<uniform> uniforms : Uniforms;

        [[location(0)]] var<in> vUv : vec2<f32>;
        [[location(0)]] var<out> outColor : vec4<f32>;

        [[binding(0), group(0)]] var mySampler: sampler;
        [[binding(1), group(0)]] var myTexture: texture_2d<f32>;


  
        [[stage(fragment)]]
        fn main() {
            
            var sum:vec4<f32> = vec4<f32>(0.0,0.0,0.0,0.0);
            var sample_offset:vec2<f32> = uniforms.sample_offset;
           
            sum = sum + textureSample(myTexture, mySampler, vUv + -10.0 * sample_offset) * 0.009167927656011385;
            sum = sum + textureSample(myTexture, mySampler, vUv + -9.0 * sample_offset) * 0.014053461291849008;
            sum = sum + textureSample(myTexture, mySampler, vUv + -8.0 * sample_offset) * 0.020595286319257878;
            sum = sum + textureSample(myTexture, mySampler, vUv + -7.0 * sample_offset) * 0.028855245532226279;
            sum = sum + textureSample(myTexture, mySampler, vUv + -6.0 * sample_offset) * 0.038650411513543079;
            sum = sum + textureSample(myTexture, mySampler, vUv + -5.0 * sample_offset) * 0.049494378859311142;
            sum = sum + textureSample(myTexture, mySampler, vUv + -4.0 * sample_offset) * 0.060594058578763078;
            sum = sum + textureSample(myTexture, mySampler, vUv + -3.0 * sample_offset) * 0.070921288047096992;
            sum = sum + textureSample(myTexture, mySampler, vUv + -2.0 * sample_offset) *  0.079358891804948081;
            sum = sum + textureSample(myTexture, mySampler, vUv + -1.0 * sample_offset) * 0.084895951965930902;
            sum = sum + textureSample(myTexture, mySampler, vUv + 0.0 * sample_offset) *  0.086826196862124602;
            sum = sum + textureSample(myTexture, mySampler, vUv + 1.0 * sample_offset) * 0.084895951965930902;
            sum = sum + textureSample(myTexture, mySampler, vUv + 2.0 * sample_offset) * 0.079358891804948081;
            sum = sum + textureSample(myTexture, mySampler, vUv + 3.0 * sample_offset) * 0.070921288047096992;
            sum = sum + textureSample(myTexture, mySampler, vUv + 4.0 * sample_offset) * 0.060594058578763078;
            sum = sum + textureSample(myTexture, mySampler, vUv + 5.0 * sample_offset) * 0.049494378859311142;
            sum = sum + textureSample(myTexture, mySampler, vUv + 6.0 * sample_offset) *0.038650411513543079;
            sum = sum + textureSample(myTexture, mySampler, vUv + 7.0 * sample_offset) * 0.028855245532226279;
            sum = sum + textureSample(myTexture, mySampler, vUv + 8.0 * sample_offset) * 0.020595286319257878;
            sum = sum + textureSample(myTexture, mySampler, vUv + 9.0 * sample_offset) * 0.014053461291849008;
            sum = sum + textureSample(myTexture, mySampler, vUv + 10.0 * sample_offset) *  0.009167927656011385;
            outColor = uniforms.attenuation * sum;
            outColor.a = 1.0;
        }
    `
}