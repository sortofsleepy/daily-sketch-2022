/**
 * Various useful helper functions.
 */
export default {

    rotateX:`
    fn rotateX(p:vec3<f32>, theta:f32) -> vec3<f32>{
        var s:f32 = sin(theta);
        var c:f32 = cos(theta);
        return vec3<f32>(p.x, p.y * c - p.z * s, p.z * c + p.y * s);   
    }
    `,

    rotateY:`
    
       fn rotateY(p:vec3<f32>, theta:f32) -> vec3<f32>{
            var s:f32 = sin(theta);
            var c:f32 = cos(theta);
            return vec3<f32>(p.x * c + p.z * s, p.y, p.z * c - p.x * s);  
       }
    `,

    rotateZ:`
       fn rotateZ(p:vec3<f32>, theta:f32) -> vec3<f32>{
            var s:f32 = sin(theta);
            var c:f32 = cos(theta);
            return vec3<f32>(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
       }
    
    `
}