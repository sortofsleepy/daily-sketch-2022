export default {

    // common functions found in noise shaders
    // note that unlike in GLSL, there is no functional overloading so each function is labeled like mod289Vec3, etc.
    commonFunctions:`
       fn mod289Vec3(x: vec3<f32>) -> vec3<f32> {
            var x1: vec3<f32>;
        
            x1 = x;
            let e2: vec3<f32> = x1;
            let e3: vec3<f32> = x1;
            return (e2 - (floor((e3 * (1.0 / 289.0))) * 289.0));
        }

        fn mod289Vec2(x2: vec2<f32>) -> vec2<f32> {
            var x3: vec2<f32>;
        
            x3 = x2;
            let e2: vec2<f32> = x3;
            let e3: vec2<f32> = x3;
            return (e2 - (floor((e3 * (1.0 / 289.0))) * 289.0));
        }
        
        fn mod289Vec4(x: vec4<f32>) -> vec4<f32> {
            var x1: vec4<f32>;
        
            x1 = x;
            let e2: vec4<f32> = x1;
            let e3: vec4<f32> = x1;
            return (e2 - (floor((e3 * (1.0 / 289.0))) * 289.0));
        }
        
        fn permuteVec3(x:vec3<f32>)->vec3<f32>{
            return mod289Vec3(((x*34.0)+1.0)*x);
        }
        
        fn permuteVec4(x:vec4<f32>)->vec4<f32>{
            return mod289Vec4(((x*34.0)+1.0)*x);
        }
        
        fn taylorInvSqrtVec4(r:vec4<f32>)->vec4<f32>{
             return 1.79284291400159 - 0.85373472095314 * r;
        }
        fn mod289F32(x:f32)->f32 {
            return x - floor(x * (1.0 / 289.0)) * 289.0; }

        fn permuteF32(x:f32)->f32 {
            return mod289F32(((x*34.0)+1.0)*x);
        }

        fn taylorInvSqrtF32(r:f32)->f32
        {
          return 1.79284291400159 - 0.85373472095314 * r;
        }
        
        fn lessThan(a:vec4<f32>, b:vec4<f32>)->f32 {
        
            return 1.0;
        }
        
        fn grad4(j: f32, ip: vec4<f32>) -> vec4<f32> {
          
            var ones: vec4<f32> = vec4<f32>(1.0, 1.0, 1.0, -1.0);
            var p:vec4<f32> = vec4<f32>(1.0,1.0,1.0,1.0);
            var s:vec4<f32>;
            
            var f:vec3<f32> = floor( fract (vec3<f32>(j) * ip.xyz) * 7.0) * ip.z - 1.0;
     
            //p.xyz = floor( fract (vec3<f32>(j) * ip.xyz) * 7.0) * ip.z - 1.0;
            p.x = f.x;
            p.y = f.y;
            p.z = f.z;
            
            
            
            p.w = 1.5 - dot(abs(p.xyz), ones.xyz);
            s = vec4<f32>(lessThan(p, vec4<f32>(0.0)));
            
               
            var f2:vec3<f32> =  (s.xyz*2.0 - 1.0) * s.www;
      
            p.x = f2.x;
            p.y = f2.y;
            p.z = f2.z;
            
            return p;
        }
        
                
        fn taylorInvSqrtFloat(r:f32)->f32
        {
          return 1.79284291400159 - 0.85373472095314 * r;
        }
        
    `,

    // noise functions based on @cabbibo's shaders found here
    // https://github.com/cabbibo/PhysicsRenderer/blob/master/shaders/simplex.glsl
    simplex:`
     
        

        //// START NOISE ///
    
        fn snoiseVec2(v:vec2<f32>)->f32{
        
            var C: vec4<f32> = vec4<f32>(0.21132487058639526, 0.3660254180431366, -0.5773502588272095, 0.024390242993831635);
            
            // first corner
            var i:vec2<f32> = floor(v + dot(v, C.yy) );
            var x0:vec2<f32> = v - i + dot(i, C.xx);
            
            // other corner
            var i1:vec2<f32>;
            if(x0.x > x0.y){
                i1 = vec2<f32>(1.0, 0.0);
            }else{
                i1 =  vec2<f32>(0.0, 1.0);
            }
            
            var x12:vec4<f32> = x0.xyxy + C.xxzz;
            x12.x = x12.x - i1.x;
            x12.y = x12.y - i1.y;
             
            // permutations
            i = mod289Vec2(i);
            
            var p:vec3<f32> = permuteVec3( permuteVec3( i.y + vec3<f32>(0.0, i1.y, 1.0 )) + i.x + vec3<f32>(0.0, i1.x, 1.0 ));
            
            //var t:vec3<f32> = 0.5 - vec3<f32>(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw));
            var m:vec3<f32> = max(0.5 - vec3<f32>(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), vec3<f32>(0.0,0.0,0.0));
            m = m*m ;
            m = m*m ;
  
            // Gradients: 41 points uniformly over a line, mapped onto a diamond.
            // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
            
            var x:vec3<f32> = 2.0 * fract(p * C.www) - 1.0;
            var h:vec3<f32> = abs(x) - 0.5;
            var ox:vec3<f32> = floor(x + 0.5);
            var a0:vec3<f32> = x - ox;
            
            
            // Normalise gradients implicitly by scaling m
            // Approximation of: m *= inversesqrt( a0*a0 + h*h );
            m = m * 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
                
            // Compute final noise value at P
            var g:vec3<f32>;
            g.x = a0.x * x0.x + h.x * x0.y;
            
            var gYzValue:vec2<f32> = a0.yz * x12.xz + h.yz * x12.yw;
            g.y = gYzValue.x;
            g.z = gYzValue.y;
            return 130.0 * dot(m, g);
        }
        
        fn snoiseVec3(v:vec3<f32>)->f32{
            var C:vec2<f32> = vec2<f32>(1.0/6.0, 1.0/3.0);
            var D:vec4<f32> = vec4<f32>(0.0, 0.5, 1.0, 2.0);
            
            // First corner
            var i:vec3<f32> = floor(v + dot(v, C.yyy) );
            var x0:vec3<f32> = v - i + dot(i, C.xxx) ;
            
            // Other corners
            var g:vec3<f32>= step(x0.yzx, x0.xyz);
            var l:vec3<f32>= 1.0 - g;
            var i1:vec3<f32> = min( g.xyz, l.zxy );
            var i2:vec3<f32> = max( g.xyz, l.zxy );
            
            var x1:vec3<f32> = x0 - i1 + C.xxx;
            var x2:vec3<f32> = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
            var x3:vec3<f32> = x0 - D.yyy; // -1.0+3.0*C.x = -0.5 = -D.y
            
            // Permutations
            i = mod289Vec3(i);
            var p:vec4<f32> = permuteVec4( permuteVec4( permuteVec4(
                     i.z + vec4<f32>(0.0, i1.z, i2.z, 1.0 ))
                   + i.y + vec4<f32>(0.0, i1.y, i2.y, 1.0 ))
                   + i.x + vec4<f32>(0.0, i1.x, i2.x, 1.0 ));
                   
            // Gradients: 7x7 points over a square, mapped onto an octahedron.
            // The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
            var n_:f32 = 0.142857142857; // 1.0/7.0
            var ns:vec3<f32> = n_ * D.wyz - D.xzx;
            
                        
            var j:vec4<f32> = p - 49.0 * floor(p * ns.z * ns.z); // mod(p,7*7)
            
            var x_:vec4<f32> = floor(j * ns.z);
            var y_:vec4<f32> = floor(j - 7.0 * x_ ); // mod(j,N)
            
            var x:vec4<f32>= x_ *ns.x + ns.yyyy;
            var y:vec4<f32>= y_ *ns.x + ns.yyyy;
            var h:vec4<f32>= 1.0 - abs(x) - abs(y);
            
            var b0:vec4<f32>= vec4<f32>( x.xy, y.xy );
            var b1:vec4<f32>= vec4<f32>( x.zw, y.zw );
            
              
            var s0:vec4<f32> = floor(b0)*2.0 + 1.0;
            var s1:vec4<f32> = floor(b1)*2.0 + 1.0;
            var sh:vec4<f32> = -step(h, vec4<f32>(0.0));
            
            var a0:vec4<f32> = b0.xzyw + s0.xzyw*sh.xxyy ;
            var a1:vec4<f32> = b1.xzyw + s1.xzyw*sh.zzww ;
            
            var p0:vec3<f32> = vec3<f32>(a0.xy,h.x);
            var p1:vec3<f32> = vec3<f32>(a0.zw,h.y);
            var p2:vec3<f32> = vec3<f32>(a1.xy,h.z);
            var p3:vec3<f32> = vec3<f32>(a1.zw,h.w);
            
            
            //Normalise gradients
            var norm:vec4<f32> = taylorInvSqrtVec4(vec4<f32>(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
            p0 = p0 * norm.x;
            p1 = p1 * norm.y;
            p2 = p2 * norm.z;
            p3 = p3 * norm.w;
            
            // Mix final noise value
            var m:vec4<f32> = max(0.6 - vec4<f32>(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), vec4<f32>(0.0,0.0,0.0,0.0));
            m = m * m;
            return 42.0 * dot( m*m, vec4<f32>( dot(p0,x0), dot(p1,x1),
                                dot(p2,x2), dot(p3,x3) ) );
        }
        
        /////// MAIN FUNCTIONS TO RUN IN APP ////////////
        fn snoise( x:vec3<f32> )->vec3<f32>{

          var s:f32  = snoiseVec3(vec3<f32>( x));
          var s1:f32 = snoiseVec3(vec3<f32>( x.y - 19.1 , x.z + 33.4 , x.x + 47.2 ));
          var s2:f32 = snoiseVec3(vec3<f32>( x.z + 74.2 , x.x - 124.5 , x.y + 99.4 ));
          var c:vec3<f32> = vec3<f32>( s , s1 , s2 );
          return c;

        }

     
        fn curlNoise( p:vec3<f32> )->vec3<f32>{
          
          var e:f32 = 0.001;
          var dx:vec3<f32>= vec3<f32>( e   , 0.0 , 0.0 );
          var dy:vec3<f32>= vec3<f32>( 0.0 , e   , 0.0 );
          var dz:vec3<f32>= vec3<f32>( 0.0 , 0.0 , e   );
        
          var p_x0:vec3<f32>= snoise( p - dx );
          var p_x1:vec3<f32>= snoise( p + dx );
          var p_y0:vec3<f32>= snoise( p - dy );
          var p_y1:vec3<f32>= snoise( p + dy );
          var p_z0:vec3<f32>= snoise( p - dz );
          var p_z1:vec3<f32>= snoise( p + dz );
        
          var x:f32 = p_y1.z - p_y0.z - p_z1.y + p_z0.y;
          var y:f32 = p_z1.x - p_z0.x - p_x1.z + p_x0.z;
          var z:f32 = p_x1.y - p_x0.y - p_y1.x + p_y0.x;
        
          var divisor:f32 = 1.0 / ( 2.0 * e );
          return normalize( vec3<f32>( x , y , z ) * divisor );
        }
    `
}

/*

CURRENTLY THIS FUNCTION TRIGGERS WEIRD SPIR-V ISSUE
   fn snoiseVec4(v:vec4<f32>)->f32{
            var C: vec4<f32> = vec4<f32>(0.138196601125011, // (5 - sqrt(5))/20 G4
                        0.276393202250021, // 2 * G4
                        0.414589803375032, // 3 * G4
                       -0.447213595499958);


            var F4:f32 = 0.309016994374947451;

            // First corner
            var i:vec4<f32> = floor(v + dot(v, vec4<f32>(F4)) );
            var x0:vec4<f32> = v - i + dot(i, C.xxxx);


            // Rank sorting originally contributed by Bill Licea-Kane, AMD (formerly ATI)
            var i0:vec4<f32>;
            var isX:vec3<f32>= step( x0.yzw, x0.xxx );
            var isYZ:vec3<f32> = step( x0.zww, x0.yyz );

            i0.x = isX.x + isX.y + isX.z;
            var i0YZW:vec3<f32> = 1.0 - isX;
            //i0.yzw = 1.0 - isX;
            i0.y = i0YZW.x;
            i0.z = i0YZW.y;
            i0.w = i0YZW.z;

            i0.y = i0.y + isYZ.x + isYZ.y;

            var i0ZW:vec2<f32> = (i0.zw + 1.0 - isYZ.xy);
            //i0.zw = i0.zw + 1.0 - isYZ.xy;

            i0.z = i0ZW.x;
            i0.w = i0ZW.y;


            i0.z = i0.z + isYZ.z;
            i0.w = i0.w + 1.0 - isYZ.z;

            // i0 now contains the unique values 0,1,2,3 in each channel

            var i2val:vec4<f32> = i0 - 1.0;
            var i1val:vec4<f32> = i0 - 2.0;

            var i3:vec4<f32> = clamp( i0, vec4<f32>(0.0,0.0,0.0,0.0), vec4<f32>(1.0,1.0,1.0,1.0) );
            var i2:vec4<f32> = clamp( i2val, vec4<f32>(0.0,0.0,0.0,0.0), vec4<f32>(1.,1.,1.,1.) );
            var i1:vec4<f32> = clamp( i1val, vec4<f32>(0.0,0.0,0.0,0.0), vec4<f32>(1.,1.,1.,1.) );

            var x1:vec4<f32> = x0 - i1 + C.xxxx;
            var x2:vec4<f32> = x0 - i2 + C.yyyy;
            var x3:vec4<f32> = x0 - i3 + C.zzzz;
            var x4:vec4<f32> = x0 + C.wwww;


            // Permutations
            i = mod289Vec4(i);
            var j0:f32 = permuteF32( permuteF32( permuteF32( permuteF32(i.w) + i.z) + i.y) + i.x);
            var j1:vec4<f32> = permuteVec4( permuteVec4( permuteVec4( permuteVec4 (
             i.w + vec4<f32>(i1.w, i2.w, i3.w, 1.0 ))
           + i.z + vec4<f32>(i1.z, i2.z, i3.z, 1.0 ))
           + i.y + vec4<f32>(i1.y, i2.y, i3.y, 1.0 ))
           + i.x + vec4<f32>(i1.x, i2.x, i3.x, 1.0 ));

            // Gradients: 7x7x6 points over a cube, mapped onto a 4-cross polytope
            // 7*7*6 = 294, which is close to the ring size 17*17 = 289.
            var ip:vec4<f32>= vec4<f32>(1.0/294.0, 1.0/49.0, 1.0/7.0, 0.0) ;

            var p0:vec4<f32>= grad4(j0, ip);
            var p1:vec4<f32>= grad4(j1.x, ip);
            var p2:vec4<f32>= grad4(j1.y, ip);
            var p3:vec4<f32>= grad4(j1.z, ip);
            var p4:vec4<f32>= grad4(j1.w, ip);

            // Normalise gradients
            var norm:vec4<f32> = taylorInvSqrtVec4(vec4<f32>(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
            p0 = p0 * norm.x;
            p1 = p1 * norm.y;
            p2 = p2 *  norm.z;
            p3 = p3 * norm.w;


            //TODO
            //p4 = p4 * taylorInvSqrtVec4(dot(p4,p4));


            // Mix contributions from the five corners

            var m0:vec3<f32> = max(0.6 - vec3<f32>(dot(x0,x0), dot(x1,x1), dot(x2,x2)), vec3<f32>(0.0,0.0,0.0));


            var m1:vec2<f32> = max(0.6 - vec2<f32>(dot(x3,x3), dot(x4,x4) ), vec2<f32>(0.0,0.0));
            m0 = m0 * m0;
            m1 = m1 * m1;
            return 49.0 * ( dot(m0*m0, vec3<f32>( dot( p0, x0 ), dot( p1, x1 ), dot( p2, x2 )))
               + dot(m1*m1, vec2<f32>( dot( p3, x3 ), dot( p4, x4 ) ) ) ) ;
        };

 */