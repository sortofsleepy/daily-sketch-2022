

// AKA BindGroupLayout trying to keep terms reasonably similar to Vulkan
export default class DescriptorLayout {
    instance:any;

    // describes the actual resource associated with a bind group item
    bindGroupEntries:Array<BindGroupDescriptorEntry> = [];

    // describes a bind group and what it will consist of
    bindGroupLayouts:Array<any> = [];

    constructor(instance) {
        this.instance = instance;
    }

    hasDescriptors(){
        return this.bindGroupEntries.length > 0;
    }

    addDescriptor(entry:BindGroupDescriptorEntry){
        this.bindGroupEntries.push(entry);
        return this;
    }

    /**
     * @deprecated
     * maintaining function but changing later to compileDescriptorLayout.
     */
    compileDefaultDescriptorLayout(){
        this.compileDescriptorLayout();
    }
    compileDescriptorLayout(){
        this.bindGroupLayouts.push(this.instance.device.createBindGroupLayout({
            entries:this.bindGroupEntries
        }))
    }

    generatePipelineLayout(){
        return this.instance.device.createPipelineLayout({
            bindGroupLayouts:this.bindGroupLayouts
        })
    }
}