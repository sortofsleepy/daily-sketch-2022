import {BGRA8UNORM, TRIANGLE_LIST} from "../constants";
import RenderPipeline from "../core/pipeline";
import Buffer from "../core/buffer";
import mat4 from "../math/mat4";
import ColorState from "../core/colorstate";
import DescriptorLayout from "./bindgroup";


interface MeshAttribute {
    name:string;
    data:Array<any>;
    buffer:Buffer;
}

export default class Mesh {
    instance:any;

    // pipeline to render
    pipeline:RenderPipeline;


    // attributes
    attributes:Array<MeshAttribute> = [];

    shader:any = null;

    // index buffer
    indexBuffer:Buffer = null;

    // model matrix for the mesh
    modelMatrix:any = mat4.create();

    // topology type to use for the mesh;
    topology:string = "triangle-list";

    cullMode:string = "none"

    name:string;

    // helps set up bind group layouts for the mesh.
    descriptorLayout:DescriptorLayout;

    // drawing parameters
    // see https://gpuweb.github.io/gpuweb/#render-pass-encoder-drawing#dom-gpurenderencoderbase-draw for list
    drawParameters:any = {
        vertexCount:0,
        instanceCount:1,
        firstVertex:0,
        firstInstance:0,
        indexCount:0,
        baseVertex:0,
        firstIndex:0
    };

    colorStates:Array<ColorState> = [];

    constructor(instance,{
        topology = TRIANGLE_LIST,
        instanceCount = 1
    }={}) {

        this.instance = instance;
        this.descriptorLayout = new DescriptorLayout(instance);
    }

    setIndexData(data:Array<number>){
        let device = this.instance.device;

        //@ts-ignore
        data = new Uint16Array(data);
        let indexCount = data.length * 3;

        let buffer = new Buffer(this.instance);
        buffer.set({
            data:data,
            usage:GPUBufferUsage.INDEX,
            arrayType:"Uint16Array"
        });

        this.drawParameters.indexCount = indexCount;
        this.indexBuffer = buffer;


    }

    addColorState(cstate:ColorState){
        this.colorStates.push(cstate);
    }


    draw(passEncoder){
        passEncoder.setPipeline(this.pipeline.pipeline);

        this.attributes.forEach((attrib,i)=>{
            passEncoder.setVertexBuffer(i,attrib.buffer.getObject())
        })


        if(this.indexBuffer === null){
            passEncoder.draw(
                this.drawParameters.vertexCount,
                this.drawParameters.instanceCount,
                this.drawParameters.firstVertex,
                this.drawParameters.firstInstance
            );
        }else{

            passEncoder.setIndexBuffer(this.indexBuffer.getObject(),"uint16")

            passEncoder.drawIndexed(
                this.drawParameters.indexCount,
                this.drawParameters.instanceCount,
                this.drawParameters.firstIndex,
                this.drawParameters.baseVertex,
                this.drawParameters.firstInstance
            )
        }


    }

    /**
     * Updates the specified attribute
     * @param id {string} id for the attribute
     * @param data {any} data for the attribute. s
     */
    updateAttribute(id:string,data:any){
        this.attributes.forEach(attrib => {
            if(attrib.name === id){
                attrib.buffer.updateData(data);
            }
        })

    }

    setNumVertices(numVertices:number){
        this.drawParameters.vertexCount = numVertices;
        return this;
    }

    setNumInstances(instances:number){
        this.drawParameters.instanceCount =  instances;
        return this;
    }

    // Returns the GPUBuffer object associated with the specified attribute name.
    getAttributeBuffer(name=""){
        let i = null;
        this.attributes.forEach(attrib => {
            if(attrib.name === name){
                i = attrib.buffer.getObject();
            }
        })

        return i;
    }

    compile(pipelineSettings:PipelineSettings){

        if(!this.shader){
            console.warn("Attempted to compile Mesh without a shader")
            return;
        }

        // if cull mode was not specified, use default mode.
        if(!pipelineSettings.hasOwnProperty("cullMode")){
            pipelineSettings.cullMode = this.cullMode;
        }

        // build a render pipeline
        this.pipeline = new RenderPipeline(this.instance,{
            topology:this.topology,
            colorStates:this.colorStates
        })


        if(pipelineSettings !== null){
            this.pipeline.buildPipeline(this.shader,pipelineSettings);
        }else{
            this.pipeline.buildPipeline(this.shader);
        }


    }



    setTopology(topology){
        this.topology = topology;
    }

    getBindGroupLayout(index=0){
        return this.pipeline.getBindGroupLayout(index);
    }

    setShader(shader){

        if(shader.hasOwnProperty("type") &&
            shader.type === "WGSLShader"){
            this.shader = shader;
        }

        //TODO @deprecated
        if(shader.hasOwnProperty("type") &&
            shader.type === "GLSLShader"){
            this.shader = shader;
        }
    }

    /**
     * Builds an attribute for the mesh
     * @param data {any} the raw data for the attribute
     * @param name {string} optional - a name to reference the attribute. Will default to array index if none specified.
     * @param mappedAtCreation {boolean} - whether or not to map and apply the data to the buffer when created
     */
    addAttribute(data:any,{
        name="",
        mappedAtCreation = true,
        flags = 0
    }={}){
        let device = this.instance.device;

        let buffer = new Buffer(this.instance);
        buffer.set({
            data:data,
            usage:flags !== 0 ? GPUBufferUsage.VERTEX | flags : GPUBufferUsage.VERTEX
        });

        let attrib:MeshAttribute = {
            name: name !== "" ? name : this.attributes.length.toString(),
            data:data,
            buffer:buffer
        }

        this.attributes.push(attrib);
    }

    addAttributeBuffer(buff:Buffer,{
        name = ""
    }={}){
        let device = this.instance.device;



        let attrib:MeshAttribute = {
            name: name !== "" ? name : this.attributes.length.toString(),
            data:null, // null cause data already written to a buffer.
            buffer:buff
        }

        this.attributes.push(attrib);
    }
}