import Mesh from "../deprecate/mesh"
import {FLOAT32_3, POINT_LIST} from "../constants";

/**
 * A basic loader for data exported from Houdini using this Python node script
 * https://gist.github.com/sortofsleepy/6b8ac9f5048fd6d8538cfd9596937e1e
 * @param data
 */
export default function(data,shader,instance){


    // data is received in an array of various attributes.
    // Vertex(P) and color(Cd) information are assumed to be 3 component vectors
    let mesh = new Mesh(instance);

    // keeps track of attributes for the debug mesh.
    let bufferDesc = []

    // keeps track of attributes for the debug mesh
    let loc = 0;

    // contains raw information about the houdini geometry
    let modelData = [];

    for(let a in data){
        let attrib = data[a];
        let len = attrib.data.length;


        mesh.addAttribute(new Float32Array(attrib.data),{
            name:attrib.attrib
        })

        bufferDesc.push(
            {
                arrayStride:4 * 3,
                attributes:[
                    {
                        shaderLocation: loc,
                        offset: 0,
                        format: FLOAT32_3
                    }
                ]
            }
        )

        loc += 1;

        // data comes in un-rolled which is harder to work with in some contexts, so
        // re-roll it

        let rolledAttribData = [];
        for(let i = 0; i < len; i += 3){
            rolledAttribData.push(
                [
                    attrib.data[i],
                    attrib.data[i + 1],
                    attrib.data[i + 2],
                ]
            )
        }
        modelData.push({
            attrib:attrib.attrib,
            data:rolledAttribData
        })
    }

    mesh.setShader(shader);
    mesh.setTopology(POINT_LIST);
    mesh.setNumVertices(modelData[0].data.length);

    mesh.compile({
        layout:null,
        buffers:bufferDesc,
        cullMode:"back"
    })

    return {
        mesh:mesh,
        data:modelData
    }

}