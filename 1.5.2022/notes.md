Setup 
===
* Install UE5. It may work on UE 4 as I don't believe I used any 5 specific features but since it would take almost an hour to double check, let's just say you need UE 5. 
* Houdini file for the Mesh is included. 


Recources
====
* https://www.youtube.com/watch?v=oIdKxYYQBdw
* More references listed in the Houdini file. 
