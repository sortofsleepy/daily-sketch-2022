// Copyright Epic Games, Inc. All Rights Reserved.

#include "SquareDestroy.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SquareDestroy, "SquareDestroy" );
