// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SquareDestroyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SQUAREDESTROY_API ASquareDestroyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
