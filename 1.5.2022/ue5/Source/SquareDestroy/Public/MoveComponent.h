// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MoveComponent.generated.h"

// Helper function to cut down on the amount of writing needed to log something. Accepts one argument
#define LOG_ARG( stream , args) UE_LOG(LogTemp, Warning, TEXT(stream),args);

// same as LOG_ARG but accepts 2 arguments 
#define LOG_2ARG( stream , arg1, arg2) UE_LOG(LogTemp, Warning, TEXT(stream),arg1,arg2);

// same as LOG_ARG but accepts 3 arguments 
#define LOG_3ARG( stream , arg1, arg2,arg3) UE_LOG(LogTemp, Warning, TEXT(stream),arg1,arg2,arg3);

// Just logs some information to the console.
#define LOG_I( stream) UE_LOG(LogTemp, Warning, TEXT(stream));

// Logs error message to console
#define LOG_E( stream) UE_LOG(LogTemp, Error, TEXT(stream));


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SQUAREDESTROY_API UMoveComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMoveComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Projectile Movement")
	FTransform transform;

	
	FVector currentPosition;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
