#version 450
#extension GL_ARB_separate_shader_objects : enable


layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
    mat4 model;
} ubo;


layout(binding = 1) uniform UniformBufferObject2 {
    float time;
    vec2 resolution;
} env;



layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inColor;
layout(location=0) out vec4 vColor;
layout(location=1) out float vLife;

vec3 rotateX(vec3 p, float theta){
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}

vec3 rotateY(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

out gl_PerVertex
{
    vec4 gl_Position;
    float gl_PointSize;
};

void main() {

    gl_PointSize = 1.0;

    vec4 pos = inPosition;

    pos.xyz = rotateX(pos.xyz,sin(env.time) * 0.5);
    pos.xyz = rotateY(pos.xyz,cos(env.time) * 0.5);
    pos.xyz = rotateZ(pos.xyz,sin(env.time) * 0.5);

    vColor = inColor;
    vLife = inPosition.a;
    gl_Position =  ubo.proj * ubo.view * vec4(pos.xyz,1.);
}