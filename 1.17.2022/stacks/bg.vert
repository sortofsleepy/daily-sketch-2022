#version 450
#extension GL_ARB_separate_shader_objects : enable

struct AppParam {
    float time;
    vec2 resolution;
    vec4 col;
};

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(binding = 1,std140) uniform UniformBufferObject2 {
    vec4 color;
    vec4 params;
} panel;

layout(binding = 2) uniform UniformBufferObject3 {
    vec4 meta;
} env;




layout(location = 0) in vec3 position;

layout(location = 0) out vec4 vPos;

vec3 rotateZ(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

out gl_PerVertex
{
    vec4 gl_Position;
    float gl_PointSize;
};

void main() {
    vPos = vec4(position,1.);

    vec3 pos = position;

    pos *= panel.params.y;

    pos.z += sin(env.meta.x);

    pos += rotateZ(pos,(env.meta.x + panel.params.x ) * panel.params.a);

    gl_Position = ubo.proj * ubo.view * vec4(pos,1.);

}