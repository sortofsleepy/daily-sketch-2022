#version 450
#extension GL_ARB_separate_shader_objects : enable

struct AppParam {
    float time;
    vec2 resolution;
    vec4 col;
};

layout(binding = 1,std140) uniform UniformBufferObject2 {
    vec4 color;
    vec4 params;
} panel;

layout(binding = 2) uniform UniformBufferObject3 {
    vec4 meta;
} env;

layout (set = 0, binding = 3) uniform sampler2D tex;

layout(location = 0) out vec4 glFragColor;
layout(location = 0) in vec4 vPos;

void main() {
    vec2 uv = gl_FragCoord.xy / env.meta.yz;
    vec4 img = texture(tex,uv);
    //glFragColor = panel.color;
    glFragColor = img * (panel.color + 0.5);

}
