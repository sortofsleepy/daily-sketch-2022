use std::mem::size_of;
use std::rc::Rc;
use ash::vk;
use yoi::math::core::to_radians;
use yoi::prelude::*;
use yoi::vk::descriptorset::TextureSamplerDescriptor;
use yoi::vk::framework::apptime::AppTime;
use yoi::vk::framework::compute::ComputePipeline;
use yoi::vk::framework::offscreen::OffScreen;
use yoi::vk::utils::generate_buffer_for_dynamic_ubo;

const WINDOW_WIDTH:u32 = 2160;
const WINDOW_HEIGHT:u32 = 1440;
const NUM_PARTICLES: i32 = 1000 * 1000; // turned back to 1 mill just cause not everyone will be able to run it I imagine. 

// represents data structure for particles.
#[derive(Copy, Clone)]
struct Particle {
    pub pos:Vec4,
    pub vel:Vec4,
    pub originalVel:Vec4,
    pub meta:Vec4
}


#[derive(Copy, Clone)]
struct PanelParams{
    color:Vec4,

    // for params
    // x = rotation
    // y = scale
    // z = z offset
    params:Vec4
}

// represents the general settings for the particle simulation
#[derive(Copy, Clone)]
struct SimData {
    pub particle_count:i32
}

struct ComputeObject {
    compute:ComputePipeline,
    data_buffer:VkBuffer
}

// contents needed to render each piece of content
struct DisplayObject {
    ds:VkDescriptorSet,
    mesh:Mesh,
    pipeline:GraphicsPipeline
}

struct Model {
    app:VkApp,
    render_obj:DisplayObject,
    panels:DisplayObject,
    compute:ComputeObject,
    sim_parameters:VkBuffer,
    params_data:SimData,
    env:AppParams,
    env_buffer:VkBuffer,
    offscreen:OffScreen
}

#[derive(Copy, Clone)]
struct AppParams {
    meta:Vec4
}

// Lotta particles.
fn main(){
    App::new(String::from("Stacks"),WINDOW_WIDTH,WINDOW_HEIGHT)
        .setup(setup)
        .draw(draw)
        .run();
}

fn setup(_app: &Yoi,window:&winit::window::Window)->Model {
    ///////// BUILD THE BASE APP ///////////

    // setup the necessary window extensions.
    let surface_extensions = ash_window::enumerate_required_extensions(window).unwrap();

    // prepare info on how to init Vulkan instance
    let info = VkAppInfo {
        surface_extension_names:surface_extensions,
        ..Default::default()
    };

    let window_size = Vec2::new(window.inner_size().width as f32, window.inner_size().height as f32);

    // init vulkan app instance.
    let mut app = VkApp::new(info,window);
    app.setup();

    let instance = app.get_instance();
    let rp = app.get_renderpass();

    // build params
    let params_data = SimData{
        particle_count:NUM_PARTICLES
    };
    let params = build_sim_params(instance,&params_data);

    // build the compute components we need
    let compute = setup_compute(instance,&app,&params);

    // build app params
    let env = AppParams{
        meta:Vec4::new(0.0,window_size.x,window_size.y,1.0)
    };
    // add descriptor for enviroment data
    let mut env_ubo = VkBuffer::new();
    env_ubo.set_arbitrary_data(instance,env);


    // build offscreen object
    let off_screen = OffScreen::new(app.get_instance(),window_size[0],window_size[1]);

    // build the panels
    let panels = build_panels(app.get_instance(),app.get_renderpass(),&app.get_sampler(),off_screen.get_color_view(),&env_ubo);

    // build the mesh object.
    let render_obj = build_render_mesh(instance,&compute.data_buffer,off_screen.get_renderpass(),&env_ubo);


    Model{
        app,
        render_obj,
        compute,
        sim_parameters:params,
        params_data,
        env,
        env_buffer:env_ubo,
        offscreen:off_screen,
        panels
    }
}

fn draw(_app:&Yoi, _model:&mut Model){
    let app = &mut _model.app;
    let instance = app.get_instance();

    let offscreen = &mut _model.offscreen;

    let compute = &mut _model.compute;
    let cp = &compute.compute;

    // render objects
    let render_gp = &mut _model.render_obj.pipeline;
    let render_mesh = &mut _model.render_obj.mesh;
    let render_ds = &mut _model.render_obj.ds;

    // panel
    let panels = &mut _model.panels;

    // simulation parameters
    let mut params = _model.params_data;
    let params_buffer = &mut _model.sim_parameters;

    // app parameters
    let mut env = _model.env;
    let mut env_buffer = &mut _model.env_buffer;


    let min_alignment = instance.get_ubo_dynamic_offset_alignment() as u32;
    let mut alignment = size_of::<PanelParams>() as u32;
    if min_alignment > 0 {
        alignment = (alignment + min_alignment - 1) & !(min_alignment - 1);
    }

    app.record_commands(app.get_instance(),|cb, fb| unsafe {



        // render offscreen content
        {
            offscreen.begin(app.get_instance(),*cb);
            render_ds.bind_descriptor_set(instance,*cb,vk::PipelineBindPoint::GRAPHICS,render_gp.get_pipeline_layout(),0);
            render_gp.bind_pipeline(instance,*cb);
            render_mesh.draw(instance,*cb);
            offscreen.end(app.get_instance(),*cb);
        }


        // panels.
        app.start_main_renderpass(cb,fb);
        {
            panels.pipeline.bind_pipeline(app.get_instance(), *cb);

            for i in 0..3 {

                panels.ds.bind_descriptor_set_with_offsets(
                    app.get_instance(),
                    *cb,
                    vk::PipelineBindPoint::GRAPHICS,
                    panels.pipeline.get_pipeline_layout(),
                    0,
                    &[i * alignment]
                );

                panels.mesh.draw(app.get_instance(),*cb);
            }

        }
        app.end_renderpass(cb);
    });

    app.run_compute(instance,cp);
    app.present_with_compute(cp);

    app.update_timer();

    env.meta.x = app.get_elapsed_time();
    env_buffer.update_arbitrary_data(app.get_instance(),env);
    app.update();

}

//////// SETUP FUNCTIONS ////////

fn build_sim_params(instance:&Rc<Vulkan>,sim_data:&SimData)->VkBuffer{
    let mut param_buffer = VkBuffer::new();
    param_buffer.set_arbitrary_data(instance,sim_data);

    param_buffer
}

fn build_render_mesh(
    instance:&Rc<Vulkan>,
    position_buffer:&VkBuffer,
    render_pass:&VkRenderPass,
    env_ubo:&VkBuffer
)->DisplayObject{
    /////// BUILD MESH //////////
    let mut mesh = Mesh::new();
    let num = NUM_PARTICLES;
    let mut colors = vec![];

    for i in 0..num {
        colors.push(rand_vec4());

    }

    mesh.set_num_verts(num as u32);
    mesh.add_attribute_buffer(0, sizeof::<Particle>() as u32,&position_buffer);
    mesh.add_attribute(instance,1,colors);

    ///// BUILD CAMERA /////
    // setup the camera
    let mut camera = Camera::new();

    camera.create_perspective(60.0, (WINDOW_WIDTH / WINDOW_HEIGHT) as f32, 0.1, 10000.0);
    camera.set_zoom(-5.5);


    let mut ubo = VkBuffer::new();
    ubo.set_arbitrary_data(instance,camera.getMatrices());

    let mut ubo_set = UniformDescriptor::new();
    ubo_set.set_buffer(ubo,0,size_of::<CameraMatrices>() as u64);
    ubo_set.set_shader_stage(vk::ShaderStageFlags::VERTEX);

    let mut env_desc = UniformDescriptor::new();
    env_desc.set_shader_binding(1);
    env_desc.set_buffer(*env_ubo,0,(size_of::<AppParams>()) as u64);


    //// BUILD DESCRIPTOR SET //////
    let mut ds = VkDescriptorSet::new();
    ds.add_uniform_descriptor(ubo_set);
    ds.add_uniform_descriptor(env_desc);
    ds.build(instance);
    ds.update_descriptor_data(instance);

    ///// BUILD SHADER //////
    let mut render_shader = VkShader::new();
    render_shader.load_vertex("shaders/stacks/particles.vert",instance);
    render_shader.load_fragment("shaders/stacks/particles.frag",instance);

    ////// BUILD PIPELINE ///////
    let mut gp = GraphicsPipeline::new();
    gp.set_viewport(0.0, 0.0, WINDOW_WIDTH as f32, WINDOW_HEIGHT as f32, 0.0, 1.0);
    gp.set_topology(vk::PrimitiveTopology::POINT_LIST);
    gp.enable_alpha_blending();
    gp.link_mesh_object(&mesh);
    gp.compile(instance,&render_shader,render_pass,&ds);

    DisplayObject{
        mesh,
        ds,
        pipeline: gp
    }

}

fn setup_compute(
    instance:&Rc<Vulkan>,
    app:&VkApp,
    param_buffer:&VkBuffer
)->ComputeObject{

    //// BUILD PARTICLES /////
    let num = NUM_PARTICLES;
    let mut data: Vec<Particle> = Vec::<Particle>::new();
    let M_PI = 3.14149;

    let azimuth:f32 = 256.0 * M_PI / (NUM_PARTICLES as f32);
    let inclination:f32 = M_PI / (NUM_PARTICLES as f32);
    let radius:f32 = 2.0;

    for i in 0..NUM_PARTICLES{

        let iFloat = i as f32;

        let x = radius * ( inclination * (i as f32) ).sin() * ( azimuth * (i as f32)).sin();
        let y = radius * (inclination * iFloat).cos();
        let z = radius * (inclination * iFloat).sin() * (azimuth * iFloat).sin();

        let mut vel = rand_vec4();

        data.push(Particle{
            pos:Vec4::new(0.0,0.0,0.0,1.0),
            vel,
            meta:Vec4::new(0.0,0.0,rand_float(0.0,2.0),1.),
            originalVel:vel
        })
    }

    let data_len = data.len();

    //// START SETUP DESCRIPTOR SET /////
    let mut ds = VkDescriptorSet::new();

    let mut param_ubo = UniformDescriptor::new();
    param_ubo.set_shader_stage(vk::ShaderStageFlags::COMPUTE);
    param_ubo.set_shader_binding(0);
    param_ubo.set_buffer(*param_buffer,0,vk::WHOLE_SIZE);

    ds.add_uniform_descriptor(param_ubo);

    //////////// SETUP SIMULATION DATA ////////////////////
    // setup staging buffer
    let mut stage_fmt = BufferFormat::default();
    stage_fmt.usage_flags = vk::BufferUsageFlags::TRANSFER_SRC;

    let mut stage_buffer = VkBuffer::new_with_format(stage_fmt);
    stage_buffer.set_data(instance,data);

    // setup particle buffer data - note we append the Vertex buffer flag so it can be used in the render mesh.
    let mut particle_fmt = BufferFormat::default();
    particle_fmt.mem_prop_flags = vk::MemoryPropertyFlags::DEVICE_LOCAL;
    particle_fmt.usage_flags = vk::BufferUsageFlags::STORAGE_BUFFER | vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::VERTEX_BUFFER;
    particle_fmt.size = (size_of::<Particle>() * data_len) as u64;

    let mut particle_buffer = VkBuffer::new_with_format(particle_fmt);
    particle_buffer.init_with_info(instance);

    copy_buffer_to_buffer(instance,app.get_primary_cb(),&stage_buffer,&particle_buffer,(size_of::<Particle>() * data_len) as u64);

    //// SETUP DESCRIPTOR FOR DATA ////
    let mut storage = StorageDescriptor::new();
    storage.set_buffer(particle_buffer, 0, sizeof::<Particle>() * (data_len as u64));
    storage.set_shader_binding(1);

    ds.add_storage_descriptor(storage);

    ds.build(instance);
    ds.update_descriptor_data(instance);


    ////// SETUP COMPUTE PASS ////////////
    let mut shader = VkShader::new();
    shader.load_compute("shaders/stacks/compute.glsl",instance);

    let mut compute_pass = ComputePipeline::new(instance);
    compute_pass.set_descriptor_set(ds);
    compute_pass.set_dispatch_size([(NUM_PARTICLES / 100) as u32,1,1]);
    compute_pass.compile(instance,shader);



    ComputeObject{
        compute:compute_pass,
        data_buffer:particle_buffer
    }
}


/// builds the panels that make up the tryptic
fn build_panels(
    instance:&Rc<Vulkan>,
    render_pass:&VkRenderPass,
    sampler:&vk::Sampler,
    img:&vk::ImageView,
    env:&VkBuffer
)->DisplayObject {

    //// BUILD PANELS //////

    let p = PlaneGeo::new(300,300, 2, 2);

    let mut mesh = Mesh::new();

    let mut verts = vec![];

    p.vertices.iter().for_each(|v| {
        verts.push(v.x);
        verts.push(v.y);
        verts.push(v.z);
    });

    // set the position attribute
    mesh.add_attribute_with_format(instance, verts, 0, MeshAttribFormat {
        offset: 0,
        data_format: vk::Format::R32G32B32_SFLOAT,
        stride: (sizeof::<f32>() * 3) as u32
    });

    mesh.add_attribute_with_format(instance, p.uvs, 1, MeshAttribFormat {
        offset: 0,
        data_format: vk::Format::R32G32_SFLOAT,
        stride: (sizeof::<Vec2>()) as u32
    });

    mesh.add_index_data(instance, p.indices);
    mesh.set_instance_count(2);


    // each panel has a different rotation and scale, set that up here
    // x = rotation
    // y = scale
    // z = z offset
    // a = spin direction
    let base_offset = 2.0;
    let panel_positions2 = vec![
        PanelParams{
            color:Vec4::new(1.0,0.0,0.0,1.0),
            params:Vec4::new(to_radians(45.0),4.0 * base_offset,0.0,1.0),
        },
        PanelParams{
            color:Vec4::new(1.0,1.0,0.0,1.0),

            params:Vec4::new(to_radians(45.0),1.9 * base_offset,2.0,-1.0),
        },
        PanelParams{
            color:Vec4::new(1.0,1.0,1.0,1.0),
            params:Vec4::new(0.0,1.5 * base_offset,1.0,1.0),
        }
    ];

    // generates a Buffer suitable for use with dynamic uniforms.
    let buffer = generate_buffer_for_dynamic_ubo(instance,&panel_positions2);

    ////////////////////////////////////////////////////
    //// BUILD CAMERA /////
    let mut cam = Camera::new();
    cam.create_perspective(70.0, (WINDOW_WIDTH / WINDOW_HEIGHT) as f32, 0.1, 10000.0);
    cam.set_zoom(-3005.0);

    let mut cam_buffer = VkBuffer::new();
    cam_buffer.set_arbitrary_data(instance, cam.getMatrices());
    ///////// GENERATE DESCRIPTOR SET //////////////

    let mut ds = VkDescriptorSet::new();

    // generate descriptor for camera
    let mut cam_desc = UniformDescriptor::new();
    cam_desc.set_shader_binding(0);
    cam_desc.set_buffer(cam_buffer, 0, size_of::<CameraMatrices>() as u64);

    // generate dynamic descriptor for panel positions
    let mut desc = DynamicUniformDescriptor::new();
    desc.set_shader_stage(vk::ShaderStageFlags::ALL);
    desc.set_shader_binding(1);
    desc.set_buffer(buffer, 0, (size_of::<Mat4>()) as u64);

    // generate descriptor for texture we render into
    let mut env_desc = UniformDescriptor::new();
    env_desc.set_buffer(*env,0,(size_of::<AppParams>()) as u64);
    env_desc.set_shader_stage(vk::ShaderStageFlags::ALL);
    env_desc.set_shader_binding(2);

    // generate descriptor for texture we render into
    let mut scene_desc = TextureSamplerDescriptor::new();
    scene_desc.set_image(*img,*sampler);
    scene_desc.set_shader_binding(3);


    //// FINISH INIT DESCRIPTOR SET ////
    ds.add_uniform_descriptor(cam_desc);
    ds.add_dynamic_uniform_descriptor(desc);
    ds.add_uniform_descriptor(env_desc);
    ds.add_texture_sampler_descriptor(scene_desc);

    ds.build(instance);
    ds.update_descriptor_data(instance);

    ////// BUILD PIPELINE //////
    let mut gp = GraphicsPipeline::new();

    gp.link_mesh_object(&mesh);

    // load the shader
    let mut shader = VkShader::create("shaders/stacks/bg.vert", "shaders/stacks/bg.frag", instance);

    gp.compile(instance, &shader, render_pass, &ds);
    DisplayObject {
        mesh,

        pipeline:gp,
        ds
    }
}
