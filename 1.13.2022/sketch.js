// Based on the second Coding In the Cabana Episode by Daniel Shiffman
// https://thecodingtrain.com/CodingInTheCabana/002-collatz-conjecture.html
// https://youtu.be/EYLWxwo1Ed8
// https://editor.p5js.org/codingtrain/sketches/XjLDE7gu6



function setup() {
  const colors = [
    createVector(237.0,120.0,71.0,93.0),
    createVector(247.0,74.0,161.0,97.0),
    createVector(77.0,198.0,247.0,97.0)
     
  ]
  createCanvas(800, 80);
  background(0);
  for (let i = 1; i < 5000; i++) { 
    let sequence = [];
    let n = i;
    do {
      sequence.push( n );
      n = collatz(n);
    } while (n != 1);
    
    sequence.push(1);
    sequence.push(constrain(Date.now(),2,5));
    //sequence.reverse(); 

    let len = 8 * constrain(Date.now(),2,5);
    let angle = 0.15;
    resetMatrix();
    translate(width/2, height);
    
    for (let j = 0; j < sequence.length; j+=2) {
    
      let value = sequence[j];
      
      if (value % 2 == 0) {
        rotate(angle);
      } else {
        rotate(-angle);
      }
      
      // set 1
      strokeWeight(2);
      stroke(255, 10);
      line(0, 0, 0, len);
      translate(0, -len);
      
      let index = Math.floor(Math.random() * colors.length);
      let color = colors[index];
      
      // set two 
      strokeWeight(1.5);
      stroke(color.x,len,-len, 10);
      line(0, 0, 0, len);
      translate(0, -len);
      
      angle += 0.2;
      
      
      // random set of circles
      fill(color.x,color.y,color.z,Math.random() + 5);
      if(i % 2 ){
        ellipse(0,0,4.5,4.5);
      }else{
        rect(0,0,6,6);
      }
      
      index = Math.floor(Math.random() * colors.length);
      color = colors[index];
      
      // set three
      strokeWeight(1.5);
      stroke(color.x,len,-len, Math.random() + 1);
      arc(50, 55, len,-len, 0, HALF_PI);
      translate(0, -len);
      
      angle += 0.2;
      
      
    }

    // Visualize the list
  }
}




function collatz(n) {
  // even
  if (n % 2 == 0) {
    return n / 2;
    // odd
  } else {
    return (n * 3 + 1)/2;
  }
}
