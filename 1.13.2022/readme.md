1.13.2022
====

[800x80 is the prompt.](https://genuary.art/prompts#jan13)

See live version [here](https://editor.p5js.org/sortofsleepy/sketches/3nWetMEcS)

Based on the second Coding In the Cabana Episode by Daniel Shiffman
https://thecodingtrain.com/CodingInTheCabana/002-collatz-conjecture.html