use nannou::prelude::*;
use crate::particle::Particle;
use nannou::noise::NoiseFn;
use nannou::rand;
use nannou::rand::prelude::SliceRandom;

mod particle;

fn main() {
    nannou::app(model)
        .update(update)
        .run()
}

struct Model {
    particles:Vec<Particle>,
    z_off:f32,
    rows:f32,
    cols:f32,
    inc:f32,
    noise:nannou::noise::Perlin,
    field:Vec<Vector2>
}

fn model(app:&App) ->Model {

    app.new_window()
        .size(2160,1440)
        .title("Blotches")
        .view(view)
        .build()
        .unwrap();


    let mut particles: Vec<Particle> = vec![];

    for _i in 0..10 {
        particles.push(Particle::new())
    }

    let scl = 10.0;
    let mut cols = app.window_rect().w() / scl;
    let mut rows = app.window_rect().h() / scl;

    cols = cols.floor();
    rows = rows.floor();

    // init vector for flow field.
    let len = cols * rows;
    let mut v = vec![];
    for _i in 0..len as i32 {
        v.push(Vector2::new(0.0,0.0));
    }


    Model{
        particles, // initialization shorthand, since key name is the same as the variable, we can leave the key name out.
        z_off:0.0,
        cols,
        rows,
        inc:0.1,
        noise:nannou::noise::Perlin::new(),
        field:v
    }
}

fn update(_app:&App, _model: &mut Model, _update:Update){

    let mut yoff = 0.0 as f32;
    let perlin = _model.noise;

    let p = &_model.particles;

    let particle = p.choose(&mut rand::thread_rng()).unwrap();


    for y in 0.._model.rows as i32{
        let mut xoff = 0.0;
        for x in 0.._model.cols as i32{

            let pos = particle.get_position();
            let index = x + y * _model.cols as i32;
            let two_pi = PI * PI;
            let angle = perlin.get([xoff as f64,yoff as f64,_model.z_off as f64]) * two_pi as f64 * 4.0;

            let mut v = Vec2::new(angle as f32,angle as f32);
            v = Vec2::new(v.length(),(x as f32) * (angle as f32));

            v.x += pos.x;
            v.y += pos.y;

            _model.field[index as usize] = v;

            xoff += _model.inc;

        }

        yoff += _model.inc;
        _model.z_off += 0.03;
    }


    for i in 0.._model.particles.len() {

        // due to how Rust and Nannou is structured, we need to update previous position here instead of
        // after drawing.
        _model.particles[i].update_prev();

        // now run the rest of the functions.
        _model.particles[i].follow(_model.cols,10.0,&_model.field);
        _model.particles[i].update();
        _model.particles[i].check_edges(_app.window_rect().w(),_app.window_rect().h())
    }

}


fn view(app:&App, _model:&Model, frame:Frame ){


    let width = app.window_rect().w();
    let height = app.window_rect().h();

    let p = & _model.particles;

    let draw = app.draw().translate(Vector3::new(-width/2.0,-height/2.0,0.0))
        .rotate(app.time.sin())
        .roll(app.time.cos());

    let color_list = [
        Vec4::new(237.0,120.0,71.0,93.0),
        Vec4::new(247.0,74.0,161.0,97.0),
        Vec4::new(77.0,198.0,247.0,97.0),
        Vec4::new(80.0,71.0,237.0,93.0),
    ];

    for i in 0..p.len() {
        let particle = p[i];

        let color = color_list.choose(&mut rand::thread_rng()).unwrap().normalize();

        let pos = particle.get_position();
        let prev_pos = particle.get_prev_pos();
        let acc = particle.get_acceleration();

        draw.line()
            .stroke_weight((pos.x + acc.x) * 0.05)
            .rgba(color.x,color.y,color.z,color.w * app.time.sin())
            .caps_round()
            .points(pt2(pos.x,pos.y),pt2(prev_pos.x,prev_pos.y));
    }

    draw.to_frame(app,&frame).unwrap()
}