use nannou::prelude::*;

#[derive(Copy, Clone)]
pub struct Particle {
    pub(crate) pos:Vector2,
    pub(crate) vel:Vector2,
    acc:Vector2,
    max_speed:i32,
    pub prev_pos:Vector2,
    pub counter:f32
}

impl Particle {
    pub fn new()->Self {

        let init_pos = Vector2::new(random_range(0.0,1024.0),random_range(0.0,768.0));
        Particle{
            counter:0.0,
            pos:init_pos.clone(),
            vel:Vector2::new(0.0,0.0),
            acc:Vector2::new(0.0,0.0),
            max_speed:4,
            prev_pos:init_pos.clone()
        }
    }

    pub fn get_acceleration(&self)->&Vec2 {
        &self.acc
    }

    pub fn update(&mut self){
        self.vel += self.acc;
        //self.vel = self.vel.limit_magnitude(self.max_speed as f32);
        self.vel = self.vel.clamp_length_max(self.max_speed as f32);
        self.pos += self.vel;
        self.acc.x = 0.0;
        self.acc.y = 0.0;

        self.counter += 0.01;
    }

    pub fn apply_force(&mut self,force:&Vector2){
        self.acc += *force;
    }

    pub fn follow(&mut self, cols:f32, scl:f32, vectors:&Vec<Vector2>){

        let mut x = self.pos.x / scl;
        x = x.floor();

        let mut y = self.pos.y / scl;
        y = y.floor();

        let index = (x + y * cols) as usize;

        // at certain window sizes you're bound to get an index > number of objects in the field.
        // so account for that.
        if index < vectors.len(){

            let force = vectors[index];
            self.apply_force(&force);
        }

    }

    pub fn get_position(&self)->&Vec2 {
        &self.pos
    }

    pub fn get_prev_pos(&self)->&Vec2 {
        &self.prev_pos
    }

    pub fn update_prev(&mut self){
        self.counter = 0.0;
        self.prev_pos.x = self.pos.x.clone();
        self.prev_pos.y = self.pos.y.clone();
    }

    pub fn check_edges(&mut self, width:f32, height:f32){

        let x_pos = self.pos.x;
        let y_pos = self.pos.y;


        if x_pos > width{
            self.pos.x = 0.0;
            self.update_prev()
        }
        if x_pos< 0.0{
            self.pos.x = width;
            self.update_prev()
        }

        if y_pos > height {
            self.pos.y = 0.0;
            self.update_prev()
        }


        if y_pos < 0.0 {
            self.pos.y = height;
            self.update_prev()
        }

    }
}