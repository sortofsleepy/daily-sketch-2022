

vec2 rot2d( vec2 p, float a ) {
	vec2 sc = vec2(sin(a),cos(a));
	return vec2( dot( p, vec2(sc.y, -sc.x) ), dot( p, sc.xy ) );
}


float random(vec3 scale,float seed){return fract(sin(dot(gl_FragCoord.xyz+seed,scale))*43758.5453+seed);}

float unpack_depth(const in vec4 color) {
	return ( color.r * 256. * 256. * 256. + color.g * 256. * 256. + color.b * 256. + color.a ) / ( 256. * 256. * 256. );
}

float sampleDepth( vec2 uv ) {
	return unpack_depth( texture( iChannel3, uv ) );
}

#define R 2.
vec3 colorize(vec2 pos){	
	
    vec2 uv = pos;
	uv 		= uv * 2. - 1.;
	uv.x 		*= iResolution.x/iResolution.y;
		
	
	float t 	= sqrt(2.)/2.;
	vec3 uvw	= vec3((t*uv.x-uv.y), -(t*uv.x+uv.y), uv.y*2.);
		
	vec3 f 		= vec3(0.);
	
	vec3 r 		= vec3(0.);	
	
	float e 	= .25;
	for(float i = 1.; i <= R; i++)
	{
		f 	= 1.-vec3((mod(e * uvw.x, 1.) < .5 ^^ mod(e * uvw.y, 1.) < .5));
		
		f 	*= fract(uvw*e);
		
		r	+= abs(f/i*2.5)-.25;
		
		e 	*= 2.;	//(mouse.x-.5)*32.;	
	}
	
	r += r/R;
	
	
    return r;
}


// Gamma correction
#define GAMMA (2.2)

vec3 ToLinear( in vec3 col )
{
	// simulate a monitor, converting colour values into light values
	return pow( col, vec3(GAMMA) );
}

vec3 ToGamma( in vec3 col )
{
	// convert back into colour values, so the correct light will come out of the monitor
	return pow( col, vec3(1.0/GAMMA) );
}

vec4 Noise( in ivec2 x )
{
	return texture( iChannel0, (vec2(x)+0.5)/256.0, -100.0 );
}

vec4 Rand( in int x )
{
	vec2 uv;
	uv.x = (float(x)+0.5)/256.0;
	uv.y = (floor(uv.x)+0.5)/256.0;
	return texture( iChannel0, uv, -100.0 );
}


void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec3 ray;
    vec2 test =  vec2(sin(iResolution.x) * iTime,cos(iResolution.y) * iTime);
    
    vec2 test2 = rot2d(test,iTime);
    
	ray.xy = 2.0*(fragCoord.xy- test / 2.0)/iResolution.y;
	ray.z = 0.4;

	float offset = iTime*.5;	
	float speed2 = (cos(offset)+1.0)*2.0;
	
    float speed = speed2+.1;
    offset += sin(offset)*.96;
	offset += 2.0;
	
	
	vec3 col = vec3(0);
	
	vec3 stp = ray/max(abs(ray.x),abs(ray.y));
	
	vec3 pos = 5.0 * stp + .5;
    
	for ( int i=0; i < 100; i++ )
	{
		float z = Noise(ivec2(pos.xy)).x;
        
		z = fract(z-offset);
        
		
       float d = 50.0*z-pos.z;
		
        float w = pow(max(0.0,1.0-8.0*length(fract(pos.xy)-.5)),2.0);
		
        vec3 c = max(vec3(0),vec3(1.0-abs(d+speed2*.5)/speed,1.0-abs(d)/speed,1.0-abs(d-speed2*.5)/speed));
		
        col -= 1.5*(1.0-z)*c*w;
		
        col.xyz *= colorize(vec2(c.x,w));
        pos += stp;
	}
	
    col.x *= sin(iTime);
	col.y *= cos(iTime);
	fragColor = vec4(ToGamma(col),1.0);
}	